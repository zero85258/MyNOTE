# fork(衍生、分支)
* 發生在當開發人員從一個軟體包拷貝了一份原始碼然後在其上進行獨立的開發，建立不同的軟體。
這個術語不只意味著版本控制上的分支

# upstream(上游)
###### 在軟體開發中，upstream通常是指擁有軟體原始碼的`原作者`或是`維護者`，他們通常也較容易發現程式錯誤及使用Patch程式來修復。
###### 舉例來說，一個修補程式被送到上游即為提供給軟體的原作者或是維護者。
```
if 被接受了
	作者或維護者就會包含該部份的修補程式到他們軟體中，可能是立即釋出或包含在未來的版本中。
else 被拒絕了
	提交修補程式的人就不得不繼續維護自他們自己的版本。
	
upstream開發將會使其他fork版本在未來的釋出中受益
```
這個詞也涉及到程式錯誤
最終所有程式錯誤仍需由`上游修復`而不是由其他衍生版本進行移植及整合。(應該先修掉地基)

# CDN
可以將網站上的`靜態內容`（例如.html文件、.jpg圖片）和`動態內容`（例如DB查詢）緩存到CDN提供商位於全球各地的多個伺服器上,加快Client讀取速度

# 內嵌函式inline function
```
內嵌函式inline function{	//論原理比較像是巨集
	來建議編譯器將 呼叫的地方 置換成 函式主體 
}

看起來跟巨集有點類似，最主要的差異在於
巨集	 是	前置處理器(preprocessor)在處理的
內嵌函式 是 	編譯器(compiler)在處理的
```

# `巨集macro` 與 `函式function` 差異


* `函式`是拿時間換取空間的，
透過執行時期在函式堆疊的切換，相同邏輯區塊只要抽出成為函式，則大家共用同一份程式即可。
函式實務上還有另一個好處是可以取得函式位址，也就是說可以把函式當參數。

* `巨集`則是拿空間換取時間，
在編譯之前，前置處理器就將該程式替換至各個呼叫區塊，所以相同的邏輯在會重複出現在各個地方，
但是由於執行時間不需要在函式堆疊切換，節省了點切換時間。

不過據說在現今強大的編譯器優化能力之下，巨集與函式在執行時期的時間差，已經可以忽略了。
由於巨集是文字替換的本質，所以使用上也有上述的細節需要注意，

所以還是提倡多多使用函式實作的函式庫吧。

# 抽象化 <---> 具體化 

	抽象化(隱藏個體過多細節) <---> 具體化
	
	主要是為了只保存和一特定目的有關的資訊
	降低複雜度
	抽象化主要是為了使複雜度降低，以得到論域中，較簡單的概念，
	好讓人們能夠控制其過程或以綜觀的角度來了解許多特定的事態。
	
	高階模組為抽象，abstract class 為抽象，interface 為抽象。
	低階模組為細節，class 為細節。
	
# abstract抽象化,interface介面
	
	若你真的需要繼承某class，但又希望別人無法new它，就將它宣告成abstract；
	若你沒有需要繼承某class，只希望N個class都要求有某些功能,就先宣告一個interface，讓各class去實作它！
	interface可解決部分語言 無法多重繼承

# 遞迴 
	因{
		會要用到大量的 呼叫函式(堆疊) 
	}果{
		使大型程式碼變簡潔
		浪費太多記憶體(內存,CPU的RAM)
	}

# 預處理概念 

	把部分的程式碼先經過編譯
	提升執行效率

# 框架 

	程式架站機CMS 類似 框架

# 作業系統.同步機制 
mutex、semaphore、monitor 等《同步機制》，那些就是作業系統課程裡的重點問題了。  

* mutex	用於`多thread`編程中，防止兩條thread同時對同一公共資源（比如全局變數）進行讀寫的機制  
* semaphore 提供平行運算環境中，控制`多process` (程序)或`多thread`(執行緒)存取共享資源的能力  
* monitor  

# 永遠搞混的SRAM,DRAM

	SRAM 就是 CPU的內存
	DRAM 就是 一條一條的金士頓等...

	伴隨中央處理器的快取記憶體(cache memory) SRAM
	主記憶體(main  memory)的容量 	DRAM
	NAND-Flash所製成的固態硬碟SSD

# 物件導向.性狀trait

	trait就是class的部分實體化
	解決{
		兩個類之間沒有共同父類
	}

# Iterator迭代器(跟yield有關) 與 生成器 

	generator的所有功能都可以使用傳統函式搭配Iterator interfae達成，
	但generator更簡單、更快速、且更節省記憶體，但也只提供forward-only功能

	http://ithelp.ithome.com.tw/articles/10133614
	可以把許多複雜的循序操作，包裝成可以透過foreach來迭代的物件

	可在容器物件（container，例如鏈表或陣列）上遍訪的介面，
	設計人員無需關心容器物件的內存分配的實現細節
	生成器 是一個返回 迭代器 的函數

	PHP的generator就是簡單板的Iterator
	資料量大時,可節省大量內存
	功能性多樣 跟 簡潔性 之間的 折衷方案

# 閉包(特性) 
[link](https://eyesofkids.gitbooks.io/javascript-start-from-es6/content/part4/closure.html)
	
# 匿名函數 
* 沒有名稱的函數,可賦值給變量,還能當物件傳遞  
* 適合作函數或方法的回調  
```js
function($name){
	return sprintf('Hello %s',$name);
};
```

# 動態庫 靜態庫(兩者各有優缺點)
```
*.DLL動態連結(Dynamic Linking)	 
目的{
	節約應用程式所需的磁碟和記憶體空間
	可不公開code的情況分享給別人使用
	更新程式庫 無需重新編譯 其他程式
}
特色{
	優:執行檔的體積小，
	缺:有相依性
}
DLL的函式會在程式執行時才被載入，
而不是直接編譯在執行檔中，這種作法讓系統更彈性的應用硬體資源，
由操作系統根據應用程式的請求，動態到指定目錄下尋找並裝載入記憶體中，
同時需要進行地址重定向
```

```
*.Lib靜態連結(Static Linking)	
目的{
	不需要外部的函數庫支援
}
特色{
	缺:執行檔的體積大
	優:內容簡單
	優:不需要外部的函數庫支援
}
會在開發階段將程式所需要的函數、
資源等全部加入程式的執行檔，執行檔的體積因此變大，
所以靜態連結的執行檔往往需要較大的記憶體空間，當所用的函式庫越多時，執行檔也就越龐大。
在編譯期間由編譯器與鏈結器將它整合至應用程式內，並製作成目的檔以及可以獨立運作的執行檔
```

# 動態語言 靜態語言

* 靜態語言（statically typed languages）
若一個程式語言的型別檢查（Type Checking）工作是發生在編譯時期（Compile Time），則稱之為靜態語言。
程式撰寫時必須使用明確的型別宣告，
編譯的當下，編譯器就會進行型別檢查，且變數或物件的型別一旦宣告後，
在Runtime執行時就無法任意更換型別，否則會發生Exception錯誤。

* 動態語言（dynamically typed languages）
若一個程式語言的型別檢查（Type Checking）工作發生在執行時期（Runtime），則稱之為動態語言。
編譯器不會事先進行型別檢查，而是在執行時才會進行，
且在執行時，變數還能不斷任意更換型別，JavaScript就是一例。



# 登錄檔 
```
什麼是登錄檔？
簡單的說，就是記錄系統活動資訊的幾個檔案，例如：何時、何地 (來源 IP)、何人 (什麼服務名稱)、做了什麼動作 (訊息登錄囉)。
換句話說就是：記錄系統在什麼時候由哪個程序做了什麼樣的行為時，發生了何種的事件等等。
```

# php 的 Static 類屬性,類方法
聲明類屬性或方法為靜態，就可以不實例化類而直接訪問。
靜態屬性 不能通過一個類已實例化的對象來訪問(要靠::)。
靜態方法


# php 的 this self parent

* $this 代表 物件本身(使用時後面變數不用$ ex:$this->apple)
* self 代表 類本身
* parent 代表 父類本身


# SQL 的 LIKE(模糊比對)
```
SELECT Name FROM city WHERE Name LIKE 'app__'
```
有點像正則表達式

# TCP/IP SOCKET HTTP 的區別
```
IP協議對應於網絡層
TCP/IP協議對應於傳輸層	(SOCKET是此層的API接口) PHP 使用Berkley的socket庫
而HTTP協議對應於應用層
```

# Program Process Thread
```
Program == 只是一堆程式碼
Process == OS分配 系統資源() 給 Program 並 執行上面的指令 
Process = Thread[0] + Thread[1] + Thread[2] + .....

作業系統 會配置給每個Thread一部分 CPU暫存器空間 (register)  以及存放執行指令過程中，暫存相關資料 的 記憶體空間
```

# Context switching內文切換(作業系統範疇內的)

	//當CPU從一個process切到另一個process執行之前，(就是切換task)
	Context switching(){	
		PSP行程堆疊.push(執行狀態) 	//OS必須保存原有process的執行狀態(eg. pc, CPU register store in PCB)，
		PSP.pop()			//同時載入 New Process的執行狀態(eg. Load PC, CPU register值from its PCB)
	}
	而 Context switching是一系統負擔，其時間長短，幾乎完全取決於Hardware因素

# 映像檔 
一種2進制檔

# Compiler知識 
```
一個code 經過 "特定Compiler" 只能放在 "特定平台運行"
code.Compiler(PC)	//是不行被放到其他平台的 只能放PC
code.Compiler(ARM)	//是不行被放到其他平台的 只能放ARM
```
# 分時系統 
只有一個CPU,但是同時處裡`多個Process`


# 多型polymorphism , 多載overload , 重載(覆寫)override (一直搞錯)
```
用 參數 控制函式行為的，就叫 "多載overload"。{
	bool Add( int a, int b );
	bool Add( float a, float b );
}


用 類別 控制函式行為的，就叫 "多型polymorphism"。{	//通常會使用interface
	相同的訊息可能會送給多個不同的類別之物件，而系統可依據物件所屬類別，引發對應類別的方法，而有不同的行為
	喇叭.PLAY()
	吉他.PLAY()
	//PLAY()出來的聲音不一樣
	
	一些 功能||範疇 相近的類別，它們有類似的行為與功能，但卻不適合塞近同一個類別中。
	為了 規範開發原則，常會利用一個 interface 或 基礎類別 來延伸這些 子類別，
	以強制這些 子類別 有完全一樣的 屬性() 與 方法()，達到 "規格最佳化"
}

覆寫,重載override {
	寫一個父類相同名稱的函數
	可覆蓋繼承過來的函數用法
}
```

# 委派delegate 
```
//委派 就是拜託他去幫你執行某個 "方法"，
//你所拜託的委派，要能做到你所要的方法(也就是傳回的型別，跟傳入的參數要一致)	
Main(){
	MyDelegate d;

	//依照傳入的動作，選擇要傳入委派的方法,(這邊有用到 匿名函式)
	if(case=="+")		d = new MyDelegate(加法);
	else if(case=="-")	d = new MyDelegate(減法);
	else if(case=="*")	d = new MyDelegate(乘法);
	else if(case=="/")	d = new MyDelegate(除法);
		
	//使用該委派(這邊有用到 匿名函式)
	int Answer = d(5, 2);
		Console.WriteLine(Answer);
	}
	
	public static int 加法(int x, int y){return (x + y);}
	public static int 減法(int x, int y){return (x - y);}
	public static int 乘法(int x, int y){return (x * y);}
	public static int 除法(int x, int y){return (x / y);}
	public delegate int MyDelegate(int x, int y);
    	
```

# Lambda表示式(是一種匿名函式)
```
	//Lambda表示式 概念圖	
	//(input parameters)  =>  { expression }
	
	if(case=="+")
		//加法是用delegate，然後傳入參數並在中括號中 return結果
		d = delegate(int x, int y){ return x + y;};
	else if(case=="-")	
		//減法省略delegate關鍵字，改用Lambda運算式的寫法
		d = (int x, int y) => { return x - y;};
	else if(case=="*")
		//乘法連 中括號 和 rtn 都省了，直接寫要傳回的結果
		d = (int x, int y) => x * y;	
	else if(case=="/")	
		//除法連傳入的型別都可省略，因Lambda會自動推斷
		d = (x, y) => y != 0 ? x / y : 0;	正確的型別
		
```
	
# Dictionary字典,Hashtable雜湊陣列 
	
	//使用上來說都是 a[KEY] = VALUE
	//但是在排序,Thread方面,等...上來說不一樣
	//Dictionary
	//Hashtable 
	//題外話:GET,POST,Cookie,Session這些東西使用上通通都是 a[KEY] = VALUE

# Websocket、Ajax輪詢、長連接(long pull) 比較 
	
	//Websocket
	//優點就是，只要建立一次連接，就可以連續不斷的得到伺服器推送的消息，節省帶寬和伺服器端的壓力。
	客戶端：我要建立websocket連接
	伺服器端：好的，已經切換到websocket協議，websocket連接已經建立
	客戶端：有什麼消息要及時告訴（推送）我
	伺服器端：好的
	伺服器端：xxxxxx
	伺服器端：yyyyyyy

	//Ajax輪詢
	//缺點顯而易見，每次都要建立HTTP連接，即使需要傳輸的數據非常少，所以這樣很浪費帶寬；同時，這個過程是被動性的，即不是伺服器主動推送的。
	ajax輪詢模擬長連接就是每個一段時間（0.5s）就向伺服器發起ajax請求，查詢伺服器端是否有數據更新
	客戶端：有沒有新消息
	伺服器端：沒有。。（第一次http結束）
	客戶端：有沒有新消息
	伺服器端：沒有。。（第三次http結束）
	客戶端：有沒有新消息
	伺服器端：沒有。。（第四次http結束）
	
	//長連接（long pull）
	//缺點也是顯而易見的，同ajax輪詢一樣，也是每次都要建立HTTP連接，也都是被動的。而且這種方法對伺服器的並行要求比較大
	客戶端：有沒有新信息（Request）--第一次http請求開始
	伺服器端：沒有信息，不作回應（時間一直的流逝。。。一直保持http連接，當等到有消息的時候）
	伺服器端：給你xxxx（Response）--這時，第一次的http請求獲得想要的結果，然後還要發起第二、三。。次http請求
	客戶端：有沒有新消息（Request）--第二次http請求開始

# Mutex互斥鎖
```
using System.Threading;
	
	private static void MyThreadProc(){
	
            //進入互斥區，搶得工作權，其他人只得等待
            mut.WaitOne();

            //做些工作
	    
            //離開互斥區，歸還工作權，換下一個
            mut.ReleaseMutex();
        }
```

# starvation 執行緒飢餓

	意思是如果一個Thread優先權太低, 那麼有可能發生沒有足夠時間執行完任務
	
# thread synchronization 執行緒同步化 

	迫使多條thread從 非同步 暫時切換成 同步執行 
	使用lock關鍵字
	迫使各thread在進入特定程式碼區塊時乖乖`列隊`，以達到同步化的效果。
	
# Schema 架構 

	//是指 資料表 的 欄位架構
	
# 分段 分頁 (記憶體分配方式)# 
	
	分段：是依照 程式所需 的記憶體實際大小來分配記憶體位址。
	分頁：是將記憶體切割成 固定大小，然後依程式需求量而給予足夠記憶體空間。
	
	一般來說，
	實體記憶體{
		價格昂貴，但運作速度快，OS 均採取 分段 的配置法，如此可避免配置不完整的空間浪費
	}虛擬記憶體{
		讀取速度慢，但價格便宜，因而採用 分頁 配置模式。
	}
	
# (CI => hook , Laravel => middleware) 待查證 

	//目的: 不修改系統核心文件的基礎上擴展系統功能
	CI 裡叫 hook 
	Laravel 叫做 middleware
	
	//在事件傳送到終點前 截獲||監控 事件的傳輸，
	//像個鉤子鉤上事件一樣，並且能鉤在鉤上事件時，處理一些自己特定的事件
	
# 軟體系統架構 

	//MVC 適合小的系統
	Action --> Controller --> Model <--> View
	
	//Flux 適合大的系統
	Action --> Dispatch --> Store --> View
			^		    |
			|------ Action	<----	

# URI URL URN 差異 

	URI{	//統一資源識別項
		URL(定位符){	//統一資源定位符
			它標識一個網際網路資源，並指定對其進行操作或取得該資源的方法
			協定/域名/目錄及檔案
		}
		URN(名稱){	//統一資源名稱
			URN是基於某命名空間通過名稱指定資源的URI
		}
	}
	
# .htaccess 文件 

	是在Apache HTTP Server這款服務器架設軟件下的
	一個對於系統目錄進行  各種權限規則設置的文件
	
	比較常見的是  
	定義默認首頁名稱，404頁面，301轉向，等等，
	還有更多的功能比如偽靜態，限製圖片外鏈，限制下載，密碼保護，去除頁面廣告等等

# implements實作 (配合interface)

	//有點監督的概念,假如沒有 實現 方法 , 編譯就不會過
	
	--- 會報錯 -----------------------------
	interface action{
    		public function run();
	}

	class animal implements action{
           
	}
	
	--- 正確 --------------------------------
	interface action{
		public function run();
	}

	class animal implements action{
    		function run(){
        	}
	}
	
# SOLID 

	S:單一職責 (好讀)
	O:開放/封閉原則 (代碼的正確率會++) 
		#用擴充的
	L:Liskov替換 (代碼的正確率會++)
		#子類可以擴展父類的功能，但不能改變父類原有的功能
	I:介面隔離 
		#只要是車,就要能開,有輪子
		#該有個規範
	D:依賴反轉 (好修改)
		#車體不該依照輪胎規格設計,應該輪子該依照車體設計(否則 輪子一改 車體得重新設計)
	
# 依賴反轉 DIP(SOLID原則的D) 

	https://www.zhihu.com/question/23277575
	//車體不該依照輪胎規格設計,應該輪子該依照車體設計
	依賴反轉 == 讓 待測物件 僅能相依於 由unitTest訂出interface

	為了可測試性，
	unitTest必須可決定 待測物件 的 相依物件，如此才可由unitTest將相依物件加以 抽換隔離。
	我們不能讓 待測物件 直接相依其他 class
	而實際 相依物件 可由unitTest來決定，如此我們才能對相依物件加以抽換隔離。
	
	依賴反轉 只確保了待測物件的相依物件相依於 interface

# IoC 控制反轉 # Inversion of Control (一種OOP設計原則) 

	用來減低計算機代碼之間的耦合度。
	其基本思想是：藉助於「第三方」實現具有依賴關係的對象之間的解耦
	
# IoC Container 控制反轉'容器' # Inversion of Control Container 

	---service1-----|========|      
	---service2-----| Ioc容器 
	---service3-----|========| 

# 依賴注入 DI (屬於一種IoC方法) 

	依賴注入 == 依賴反轉 + 注入動作
	所謂 依賴注入，就是把底層類(輪胎)作為參數傳入上層類，實現上層類(車體)對下層類(輪胎)的“控制”

[連結](http://jaceju.net/2014-07-27-php-di-container/)

就是將實例變量傳入到一個對象中去(Dependency injection means giving an object its instance variables)。
	
	所謂 依賴
	如果在 Class A 中，有 Class B 的實例，則稱 Class A 對 Class B 有一個依賴。
	例如下面類 Human 中用到一個 Father 對象，我們就說類 Human 對類 Father 有一個依賴。
	
	WHY 不new 改成 建構子(User $user) != 有依賴 {
	
	}

	laravel 是因為service container 讓我們方便實現 DI

# 依賴注入 DI (屬於一種IoC方法) 

	//我們的相依物件 要 相依interface, 
	//我們的相依物件 = model資料 ,所以框架本身model應該是有相依某interface~
	//不然就是我們的 待側物件 根本沒有 相依物件
	DIP 只確保 待測物件 的 相依物件 相依於 interface
	
	高階模組 不要 依賴細節，細節要依賴 interface。
	
	既然 相依物件 相依於 interface，		//interface 指的就是controller抽出的service???
	若 單元測試 可以產生該 interface 的物件，	//new XXXXService()???;
	並加以注入，就可以將相依物件加以抽換隔離，這就是依賴注入。	//publice function xxxxx(User $user)
	
# 單元測試 
```
為了可測試性，unitTest必須可決定 待測物件 的 相依物件，如此才可由單元測試將相依物件加以抽換隔離
想 單元測試 得達成{	//需要 由unitTest 將 相依物件 加以抽換隔離
		必須可決定 待測物件 的 相依物件，如此才可由unitTest。	//白話的說 必須可以mock相依物件,以之前來說可能就是資料上的相依 
	}


	有了IoC != 達成可測試性，IoC只確保了待測物件的相依物件相依於 interface。

	//疑點{
		既然 待測物件 的 相依物件 相依於 interface，
		若單元測試可以產生該 interface 的物件 , 並加以注入，
		就可以將 相依物件 加以抽換隔離，這就是依賴注入。
	}不知是不是答案{
		//不知是不是指mock這件事
	}
	
	
	
	而應該由單元測試訂出 interface，讓待測物件僅能相依於 interface，
```	

# pipeline管線化

# gc (資源釋放)


# 語法糖 

	//JSX,TypeScript,CoffeeScript都算是語法糖
	
# (SQL) ON DUPLICATE KEY用法 (簡潔度優化) 
	
	需求是「新增一筆資料，如果資料已經存在改成更新」(否則insert之前還要select)
	
	INSERT INTO Player(sn, playerName, height, weight)
	VALUES('12', 'Kevin', 177, 72)
	ON DUPLICATE KEY UPDATE playerName = 'Kevin', height = 177, weight = 72
	
# SELECT ... FOR UPDATE (正確度優化) 
[避免 Race condition](http://blog.xuite.net/vexed/tech/22289223-%E7%94%A8+SELECT+...+FOR+UPDATE+%E9%81%BF%E5%85%8D+Race+condition)
	
# php gc  
	
# Laravel核心概念 服務容器 

	Laravel的核心 就是一個 IoC容器，
	稱其為“服務容器”，顧名思義，該容器提供了整個框架中需要的一系列服務(Router,Migrate,Blade等等等...)
	
# laravel 服務提供者 

	服務提供者（的ServiceProvider）。
	假設模塊一多，那麼容器不是越來越大？每次加載，豈不是加載好久？
	能像管子一樣，連接著模塊，插在容器上，
	"需要時"再通過管道獲取呢？這樣子，容器只是裝著管頭而已，就不怕被撐大了！
	這條管子就是服務提供者。
	
	---service1-----|========|      
	---service2-----| Ioc容器 
	---service3-----|========| 
	
# 麵包屑路徑 Breadcrumbs 
	
	//像是這種 就叫做 麵包屑路徑(網頁上的pwd)
	Home > 產品分析 > 廣告主 > 亞太電信股份有限公司 > 產品列表

# closure 
一種外層函數會跑到內層的`特性`

# callback 
	
	//把一個函數傳入另一個函數,然後執行傳入的函數
	
# 匿名函式 

# fetch API

	//為了取代XHR
	
# Promise 

	Promise結構是一種 異步執行 的  控制流程架構

	可以 異步執行的callback函式，
	可以把 多個 異步執行的函式(callback hell)，
	執行流程轉變為序列執行 一個接一個( .then().then().then() )，
	或是並行執行(全部都要處理完再說)，
	更好的錯誤處理方式。

# 多層迴圈 

	//比較常發生的情況
	json結構 每個元素的處理

# mysql viewTable (速度優化) 

	//可把常用的查詢存起來
	//laravel 底下可用 ->toSql() 打印出sql 再存入view;

# mysql Stored procedure (速度優化) 

	//有點像把 多個常用的sql 串成 腳本
	sqlJob1()->sqlJob2()->sqlJob3()->sqlJob4()
	
# laravel job Vs commend 
	
	//一個是排程 //一個是queue
	//其中之一只執行一次
	
# host檔 (host檔的功能 == DNS的功能, 有點像是DNS的本地暫存的概念)

	//網址與IP是 一對多關係
	//使用者上網時原理 user --> DNS($網址) --> 網站IP
	$IP = hosts檔($網址) ? hosts檔($網址) : DNS($網址);

# 資料庫 正規化() 反正規化(效能up) 

	//正規化()		只是建立資料表的原則，而非鐵的定律。
	目的:降低資料的重覆性、避免資料更新異常，使資料庫運作起來更有效率，維護上也更容易
		
	if(過度正規化){
		查詢來自於多個 表 的大量資料時，會造成效能下降(join太多)。
	}	
	
	//反正規化()
	目的:提升效能，以查詢(select)效能為考量
	亦即將原來的第三正規化降成第二正規化，或是將第二正規化降成第一正規化。

# 正規化1~3 

	https://support.microsoft.com/zh-tw/help/283878/description-of-the-database-normalization-basics

	正規化1{
		使用pk index識別每一組關聯的資料。
		刪除各個table中的重複群組。
		為每一組關聯的資料建立不同的table。
	}
	
	正規化2{
		為可套用於多筆記錄的多組值建立不同的table。
		使用fk，讓這些資料表產生關聯。
	}
	
	正規化3{
		刪除不依賴索引鍵的欄位
	}
	
# 存儲引擎 MyISAM InnoDB 

	https://www.jianshu.com/p/a957b18ba40d
	
	if(R/W > 100:1 且update相對較少 || 並發不高 || table數據量小 || 硬件資源有限){
		採用MyISAM引擎
	}
	
	if(R/W比較小，頻繁更新大字段 || 並發高 || table數據量超過1000萬 || 安全性和可用性要求高){
		採用InnoDB引擎
	}
	
	MyISAM{	//性能,性能,性能
		1.每個MyISAM在磁盤上存儲成三個文件{ 
			.frm文件存儲表定義。
			數據文件的擴展名為.MYD (MYData)。
			索引文件的擴展名是.MYI (MYIndex)。
		}
		2.data是以文件的形式存儲，所以在跨平台的數據轉移中會很方便。在backup和恢復時可單獨針對某個table進行操作
		3.強調的是性能，每次查詢具有原子性,其執行數度比InnoDB類型更快，但是不支持 事務。
		4.可以和其他字段一起建立聯合索引。引擎的AI列必須是索引，如果是組合索引，自動增長可以不是第一列，他可以根據前面幾列進行排序後遞增
		5.只支持table lock,操作myisam表時 curd語句都會給表自動加鎖
		6.允許沒有任何index和pk的表存在，index都是保存行的地址
		7.不支持fk
	}

	InnoDB{	//安全,安全,安全
		1.所有的table都保存在同一個數據文件中
		2.需要更多的ram和存儲，它會在ram中建立其專用的緩衝池用於高速緩衝數據和index
		3.免費的方案可以是拷貝數據文件、備份 binlog，或者用 mysqldump，在數據量達到幾十G的時候就相對痛苦了。
		4.提供支持 事務，fk等高級數據庫功能
		5.InnoDB中必須包含只有該字段的index。引擎的AI列必須是index，如果是組合索引也必須是組合索引的第一列
		6.支持事務和row lock，是innodb的最大特色。row鎖大幅度提高了多用戶 並發操作的新能
		7.如果沒有設定pk || not null唯一索引，就會自動生成一個6字節的主鍵(用戶不可見)，數據是主索引的一部分，附加索引保存的是主索引的值
		8.如果你的數據執行大量的INSERT或UPDATE，出於性能方面的考慮，應該使用InnoDB表。 
		  DELETE 從性能上InnoDB更優，但DELETE FROM table時，InnoDB不會重新建立表，而是一行一行的刪除，。
	}

# (SQL) 資料庫分區化(Partition)(速度優化) 

	當資料表的資料越來越多，檔案越來越大，存取速度越來越慢。
	使用分區(partition)是將同一個資料表的資料，分成不同小檔案儲存，
	甚至可以指定每個小檔案儲存的位置，例如放在不同磁碟，增加存取的速度。
	
	#if($data < 20)
		$分區1->save($data) 
	#elseif($data < 40)
		$分區2->save($data) 
		
	"ALTER TABLE {$表名} ADD PARTITION (
		PARTITION {$分區1} VALUES LESS THAN (
			20
		)
		PARTITION {$分區2} VALUES LESS THAN (
			40
		)
	)"

# truncate table 

	在innodb上如果要清空保存有大量數據的表，最好使用truncate table這個命令

# Temporary Table 臨時表 

	如果有一個300萬筆的資料表，而你只需要用到其中的一千筆，
	而且還需要重覆的篩選、比對(Join)這一千筆資料的話，
	那麼Temporary Table(臨時表)能夠減少重複查詢大資料表的次數，
	也能讓資料的篩選和比對變得更容易
	
	下面簡單跟大家說明TEMPORARY TABLE的幾個特性
1. 功能與實體表相同，可以儲存資料、建立、刪除、增加INDEX、PRIMARY KEY...等功能
2. 不可以被reopen(在同一個查詢指令內，查詢TEMPORARY TABLE兩次，但實體表可以)
3. 在同一個連線下會持續存在，當連線關閉時，TEMPORARY TABLE會自動被刪除
4. 表的大小取決於 tmp_table_size 的設定值，當表的大小超過這個設定值資料表會將表存入磁盤，則造成效能大減
	Schema::create('temp', function (Blueprint $table) {
       		$table->increments('id');
        	$table->string('session_id');
        });

# 資料庫交易 

# nginx架構概念 (3大模塊Handler,Filter,Upstream) 

* Handler模塊() 接受來自client的request並產生output的模塊

* Filter模塊() 是過濾response Header和內容的模塊，
	可以對回复的header和body進行處理。
	它的處理時間在獲取回复內容之後，向user發送response之前。
	它的處理過程分為2個階段，過濾HTTP回复的header() 和 過濾HTTP回复的body()，分別進行修改

* Upstream模塊()
	
	```
	//執行nginx流程大綱
	初始化 HTTP Request（讀取來自Client的數據，生成 HTTP Request 對象，該對象含有該請求所有的信息）。
	處理request header。
	處理request body。
	如果有的話，調用與此request（URL 或者 Location）關聯的 handler。
	依次調用各 phase handler 進行處理。
	```
	
# 池Pool 
```
如果有某個物體需要經常 實例化() 並進行 銷毀()
這會導致實例化的成本過高
這時候就可以透過物件池來對物體進行管理
要用時取出,沒用時放回去
//同理 連線池,線程池,事件池,人力池........
```

# mysql mysqld 

	mysql是shell
	mysqld是實際運行的 後台程序
	
# Linux Swap (類似windows的虛擬記憶體)
```
	swapon -s			#查看目前使用的虛擬記憶體 資料
	fallocate -l 4G /swapfile	#在根目錄底下建4G 大小的swap檔案
	
	swapoff /swapfile		#關閉off
	chmod 600 /swapfile
	mkswap /swapfile		#格式化
	swapon /swapfile		#打開on
```
# 代理 反向代理 
	
* `Proxy Server 代理伺服器` 的工作是去各個 Web Server 抓取資料回來放在伺服器上來供用戶讀取、下載，
	
* `Reverse Proxy Server 反向代理伺服器` 做的事則是和 Proxy Server 剛好相反，
	負責將用戶端的資料傳送給藏在 Reverse Proxy Server 後面	的 Web Server，
	這些躲在後面的 Web Server 不會、也不能直接被用戶直接連結，只能經由 Reverse Proxy Server 代理傳送和接收資料。


# 水平和垂直擴充 (Scaling) 
* 垂直擴充（vertical scaling）
就是把硬體升級到更快的CPU或更多RAM  
傳統DB架構的設計是為了能在單一主機上運作順暢，應付大量營運的最簡單方式


* 水平擴充（horiontal scaling）
例如：Hadoop 和 Cassandra，則是被設計在相對較低階伺服器的叢集上執行，  
所以要應付更多資料，最簡單地就是在叢集中增加更多主機，當營運量越高、資料量越大，這種方式就越便宜，

那些巨量資料處理流程也都被建置在水平擴充模型中。但是這種方式有一項特殊成本 -- 
處理分散式資料的程式碼開發工作是有相當難度的，同時也需要考慮速度、擴充性、容錯性、傳統資料庫的單元性（或稱不可分割性）和一致性，在這些要求之間取捨。


# Graph API 
[link](https://medium.com/@evenchange4/2018-graphql-%E6%BC%B8%E9%80%B2%E5%BC%8F%E5%B0%8E%E5%85%A5%E7%9A%84%E6%9E%B6%E6%A7%8B-aeb2603f2223)

# systemd systemctl service
* systemd 啟動服務的機制
* systemctl == service(腳本命令) + chkconfig

# L5 Repository 
* [github](https://github.com/andersao/l5-repository)
* [教學](https://www.jianshu.com/p/f8027eab186c)
* 可在Repository把一些重複的程式碼消掉
* 例如fetch,fetchAll之類幫你包好,在service段呼叫就可以更一致化

# L5 Repository Transformer
* `composer require league/fractal`
* for API 好用

# CSP(可以理解成 網頁資源加載白名單)
* [link](http://www.ruanyifeng.com/blog/2016/09/csp.html)
* 可防XSS

# 連線池　DB Connection thread Pool
[link](http://peggg327.blogspot.com/2014/11/connection-pool.html)
* DB Connection 的建立成本是昂貴的，故當有許多Thread都需要建立connection時，其資源的耗費是龐大的。
##### 對於最佳 Pool 用例，需要考慮一些經驗法則：
* 變量`Threads_running ` == 當前在MySQL服務器中執行的 `並發語句數量`
```
//對於InnoDB工作負載通常超過40
if (Threads_running　始終 > 服務器無法以最佳方式運行的區域) 
  則 Pool 將是`good`的，尤其是在極端 並行 過載情況下。
}
```
* if您使用 innodb_thread_concurrency 限制 `並發執行語句`的數量，
您會發現 Pool 通過為線程組分配連接，然後根據`Transaction事務`內容，用戶定義的名稱等排隊執行，只能更好地解決相同的問題。
* 最後，如果您的工作負載主要包含短查詢，則 Pool 將是有益的。


# Isolation Levels(transaction的隔離機制種類)
* [link](https://medium.com/getamis/database-transaction-isolation-a1e448a7736e)
* 事務隔離級別越高,安全性越good,but是並發就越bad
* 用這個指令`SET SESSION transaction_isolation='SERIALIZABLE';`
1. Read Uncommitted
2. Read Committed
3. Repeatable Read
4. Serializable

# Serializable (一種Isolation Levels)
* (決定多個transaction的演算法)
* 資料庫引擎必須要能在2個以上的交易同時進行時，自動決定能將兩個交易都順利完成的排程順序，
這個排程演算法 (scheduling algorithm) 是交易隔離層次機制實作的其中一個部份，決定排程的動作
* 為了解決 Serialization failure: 1213 Deadlock found when trying to get lock; try restarting transaction in /var
/www/html/techsolutions/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOStatement.php:141

# Transaction相關
* `Dirty Read`
  * 如果一個 transaction 還沒有 commit，但是你卻讀得到已經更新的結果，
* `Phantom reads`
  * 當在同一個 transaction 連續兩次讀取時，讀取出來的筆數跟上次不同，這個情況稱為 Phantom reads。

# Facade Pattern (外觀模式)
* laravel用一個類別的靜態函式,包裝呼叫物件的函式(可讓你少打很多字)
* 用 `::method` 取代 `new $obj() + $obj->method()`
* [link](http://blog.turn.tw/?page_id=875)

# `sync同步`,`async非同步`,`blocking阻塞`,`non-blocking非阻塞`,`Concurrent併發`,`Parallelis並行`
[link](https://tw.saowen.com/a/ee53cd13f1a8fac04b5ae00cf31eaa2c1789748f5e9e3d2c95b49e4406552f64)
* sync同步: 一件事情解決,有結果,才能做下一件
* async非同步: 先讓事情進行,不等結果,就可以做下一件 (Goroutine就是這樣先做,再去chan收割結果)
* blocking阻塞: 呼叫結果返回之前，當前thread會被掛起。呼叫執行緒只有在得到結果之後才會返回
* non-blocking非阻塞: 指在不能立刻得到結果之前，該呼叫不會阻塞當前執行緒。
* Concurrent併發: cpu*1 只是來回切換很快
* Parallelism並行: cpu*N 名副其實的同時進行
###### 也就是說，
* `同步/非同步`是“事情完成訊息”通知的方式（機制），
* `阻塞/非阻塞`則是在wait“事情完成訊息”通知過程中的狀態（能不能幹其他任務），
* 在不同的場景下，同步/非同步、阻塞/非阻塞的四種組合都有應用。

# `異步IO`,`同步IO`
是計算機OS對輸入輸出的處理方式：
* `異步IO` : 發起IO請求的 thread 不等IO操作完成，就繼續執行隨後的代碼，IO結果用其他方式通知發起IO請求的程序。
* `同步IO` : 發起IO請求的 thread 不從 `正在調用的IO操作函數`返回（即被阻塞），直至IO操作完成。

# php-fpm , fastCGI ,php-CGI , CGI
* php-fpm: 管理`多個fastCGI`的`process管理器`(性能好)
* fastCGI: process常駐型,常駐在`RAM`所以性能比較好
* PHP-CGI :就是PHP實現的自帶的FastCGI的管理器(性能差)
* CGI: process需要開開關關的,怕大規模併發

# CGI
* (process需要開開關關的(`fork-and-execute模式`),怕大規模併發)
```
cgi就是專門用來和web服務器打交道的.
	
if(web服務器 收到 用戶request){
	cgi程序(用戶request);	//cgi程序(php的fastcgi)
}
	
根據request提交的參數應處理（解析php），
然後 輸出標準的HTML語句 返回給 web服務器，
再response給client，這就是普通的CGI的工作原理。
```

# FastCGI
* process常駐型,常駐在`RAM`所以性能比較好

# php-cgi + cgi VS php-fpm + fastCGI
[link](https://www.awaimai.com/371.html)
* CGI來說，每個request,PHP都必須重新解析php.ini文件，重新載入全部extension，並重新初始化全部數據結構。
* FastCGI，所有這些都只在process啟動時發生一次。一個額外的好處 是，持續DB連接（持久性數據庫連接）可以工作。
* 由於FastCGI是`多process`，所以比CGI`多thread`消耗更多的RAM，PHP-CGI解釋器每進程消耗7至25兆RAM，將這個數字乘以50或100就是很大的RAM數。

# node.js
* 是`單thread`。好處就是
1. 簡單
2. 高性能，避免了頻繁的thread切換(contextSwitch)開銷
3. 占用資源小，因為是單thread，在大負荷情況下，對ram占用仍然很低
4. 單thread安全，沒有加鎖、解鎖、死鎖這些問題

* 壞處就是
如何解決高並發？
node使用`async IO(異步IO)`和`事件驅動`（callback函數）來解決這個問題。

一般來說，高並發解決方案會提供`多thread`模型，為每個業務邏輯提供一個thread，通過contextSwitch來來彌補同步I/O調用的時間開銷。  
像apache，是一個request一個thread。

而node.js使用的是`單thread`模型，對所有I/O都採用`async異步`的請求方式，避免頻繁的contextSwitch，  
在node.js執行的時候維護著一個`事件queue`；程序在執行時進入事件循環等待下一個事件到來，每個異步I/O請求完成後都會被推送到`事件queue中的等待執行。

# nodeJs處理流程
eventEmitters > eventQueue > eventLoop > eventHandler

# 非同步編程
整個程式只有一個 eventQueue  
async 的 function 都會被塞到 eventQueue  
系統有空就開始跑 eventQueue  
裡面的所有任務會輪流跑  
跑完就呼叫 callback  

# serverless
* AWS Lambda就是
  * 早期的網頁程式是整個包成一包，而將我們寫好的這一包程式，放在網頁上後去執行；  
  * 但Serverless的情況是，，最小的單位稱為Function，每一個Function，負責一段商業邏輯。
```
ex:
假設我們的網站有100個功能，早期會將這100個功能包成一包，放到一台server上，
但Serverless的情況變成，我們要分別將100個功能，分成100包檔案，佈署到100個Lambda Server上
```

# VPC(AWS服務)
Amazon VPC 讓您能夠在 Amazon Web Services (AWS) 雲端佈建一個在邏輯上隔離的部分，以在自己定義的虛擬網路中啟動 AWS 資源。  
您可以完全掌控虛擬聯網環境，包括選擇自己的 IP 地址範圍、建立子網路，以及設定路由表和網路閘道。  
您也可以在公司資料中心和 VPC 之間建立硬體虛擬私人網路 (VPN) 連接，將 AWS 雲端當成公司資料中心的延伸。  

# 二元樹的遍歷Binary Tree Traversal
* DFS  (深度優先)
  * Preorder Traversal 前序遍歷(Root排在前面)  
理論上的遍歷順序是Root、L子樹、R子樹。  
即是Depth-first Search。  

  * Inorder Traversal 中序遍歷(Root排在中間)  
理論上的遍歷順序是：L子樹、Root、R子樹。  
實際上是採用Depth-first Search，只不過更動了節點的輸出順序。  

  * Postorder Traversal 後序遍歷(Root排在後面)  
理論上的遍歷順序是：L子樹、R子樹、Root。  
實際上是採用Depth-first Search，只不過更動了節點的輸出順序。  

* BFS  (廣度優先)
  * Level-order Traversal 層序遍歷
即是Breath-first Search。
遍歷順序，理論上總共四種 ── 但是事實上還是只有  

# JWT
* 是一種認證協議 (安全標準)
JWT提供了一種用於發佈接入令牌（Access Token),並對發佈的簽名接入令牌進行驗證的方法。  
令牌（Token）本身包含了一系列聲明，應用進程可以根據這些聲明限制用户對資源的訪問。
```
//基本思路就是
client提供username和password給`認證server`，
server驗證用户提交信息信息的合法性；

if(驗證成功){
	會產生並返回一個`Token令牌`
	client可以使用這個`Token令牌`訪問服務器上受保護的資源。
}
```

# OAuth2
* 是一種授權框架 
另一方面，OAuth2是一種授權框架，提供了一套詳細的授權機制（指導）。用户或應用可以通過公開的或私有的設置，授權第三方應用訪問特定資源。

```
它詳細描述了系統中不同角色、用户、服務前端應用（比如API），以及客户端（比如網站或移動App）之間怎麼實現相互認證。
```

# php ob函示


# Kubernetes(簡稱K8S)
```
K8S cluster == Master + []Node
Node == []Pod
Pod(部署App最小單位) == []Volume + []ContainerizedApp
```
* Master
* Nodes

# MYSQL 連線機制
```
if($mysql < 5.6) 
  一個連線一個thread(不斷銷毀建立)
else
  thread pool 
```

# Linux程序呼叫(fork-and-exec流程)  
程序都會藉由 `父process` 以複製 (fork) 的方式產生一個一模一樣的`子process`，  
then 被複製出來的`子process`再以 exec 的方式來執行實際要進行的程式，  
最終就成為一個`子process`的存在。


# OKR(Google服務)

# OOCSS
* OOCSS 不是什麼新技術，只是一種撰寫 CSS 的設計模式，或者可以說是一種「道德規範  
減少對 HTML 結構的依賴 . 
增加 CSS class 重複性的使用 . 


# 消息佇列
* `AMQP`一種協議
* `RabbitMQ` 
* 解決 多system、異構系統間的 `數據交換`,`消息通知`,`通訊`
### 架構
* Broker:它提供一種傳輸服務,它的角色就是維護一條從生產者到消費者的路線，保證數據能按照指定的方式進行傳輸, 
* Exchange：消息交換機,它指定消息按什麼規則,路由到哪個隊列。 
* Queue:消息的載體,每個消息都會被投到一個或多個隊列。 
* Binding:綁定，它的作用就是把exchange和queue按照路由規則綁定起來. 
* Routing Key:路由關鍵字,exchange根據這個關鍵字進行消息投遞。 
* vhost:虛擬主機,一個broker裏可以有多個vhost，用作不同用户的權限分離。 
* Producer:消息生產者,就是投遞消息的process. 
* Consumer:消息消費者,就是接受消息的process. 
* Channel:消息信道,在客户端的每個連接裏,可創建多個channel.


# VueJS相關
* `Vuido`:用vue寫桌面應用


# failover (故障轉移機制)
HA 架構 master 死掉 slave 頂替


# AWS RI(`Reserved Instances預留執行個體`)
* 預定機器會使用一年,有點像綁約,比較便宜

# CPU 時間片
* 時間片`timeslice`  == 處理器片`processor slice`
* 從process開始執行 ~ 被搶占的時間

# js Generator
* 跟PHP的一樣
* js使用場景:使`非同部`代碼看起來跟`同步`一樣

# 監督式 非監督式(機器學習)

# Dojo 2
[link](https://www.ithome.com.tw/news/122897)

# 目錄結構 /var/run
[link](https://www.jb51.net/article/137999.htm)

# 主機監控工具
* smokeping
* cacti
* nagios
* zabbix
* ntop

# rsync
* Linux 系統上最常被用來複製與備份檔案的工具

# is-a (是一種)

# `敏捷式` & `瀑布式` 開發
* `敏捷式`Agile Methodology
* `瀑布式`Waterfall
* 敏捷式UX(Agile UX)
* Lean UX

# LLVM 
* 是low level virtual machine的簡稱，其實是一個編譯器框架

# 語言虛擬機
[為什麼需要 JVM](https://openhome.cc/Gossip/JavaEssence/WhyJVM.html)
[HHVM](http://wuduoyi.com/note/hhvm/)

# zend
#### Zend 的執行過程可以分為2部分
* 將 PHP 編譯為 opcode
* 執行 opcode

# 外鍵
* 用來保證資料的完整性和一致性
* 會使速度和性能`下降`
