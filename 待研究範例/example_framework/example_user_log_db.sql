-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: 2016 年 06 月 09 日 08:15
-- 伺服器版本: 5.1.73
-- PHP 版本: 5.5.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `example_user_log_db`
--

-- --------------------------------------------------------

--
-- 資料表結構 `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `sesskey` varchar(64) NOT NULL DEFAULT '',
  `expiry` datetime NOT NULL,
  `expireref` varchar(250) DEFAULT '',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `sessdata` longtext,
  PRIMARY KEY (`sesskey`),
  KEY `sess2_expiry` (`expiry`),
  KEY `sess2_expireref` (`expireref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'log 流水號',
  `table_name` varchar(64) NOT NULL DEFAULT '' COMMENT '表格名稱',
  `table_id` varchar(32) NOT NULL DEFAULT '' COMMENT '表格流水號',
  `table_func` varchar(32) NOT NULL DEFAULT '' COMMENT '功能',
  `table_action` varchar(32) NOT NULL DEFAULT '' COMMENT '動作',
  `user_no` int(10) unsigned NOT NULL COMMENT '管理者流水號',
  `modifiedtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改時間',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理者LOG' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
