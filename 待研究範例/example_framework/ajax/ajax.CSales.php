<?php
include_once('../inc/config.php');
include_once('../inc/class.session.php');
include_once('../inc/CMisc.php');

include_once('../inc/model/CUser.php');
include_once('../inc/model/CGalaxyClass.php');

$session	= new session($_GET['PHPSESSID']);
CGalaxyClass::$session =$session;	//insert to basic class static member

if(is_null($session->get('oCurrentUser'))){
	exit(false);
}


//quotes info from client
$_GET = CMisc::my_quotes($_GET);
$_POST = CMisc::my_quotes($_POST);


include_once('../inc/model/CSales.php');

$name = $_POST['text'];
$limit = "20";
$aAllSales = CSales::aAllSales("`sales_name` LIKE ? ",array("%$name%"),"LIMIT $limit");
foreach ($aAllSales as $oCSales) {
	$aMap = array(	'sales_no'=>$oCSales->iSalesNo,
					'sales_name'=>"{$oCSales->oCDealer->sCode}{$oCSales->oCDealer->sName}-{$oCSales->sName}"
					);
	$aReturn[] = $aMap;
}
if($aReturn)
	exit(json_encode($aReturn));


exit(false);
?>