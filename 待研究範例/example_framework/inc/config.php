<?php

/**
 * @desc config 設定檔
 * @created 2016/08/27
 */

// config_stable.php 測試機
// config_release.php 正式機
// config_[xxx].php 個人設定檔案

defined('PATH_ROOT')|| define('PATH_ROOT', realpath(dirname(__FILE__) . '/..'));

$sFileName = PATH_ROOT."/env.php";
if (file_exists($sFileName)){
	include_once($sFileName);
} else {
	die("File Not Exist env.php. See config.php");
	//vCreateEnvPhp($sFileName);
	//include_once($sFileName);
}

if (!file_exists(PATH_ROOT."/inc/config")){
    die("Directory /inc/config/ not exist.");
}
if (!file_exists(PATH_ROOT."/inc/config/config_".ENVIRONMENT.".php")){
    die("File not exist, /inc/config/config_".ENVIRONMENT.".php not exist.");
}
include_once(PATH_ROOT."/inc/config/config_".ENVIRONMENT.".php");

function vCreateEnvPhp($sFileName=''){
	if (!$sFileName) return FALSE;
	$fp = fopen($sFileName, 'w');
	fwrite($fp, '<?php define("ENVIRONMENT", "development"); ?>');
	fclose($fp);
}

?>