<?php

/********************************************************************
 * @heading(標題):
 *			繼承 CGalaxyClass
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			繼承 CGalaxyClass
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) : 繼承 CGalaxyClass
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once(PATH_ROOT.'/inc/model/CGalaxyClass.php');

class CGalaxyController extends CGalaxyClass
{
	static public $Smarty;

	public function __construct(){

	}
}
?>