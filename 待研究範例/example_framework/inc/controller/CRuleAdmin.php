<?php

/********************************************************************
 * @heading(標題):
 *			CRuleAdmin 權限
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			權限
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) : 權限
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

//include model.php
include_once('../inc/model/CRule.php');

class CRuleAdmin extends CGalaxyController
{

	/*
		exception code of this controller
	*/
	const BACK_TO_LIST = 1;
	const BACK_TO_VIEW = 2;
	const BACK_TO_EDIT = 3;

	private $sDBName = 'EXAMPLE';
	
	/*
		constructor of this controller
	*/
	public function __construct(){
	}


	static public $aSearchOption = array(
		'rule_name' => '名稱',
		'rule_desc' => '描述',
		'func'      => 'func名稱',
		'action'    => 'action名稱'
	);

	/*
		entry of this controller
		handle exception and decide where to redirect by exception code
	*/
	/**
	 * @desc 權限進入的頁面動作 
	 * @created 2017/02/24
	 */	
	public function tManager() {
		$action = isset($_GET['action'])?$_GET['action']:'';
		try{
			switch($action){
				case "add":
					return $this->tRuleAdd();
					break;
				case "edit":
					return $this->tRuleEdit();
					break;
				case "active":
					return $this->vRuleActive();
					break;
				default:
				case "search":
				case "list":
					return $this->tRuleList();
					break;		
			}
		}catch (Exception $e){
			switch($e->getCode()){
				case self::BACK_TO_LIST:
					$sUrl = $_SERVER['PHP_SELF'].'?func='.$_GET['func'];
					if(isset($_GET['rule_no']))
						$sUrl .= '&action=list&goid='.$_GET['rule_no'];
					CJavaScript::vAlertRedirect($e->getMessage(),$sUrl);
					break;
				case self::BACK_TO_EDIT:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF'].'?func='.$_GET['func'].'&action=edit&rule_no='.$_GET['rule_no']);
					break;
				default:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF']);
					break;
			}
		}
		exit;
	}

	/*
		add rule
	*/
	/**
	 * @desc 新增權限 
	 * @created 2017/02/24
	 */
	private function tRuleAdd(){
		$Smarty = self::$Smarty;
		if(empty($_POST)){
			$Smarty->assign('aCategory',CRule::aAllRuleInCategory());
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/rule_add.html');
		}else{
			//use post data to create a $oCRule
			$oCRule = new CRule($_POST);	//if $_POST has all we need
			//add
			try{
				$oCRule->iAddRule();	//$oCRule->iRuleNo will be changed to insert id
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_LIST);
			}
		}
		CJavaScript::vAlertRedirect('',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCRule->iRuleNo}");
	}
		/*
		edit rule
	*/
	/**
	 * @desc 編輯權限 
	 * @created 2017/02/24
	 */	
	private function tRuleEdit(){
		$Smarty = self::$Smarty;
		if(empty($_GET['rule_no']))
			throw new Exception('no such rule',self::BACK_TO_LIST);
		$iRuleNo = $_GET['rule_no'];
		$oCRule = CRule::oGetRule($iRuleNo);
		if(empty($_POST)){
			$Smarty->assign('oCRule',$oCRule);
			$Smarty->assign('aCategory',CRule::aAllRuleInCategory());
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/rule_edit.html');
		}else{
			$oCRuleUpdate = new CRule($_POST);
			$oCRule->vOverWrite($oCRuleUpdate);	//overwrite
			//update
			try{
				$oCRule->vUpdateRule();
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_LIST);
			}
		}
		CJavaScript::vAlertRedirect('',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCRule->iRuleNo}");
	}

	/*
		activate/deactivate rule
	*/
	/**
	 * @desc 編輯權限狀態 
	 * @created 2017/02/24
	 */	
	private function vRuleActive(){
		if(empty($_GET['rule_no']))
			throw new Exception('no rule',self::BACK_TO_LIST);
		$iRuleNo = $_GET['rule_no'];
		$oCRule = CRule::oGetRule($iRuleNo);

		try{
			$oCRule->vActivate();
		}catch (Exception $e){
			throw new Exception($e->getMessage(),self::BACK_TO_LIST);
		}
		CJavaScript::vAlertRedirect('',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCRule->iRuleNo}");
	}
	

	/*
		list of rules
	*/
	/**
	 * @desc 權限列表 
	 * @created 2017/02/24
	 */	
	private function tRuleList(){
		$Smarty = self::$Smarty;
		$session = self::$session;
		$oDB = self::oDB($this->sDBName);
		
		if(!empty($_POST)) {
			$js_valid = isset($_GET['js_valid'])?$_GET['js_valid']:0;
			if($js_valid==1) {
				$this->vaildRuleSearch($_POST,1);	//client javascript vaild data
			}else{
				$this->vaildRuleSearch($_POST,0);	//form submit vaild data
			}
		}

		if(empty($_GET['items'])) $iPageItems = PAGING_NUM;
		else $iPageItems = $_GET['items'];

		if(empty($_GET['order'])) $sOrder = "rule_no";
		else $sOrder = $_GET['order'];
		
		if(empty($_GET['sort'])) $sSort = "DESC";
		else $sSort = $_GET['sort'];
		
		if(empty($_GET['page'])) $iPg = 0;
		else $iPg = $_GET['page'];
		
		if(empty($_GET['goid'])) $goid = 0;
		else $goid = $_GET['goid'];

		$aSearchSql = array();
		if(isset($_GET['search']) AND $_GET['search']==='1'){
			$aSearchSql = $this->aGetSearchSql($_POST);
			$sSearchSql = $this->sGetSearchSql($_POST);
			

		}else
			$sSearchSql ='';
		//得到某筆資料是在第幾頁
		if($goid){
			if($sSearchSql!=='') $sWhereSql = "WHERE $sSearchSql";	//no default filter
            $iPg = $oDB->iGetItemAtPage("example_rule","rule_no",$goid,$iPageItems,$sSearchSql,"ORDER BY $sOrder $sSort");
            // $iPg = $oDB->iGetItemAtPage("example_rule","rule_no",$goid,$iPageItems,$sSearchSql,$aSearchSql,"ORDER BY $sOrder $sSort");
		}

		//共幾筆
		$iAllItems = CRule::iGetCount($sSearchSql,$aSearchSql);
		$iStart=$iPg*$iPageItems;

		if($iAllItems!==0){
			$sPostFix = "ORDER BY $sOrder $sSort LIMIT $iStart,$iPageItems";	//sql postfix
			$Smarty->assign("aCRule",CRule::aAllRule($sSearchSql,$aSearchSql,$sPostFix));
		}

		$action = isset($_GET['action'])?$_GET['action']:'';

		//assign frame attribute
		$Smarty->assign("NowOrder",$sOrder);		
		$Smarty->assign("NowSort",$sSort);
		
		$Smarty->assign("OrderUrl",$_SERVER['PHP_SELF']."?func=".$_GET['func']."&action=".$action."&page=$iPg");
		$Smarty->assign("OrderSort",(strtoupper($sSort)=="DESC")?"ASC":"DESC");

		$Smarty->assign('searchKey',	$session->get("s_rule_key") );
		$Smarty->assign('searchTerm',	$session->get("s_rule_terms") );
        
        $Smarty->assign('searchOption',	self::$aSearchOption);

		$Smarty->assign("Total",$iAllItems);
		$Smarty->assign("PageItem",$iPageItems);
		
		$Smarty->assign("StartRow",$iStart+1);
		$Smarty->assign("EndRow",$iStart+$iPageItems);

		$Smarty->assign("iPg",$iPg);
		$Smarty->assign('PageBar',	CMisc::sMakePageBar($iAllItems, $iPageItems, $iPg, "func=".$_GET['func']."&action=".$action."&order=$sOrder&sort=$sSort"));
		
		return $output = $Smarty->fetch('./admin/'.get_class($this).'/rule_list.html');
		
	}

	/*
		check if the search string is vaild
	*/
	/**
	 * @desc 權限查詢
	 * @created 2017/02/24
	 */	
	private function vaildRuleSearch($postData=array(),$return_type=0){
		
		$aErrorMsg = array();
		
		if(strlen(trim($postData['s_key'])) == 0){
			$aErrorMsg[]=_LANG_RULE_VAILD_SEARCH_KEY;
		}	
		$sErrorMsg = "";

		//client javascript vaild data
		if($return_type==1) {
			$sErrorMsg = implode("<BR>",$aErrorMsg);
			echo $sErrorMsg;
			exit;
		}
		//form submit vaild data
		if(count($aErrorMsg) > 0){
			$sErrorMsg = implode('\n',$aErrorMsg);
			throw new Exception(sprintf($sErrorMsg),self::BACK_TO_LIST);
		}
	}

	/*
		change search rule name into sql string
	*/
	/**
	 * @desc 權限搜尋關鍵字sql
	 * @created 2017/02/24
	 */ 
	private function sGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_rule_key");
			$sTerms =  $session->get("s_rule_terms");
		}	
		$sSql = "";
		
		if(!$sKey) {
			$session->set("s_rule_key","");
			$session->set("s_rule_terms","");
			return $sSql;
		}
		$session->set("s_rule_key",$sKey);
		$session->set("s_rule_terms",$sTerms);
		
		switch($sTerms){
			default :
				// $sSql = $sSql." ($sTerms LIKE ?)";
				$sSql = $sSql." ($sTerms LIKE '%".$sKey."%')";
				break;
		}

		return $sSql;
	}
	/**
	 * @desc 權限搜尋關鍵字sql
	 * @created 2017/02/24
	 */
	private function aGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_rule_key");
			$sTerms =  $session->get("s_rule_terms");
		}	
		$sSql = "";
		
		if(!$sKey) {
			$session->set("s_rule_key","");
			$session->set("s_rule_terms","");
			return $sSql;
		}
		$session->set("s_rule_key",$sKey);
		$session->set("s_rule_terms",$sTerms);
		
		return array("%$sKey%");
	}
}
?>