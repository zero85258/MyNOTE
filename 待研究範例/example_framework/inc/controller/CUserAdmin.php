<?php


/********************************************************************
 * @heading(標題):
 *			CUserAdmin 人員
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			人員動作
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) :
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/controller/CGalaxyController.php');
//include model.php
include_once('../inc/model/CUser.php');

class CUserAdmin extends CGalaxyController
{
	
	/*
		exception code of this controller
	*/
	const BACK_TO_LIST = 1;
	const BACK_TO_VIEW = 2;
	const BACK_TO_EDIT = 3;

	private $sDBName = 'EXAMPLE';

	static public $aSearchOption = array(	"example_user.user_name" 	=> '名稱',
											"example_user.user_account" 	=> '帳號',	
											"example_user.user_no"		=> '序號'
											);

	/*
		constructor of this controller
	*/
	public function __construct(){
	}

	/*
		entry of this controller
		handle exception and decide where to redirect by exception code
	*/

	/**
	 * @desc 人員進入的動作 
	 * @created 2017/02/24
	 */

	public function tManager() {
		$action = isset($_GET['action'])?$_GET['action']:'';
		try{
			switch($action){
				case "add":
					return $this->tUserAdd();
					break;
				case "edit":
					return $this->tUserEdit();
					break;
				case "selfedit":
					return $this->tSelfEdit();
					break;
				case "active":
					return $this->vUserActive();
					break;
				case "del": // 刪除人員
					return $this->vUserDel();
					break;
				default:
				case "search":
				case "list":
					return $this->tUserList();
					break;		
			}
		}catch (Exception $e){
			switch($e->getCode()){
				case self::BACK_TO_LIST:
					$sUrl = $_SERVER['PHP_SELF'].'?func='.$_GET['func'];
					if(isset($_GET['user_no']))
						$sUrl .= '&action=list&goid='.$_GET['user_no'];
					CJavaScript::vAlertRedirect($e->getMessage(),$sUrl);
					break;
				case self::BACK_TO_EDIT:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF'].'?func='.$_GET['func'].'&action=edit&user_no='.$_GET['user_no']);
					break;
				default:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF']);
					break;
			}
		}
		exit;
	}

	/*
		add an iwant user
	*/

	/**
	 * @desc 新增人員 
	 * @created 2017/02/24
	 */
	private function tUserAdd(){
		$Smarty = self::$Smarty;
		if(empty($_POST)){
			$Smarty->assign('aAllGroup',CGroup::aAllGroup());
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/user_add.html');
		}else{
			//use post data to create a $oCUser
	
			$oCUser = new CUser($_POST);	//if $_POST has all we need
			$oCUser->vChangePassword($_POST['new_password']);

			//add
			try{
				$oCUser->iAddUser();	//$oCUser->iUserNo will be changed to insert id
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_LIST);
			}
		}
		CJavaScript::vAlertRedirect('user add success',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCUser->iUserNo}");
	}

	/*
		edit an iwant user
	*/
	/**
	 * @desc 編輯人員 
	 * @created 2017/02/24
	 */
	private function tUserEdit(){
		$Smarty = self::$Smarty;
		if(empty($_GET['user_no']))
			throw new Exception('',self::BACK_TO_LIST);
		$iUserNo = $_GET['user_no'];
		$oCUser = CUser::oGetUser($iUserNo);

		if(empty($_POST)){
			$Smarty->assign('oCUser',$oCUser);
			$Smarty->assign('aUserGroup',$oCUser->aGroup());
			$Smarty->assign('aAllGroup',CGroup::aAllGroup());
			// echo "<PRE>";print_r(CGroup::aAllGroup());exit;
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/user_edit.html');
		}else{
			if($_POST['user_no']!==$iUserNo)
				throw new Exception('GET and POST not match',self::BACK_TO_EDIT);
			//change the value in $oCUser
			$oCUser->sName = $_POST['user_name'];

			$oCUser->bStatus = ($_POST['status'])?"1":"0";
			$oCUser->sEmail = $_POST['user_email'];
			$oCUser->sTel = $_POST['user_tel'];
			$oCUser->sFax = isset($_POST['user_fax'])?$_POST['user_fax']:'';
			$oCUser->sMobile = $_POST['user_mobile'];

			//set groups
			// echo  "<pre>";print_r($_POST['group']);
			$group	= isset($_POST['group'])?$_POST['group']:'';
			$oCUser->vSetGroups($group);
			//change password if $_POST['password'] $_POST['password2'] is given
			if($_POST['new_password'] && $_POST['confirm_password'] && $_POST['new_password']===$_POST['confirm_password'])
				$oCUser->vChangePassword($_POST['new_password']);
			//update
			try{
				$oCUser->vUpdateUser();
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_EDIT);
			}
		}
		CJavaScript::vAlertRedirect('user edit success',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCUser->iUserNo}");
	}

	/*
		edit current user selfdata
		allow changing password
	*/
	/**
	 * @desc 編輯該者資料 頁面
	 * @created 2017/02/24
	 */	
	private function tSelfEdit(){
		$Smarty = self::$Smarty;
		$session = self::$session;
		$oCUser = $session->get('oCurrentUser');
		$user_no = $oCUser->iUserNo;
		if(empty($_POST)){
			$Smarty->assign('oCUser',$oCUser);
			$Smarty->assign('aAllGroup',CGroup::aAllGroup());
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/user_self_edit.html');
		}else{
			// //change the value in $oCUser
			// $oCUser->sName = $_POST['user_name'];
			// // $oCUser->sAccount = $_POST['user_account'];
			// $oCUser->bStatus = $_POST['status'];
			// $oCUser->sEmail = $_POST['user_email'];
			// $oCUser->sTel = $_POST['user_tel'];

			// $oCUser->sMobile = $_POST['user_mobile'];
			
			// $oCUser->iDeptNo = isset($_POST['dept_no'])?$_POST['dept_no']:'';

			$UserData['user_name'] 		= $_POST['user_name'];
			$UserData['user_account'] 	= isset($_POST['user_account'])?$_POST['user_account']:'';
			$UserData['user_tel']		= isset($_POST['user_tel'])?$_POST['user_tel']:'';
			$UserData['user_mobile']	= isset($_POST['user_mobile'])?$_POST['user_mobile']:'';
			$UserData['user_email']		= isset($_POST['user_email'])?$_POST['user_email']:'';
			//change password if $_POST['password'] $_POST['password2'] is given
			if($_POST['new_password'] && $_POST['confirm_password'] && $_POST['new_password']===$_POST['confirm_password'])
				$oCUser->vChangePassword($_POST['new_password']);
			//update
			try{
				// $oCUser->vUpdateUser();
				CUser::UpdateUser($user_no, $UserData);
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_LIST);
			}
			CJavaScript::vAlertRedirect('self edit success',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCUser->iUserNo}");
		}
	}

	/*
		active/deactive an iwant user
	*/
	/**
	 * @desc 編輯人員狀態 
	 * @created 2017/02/24
	 */	
	private function vUserActive(){
		if(empty($_GET['user_no']))
			throw new Exception('',self::BACK_TO_LIST);
		$iUserNo = $_GET['user_no'];
		$oCUser = CUser::oGetUser($iUserNo);
		try{
			$oCUser->vActivate();
		}catch (Exception $e){
			throw new Exception($e->getMessage(),self::BACK_TO_LIST);
		}
		CJavaScript::vAlertRedirect('active success',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCUser->iUserNo}");
	}

	/*
		delete an iwant user
	*/
	private function tUserDelete(){
		
	}

	/*
		list of iwant users
	*/
	/**
	 * @desc 列出人員資料 
	 * @created 2017/02/24
	 */	
	private function tUserList(){
		$Smarty = self::$Smarty;
		$session = self::$session;
		$oDB = self::oDB($this->sDBName);
		if(!empty($_POST)) {
			$js_valid = isset($_GET['js_valid'])?$_GET['js_valid']:0;
			if($js_valid==1){
				$this->vaildUserSearch($_POST,1);	//client javascript vaild data
			}else{
				$this->vaildUserSearch($_POST,0);	//form submit vaild data
			}

			// if($_GET['js_valid']==1) {
			// 	$this->vaildUserSearch($_POST,1);	//client javascript vaild data
			// }else{
			// 	$this->vaildUserSearch($_POST,0);	//form submit vaild data
			// }
		}
		if(empty($_GET['items'])) $iPageItems = PAGING_NUM;
		else $iPageItems = $_GET['items'];

		//because this function join two tables, order must be full name with table, caution in tpl!
		if(empty($_GET['order'])) $sOrder = "createtime";
		else $sOrder = $_GET['order'];
		
		if(empty($_GET['sort'])) $sSort = "DESC";
		else $sSort = $_GET['sort'];
		
		if(empty($_GET['page'])) $iPg = 0;
		else $iPg = $_GET['page'];
		
		if(empty($_GET['goid'])) $goid = 0;
		else $goid = $_GET['goid'];
		
		$aSearchSql = array();
		$action = isset($_GET['action'])?$_GET['action']:'';
		if($action === 'search'){
			$aSearchSql = $this->aGetSearchSql($_POST);
			$sSearchSql = $this->sGetSearchSql($_POST);
			

		}else
			$sSearchSql ='';

		//得到某筆資料是在第幾頁
		if($goid){
            $iPg = $oDB->iGetItemAtPage("example_user","user_no",$goid,$iPageItems,$sSearchSql,"ORDER BY $sOrder $sSort");
            // $iPg = $oDB->iGetItemAtPage("example_user","user_no",$goid,$iPageItems,$sSearchSql,$aSearchSql,"ORDER BY $sOrder $sSort");
		}
		
		//共幾筆
		$iAllItems = CUser::iGetCount($sSearchSql,$aSearchSql);
		$iStart=$iPg*$iPageItems;

		//get objects
		if($iAllItems!==0){
			$sPostFix = "ORDER BY $sOrder $sSort LIMIT $iStart,$iPageItems";	//sql postfix
			$aCUsers = CUser::aAllUser($sSearchSql,$aSearchSql,$sPostFix);
			$Smarty->assign("aCUsers",$aCUsers);
			
			if(count($aCUsers)===0)
				CJavaScript::vAlertRedirect("",$_SERVER['PHP_SELF']."?func=".$_GET['func']."&action=list&items=".$_GET['items']);
		
		}
        

		//assign frame attribute
		$Smarty->assign("NowOrder",$sOrder);		
		$Smarty->assign("NowSort",$sSort);
		
		$Smarty->assign("OrderUrl",$_SERVER['PHP_SELF']."?func=".$_GET['func']."&action=".$action."&page=$iPg");
		$Smarty->assign("OrderSort",(strtoupper($sSort)=="DESC")?"ASC":"DESC");

		$Smarty->assign('searchKey',	$session->get("s_user_key") );
		$Smarty->assign('searchTerm',	$session->get("s_user_terms") );
		$Smarty->assign('searchOption',	self::$aSearchOption);
                
		$Smarty->assign("Total",$iAllItems);
		$Smarty->assign("PageItem",$iPageItems);
		
		$Smarty->assign("StartRow",$iStart+1);
		$Smarty->assign("EndRow",$iStart+$iPageItems);

		$Smarty->assign("iPg",$iPg);
		$Smarty->assign('PageBar',	CMisc::sMakePageBar($iAllItems, $iPageItems, $iPg, "func=".$_GET['func']."&action=".$action."&order=$sOrder&sort=$sSort"));

		return $output = $Smarty->fetch('./admin/'.get_class($this).'/user_list.html');
	}

	/*
		check if the search string is vaild
	*/
	/**
	 * @desc搜尋人員資料 
	 * @created 2017/02/24
	 */	
	private function vaildUserSearch($postData=array(),$return_type=0){

		$aErrorMsg = array();
		
		if(strlen(trim($postData['s_key'])) == 0){
			$aErrorMsg[]=_LANG_RULE_VAILD_SEARCH_KEY;
		}	
		$sErrorMsg = "";

		//client javascript vaild data
		if($return_type==1) {
			$sErrorMsg = implode("<BR>",$aErrorMsg);
			echo $sErrorMsg;
			exit;
		}
		//form submit vaild data
		if(count($aErrorMsg) > 0){
			$sErrorMsg = implode('\n',$aErrorMsg);
			throw new Exception(sprintf($sErrorMsg),self::BACK_TO_LIST);
		}
	}

	/*
		change search user name into sql string
	*/ 
	/**
	 * @desc 取出搜尋關鍵字的sql
	 * @created 2017/02/24
	 */
	private function sGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_user_key");
			$sTerms =  $session->get("s_user_terms");
		}	
		$sSql = "";
		
		if(!$sKey) {
			$session->set("s_user_key","");
			$session->set("s_user_terms","");
			return $sSql;
		}
		$session->set("s_user_key",$sKey);
		$session->set("s_user_terms",$sTerms);
		
		switch($sTerms){
			default :
				$sSql = $sSql." ($sTerms LIKE '%".$sKey."%')";
				break;
		}
		return $sSql;
	}
	/**
	 * @desc 取出搜尋關鍵字 
	 * @created 2017/02/24
	 */
	private function aGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_user_key");
			$sTerms =  $session->get("s_user_terms");
		}	
		
		
		if(!$sKey) {
			$session->set("s_user_key","");
			$session->set("s_user_terms","");
			return array();
		}
		$session->set("s_user_key",$sKey);
		$session->set("s_user_terms",$sTerms);
		
		return array("%$sKey%");
	}
}
?>