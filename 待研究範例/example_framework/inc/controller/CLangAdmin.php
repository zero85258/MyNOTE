<?php


/********************************************************************
 * @heading(標題):
 *			CLangAdmin 提示訊息
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			提示訊息
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) :
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/controller/CGalaxyController.php');
//include model.php
include_once('../inc/CLang.php');

class CLangAdmin extends CGalaxyController
{
	
	/*
		exception code of this controller
	*/
	const BACK_TO_LIST = 1;
	const BACK_TO_VIEW = 2;
	const BACK_TO_EDIT = 3;

	private $sDBName = 'EXAMPLE';

	/*
		constructor of this controller
	*/
	public function __construct(){
	}

	/**
	 * 提示訊息排序
	 */
	static public $aSearchOption = array(
		'lang_func'  => 'func名稱',
		'lang_type'  => 'type名稱',
		'lang_name'  => '名稱',
		'lang_value' => '值'
	);

	/*
		entry of this controller
		handle exception and decide where to redirect by exception code
	*/

	/**
	 * @desc 提示訊息進入的動作 
	 * @created 2017/02/24
	 */

	public function tManager() {
		$action = isset($_GET['action'])?$_GET['action']:'';
		try{
			switch($action){
				case "add":
					return $this->tLangAdd();
					break;
				case "edit":
					return $this->tLangEdit();
					break;
				case "active":
					return $this->vLangActive();
					break;
				case "del": // 刪除人員
					return $this->vLangDel();
					break;
				default:
				case "search":
				case "list":
					return $this->tLangList();
					break;		
			}
		}catch (Exception $e){
			switch($e->getCode()){
				case self::BACK_TO_LIST:
					$sUrl = $_SERVER['PHP_SELF'].'?func='.$_GET['func'];
					if(isset($_GET['lang_id']))
						$sUrl .= '&action=list&goid='.$_GET['lang_id'];
					CJavaScript::vAlertRedirect($e->getMessage(),$sUrl);
					break;
				case self::BACK_TO_EDIT:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF'].'?func='.$_GET['func'].'&action=edit&lang_id='.$_GET['lang_id']);
					break;
				default:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF']);
					break;
			}
		}
		exit;
	}

	/*
		add rule
	*/
	/**
	 * @desc 新增提示訊息
	 * @created 2017/02/24
	 */
	private function tLangAdd(){
		$Smarty = self::$Smarty;
		if(empty($_POST)){
			$Smarty->assign('aCategory',CRule::aAllRuleInCategory());
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/lang_add.html');
		}else{
			//use post data to create a $oCLang
			$oCLang = new CLang($_POST);	//if $_POST has all we need
			//add
			try{
				$oCLang->iAddLang();	//$oCLang->iLangId will be changed to insert id
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_LIST);
			}
		}
		CJavaScript::vAlertRedirect('',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCLang->iLangId}");
	}

	/*
		edit an iwant user
	*/
	/**
	 * @desc 編輯提示訊息 
	 * @created 2017/02/24
	 */
	private function tLangEdit(){
		$Smarty = self::$Smarty;
		if(empty($_GET['lang_id']))
			throw new Exception('',self::BACK_TO_LIST);
		$iLangId = $_GET['lang_id'];
		$oCLang = CLang::oGetLang($iLangId);

		if(empty($_POST)){
			$Smarty->assign('oCLang',$oCLang);
		
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/lang_edit.html');
		}else{
			if($_POST['lang_id']!==$iLangId)
				throw new Exception('GET and POST not match',self::BACK_TO_EDIT);
			//change the value in $oCLang
			$oCLang->sFunc  = isset($_POST['lang_func'])?$_POST['lang_func']:'';
			
			$oCLang->sType  = isset($_POST['lang_type'])?$_POST['lang_type']:'';
			$oCLang->sName  = isset($_POST['lang_name'])?$_POST['lang_name']:'';
			$oCLang->sValue = isset($_POST['lang_value'])?$_POST['lang_value']:'';
			
			//update
			try{
				$oCLang->vUpdateLang();
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_EDIT);
			}
		}
		CJavaScript::vAlertRedirect('user edit success',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCLang->iLangId}");
	}


	/*
		active/deactive an iwant user
	*/
	/**
	 * @desc 編輯提示訊息狀態 
	 * @created 2017/02/24
	 */	
	private function vUserActive(){
		if(empty($_GET['user_no']))
			throw new Exception('',self::BACK_TO_LIST);
		$iUserNo = $_GET['user_no'];
		$oCLang = CLang::oGetUser($iUserNo);
		try{
			$oCLang->vActivate();
		}catch (Exception $e){
			throw new Exception($e->getMessage(),self::BACK_TO_LIST);
		}
		CJavaScript::vAlertRedirect('active success',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCLang->iUserNo}");
	}

	/*
		delete an iwant user
	*/
	private function tUserDelete(){
		
	}

	/*
		list of iwant users
	*/
	/**
	 * @desc 列出提示訊息資料 
	 * @created 2017/02/24
	 */	
	private function tLangList(){
		$Smarty = self::$Smarty;
		$session = self::$session;
		$oDB = self::oDB($this->sDBName);
		if(!empty($_POST)) {
			$js_valid = isset($_GET['js_valid'])?$_GET['js_valid']:0;
			if($js_valid==1){
				$this->vaildLangSearch($_POST,1);	//client javascript vaild data
			}else{
				$this->vaildLangSearch($_POST,0);	//form submit vaild data
			}

		}
		if(empty($_GET['items'])) $iPageItems = PAGING_NUM;
		else $iPageItems = $_GET['items'];

		//because this function join two tables, order must be full name with table, caution in tpl!
		if(empty($_GET['order'])) $sOrder = "lang.lang_id";
		else $sOrder = $_GET['order'];
		
		if(empty($_GET['sort'])) $sSort = "DESC";
		else $sSort = $_GET['sort'];
		
		if(empty($_GET['page'])) $iPg = 0;
		else $iPg = $_GET['page'];
		
		if(empty($_GET['goid'])) $goid = 0;
		else $goid = $_GET['goid'];
		$aSearchSql = array();

		$action = isset($_GET['action'])?$_GET['action']:'';
		if($action === 'search'){
			$aSearchSql = $this->aGetSearchSql($_POST);
			$sSearchSql = $this->sGetSearchSql($_POST);
			

		}else
			$sSearchSql ='';

		//得到某筆資料是在第幾頁
		if($goid){
            $iPg = $oDB->iGetItemAtPage("lang","lang_id",$goid,$iPageItems,$sSearchSql,"ORDER BY $sOrder $sSort");
		}
		
		//共幾筆
		$iAllItems = CLang::iGetCount($sSearchSql,$aSearchSql);
		$iStart=$iPg*$iPageItems;

		//get objects
		if($iAllItems!==0){
			$sPostFix = "ORDER BY $sOrder $sSort LIMIT $iStart,$iPageItems";	//sql postfix
			$aCLangs = CLang::aAllLang($sSearchSql,$aSearchSql,$sPostFix);
			$Smarty->assign("aCLangs",$aCLangs);

			if(count($aCLangs)===0)
				CJavaScript::vAlertRedirect("",$_SERVER['PHP_SELF']."?func=".$_GET['func']."&action=list&items=".$_GET['items']);
		}
        

		//assign frame attribute
		$Smarty->assign("NowOrder",$sOrder);		
		$Smarty->assign("NowSort",$sSort);
		
		$Smarty->assign("OrderUrl",$_SERVER['PHP_SELF']."?func=".$_GET['func']."&action=".$action."&page=$iPg");
		$Smarty->assign("OrderSort",(strtoupper($sSort)=="DESC")?"ASC":"DESC");

		$Smarty->assign('searchKey',	$session->get("s_user_key") );
		$Smarty->assign('searchTerm',	$session->get("s_user_terms") );
		$Smarty->assign('searchOption',	self::$aSearchOption);
                
		$Smarty->assign("Total",$iAllItems);
		$Smarty->assign("PageItem",$iPageItems);
		
		$Smarty->assign("StartRow",$iStart+1);
		$Smarty->assign("EndRow",$iStart+$iPageItems);

		$Smarty->assign("iPg",$iPg);
		$Smarty->assign('PageBar',	CMisc::sMakePageBar($iAllItems, $iPageItems, $iPg, "func=".$_GET['func']."&action=".$action."&order=$sOrder&sort=$sSort"));

		return $output = $Smarty->fetch('./admin/'.get_class($this).'/lang_list.html');
	}


	/*
		check if the search string is vaild
	*/
	/**
	 * @desc搜尋提示訊息資料 
	 * @created 2017/02/24
	 */	
	private function vaildLangSearch($postData=array(),$return_type=0){

		$aErrorMsg = array();
		
		if(strlen(trim($postData['s_key'])) == 0){
			$aErrorMsg[]=_LANG_RULE_VAILD_SEARCH_KEY;
		}	
		$sErrorMsg = "";

		//client javascript vaild data
		if($return_type==1) {
			$sErrorMsg = implode("<BR>",$aErrorMsg);
			echo $sErrorMsg;
			exit;
		}
		//form submit vaild data
		if(count($aErrorMsg) > 0){
			$sErrorMsg = implode('\n',$aErrorMsg);
			throw new Exception(sprintf($sErrorMsg),self::BACK_TO_LIST);
		}
	}

	/*
		change search user name into sql string
	*/ 
	/**
	 * @desc 取出搜尋關鍵字的sql
	 * @created 2017/02/24
	 */
	private function sGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_user_key");
			$sTerms =  $session->get("s_user_terms");
		}	
		$sSql = "";
		
		if(!$sKey) {
			$session->set("s_user_key","");
			$session->set("s_user_terms","");
			return $sSql;
		}
		$session->set("s_user_key",$sKey);
		$session->set("s_user_terms",$sTerms);
		
		switch($sTerms){
			default :
				$sSql = $sSql." ($sTerms LIKE '%".$sKey."%')";
				break;
		}
		return $sSql;
	}
	/**
	 * @desc 取出搜尋關鍵字 
	 * @created 2017/02/24
	 */
	private function aGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_user_key");
			$sTerms =  $session->get("s_user_terms");
		}	
		
		
		if(!$sKey) {
			$session->set("s_user_key","");
			$session->set("s_user_terms","");
			return array();
		}
		$session->set("s_user_key",$sKey);
		$session->set("s_user_terms",$sTerms);
		
		return array("%$sKey%");
	}
}
?>