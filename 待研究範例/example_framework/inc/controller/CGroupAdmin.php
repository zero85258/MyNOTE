<?php

/********************************************************************
 * @heading(標題):
 *			CGroupAdmin 群組
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			群組
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) : 群組
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/controller/CGalaxyController.php');
//include model.php
include_once('../inc/model/CGroup.php');
include_once('../inc/model/CRule.php');

class CGroupAdmin extends CGalaxyController
{
	/*
		exception code of this controller
	*/
	const BACK_TO_LIST = 1;
	const BACK_TO_VIEW = 2;
	const BACK_TO_EDIT = 3;

	private $sDBName = 'EXAMPLE';

	/*
		constructor of this controller
	*/
	public function __construct(){
	}

	/*
		entry of this controller
		handle exception and decide where to redirect by exception code
	*/
	/**
	 * @desc 群組進入的頁面動作 
	 * @created 2017/02/24
	 */		
	public function tManager() {
		$action = isset($_GET['action'])?$_GET['action']:'';
		try{
			switch($action){
				case "add":
					return $this->tGroupAdd();
					break;
				case "edit":
					return $this->tGroupEdit();
					break;
				case "active":
					return $this->vGroupActive();
					break;
				default:
				case "search":
				case "list":
					return $this->tGroupList();
					break;
			}
		}catch (Exception $e){
			switch($e->getCode()){
				case self::BACK_TO_LIST:
					$sUrl = $_SERVER['PHP_SELF'].'?func='.$_GET['func'];
					if(isset($_GET['group_no']))
						$sUrl .= '&action=list&goid='.$_GET['group_no'];
					CJavaScript::vAlertRedirect($e->getMessage(),$sUrl);
					break;
				case self::BACK_TO_EDIT:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF'].'?func='.$_GET['func'].'&action=edit&group_no='.$_GET['group_no']);
					break;
				default:
					CJavaScript::vAlertRedirect($e->getMessage(),$_SERVER['PHP_SELF']);
					break;
			}
		}
		exit;
	}

	/*
		add group
	*/
	/**
	 * @desc 新增群組 
	 * @created 2017/02/24
	 */		
	private function tGroupAdd(){
		$Smarty = self::$Smarty;
		if(empty($_POST)){
			$Smarty->assign('aCategories',CRule::aAllRuleInCategory());
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/group_edit.html');
		}else{
			//use post data to create a $oCGroup
			$oCGroup = new CGroup($_POST);	//if $_POST has all we need
			$oCGroup->vSetRules($_POST['rule']);
			//add
			try{
				$oCGroup->iAddGroup();	//$oCGroup->iGroupNo will be changed to insert id
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_LIST);
			}
		}
		CJavaScript::vAlertRedirect('',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCGroup->iGroupNo}");
	}

	/*
		edit group
	*/
	/**
	 * @desc 編輯群組 
	 * @created 2017/02/24
	 */	
	private function tGroupEdit(){
		$Smarty = self::$Smarty;
		if(empty($_GET['group_no']))
			throw new Exception('',self::BACK_TO_LIST);
		$iGroupNo = $_GET['group_no'];
		$oCGroup = CGroup::oGetGroup($iGroupNo);
		if(empty($_POST)){
			$Smarty->assign('oCGroup',$oCGroup);
			$Smarty->assign('aGroupRule',$oCGroup->aCRule());
			$Smarty->assign('aCategories',CRule::aAllRuleInCategory());
			return $output = $Smarty->fetch('./admin/'.get_class($this).'/group_edit.html');
		}else{
			//use post data to modify $oCGroup
			$oCGroup->sName = $_POST['group_name'];
			$oCGroup->sDesc = $_POST['group_desc'];
			$oCGroup->vSetRules(isset($_POST['rule'])?$_POST['rule']:'');
			$oCGroup->bStatus = isset($_POST['group_status'])?$_POST['group_status']:0;	//group_status
			// $oCGroup->bStatus = ($_POST['group_status'])?"1":"0";	//group_status
			//update
			try{
				$oCGroup->vUpdateGroup();
			}catch (Exception $e){
				throw new Exception($e->getMessage(),self::BACK_TO_LIST);
			}
		}
		CJavaScript::vAlertRedirect('',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCGroup->iGroupNo}");
	}

	/*
		activate/deactivate group
	*/
	/**
	 * @desc 編輯群組狀態 
	 * @created 2017/02/24
	 */	
	private function vGroupActive(){
		if(empty($_GET['group_no']))
			throw new Exception('',self::BACK_TO_LIST);
		$iGroupNo = $_GET['group_no'];
		$oCGroup = CGroup::oGetGroup($iGroupNo);
		try{
			$oCGroup->vActivate();
		}catch (Exception $e){
			throw new Exception($e->getMessage(),self::BACK_TO_LIST);
		}
		CJavaScript::vAlertRedirect('',$_SERVER['PHP_SELF'].'?func='.$_GET['func']."&action=list&goid={$oCGroup->iGroupNo}");
	}


	/*
		list of groups
	*/
	/**
	 * @desc 列出群組 
	 * @created 2017/02/24
	 */	
	private function tGroupList(){
		$Smarty = self::$Smarty;
		$session = self::$session;
		$oDB = self::oDB($this->sDBName);

		if(!empty($_POST)) {
			$js_valid = isset($_GET['js_valid'])?$_GET['js_valid']:0;
			if($js_valid==1) {
				$this->vaildGroupSearch($_POST,1);	//client javascript vaild data
			}else{
				$this->vaildGroupSearch($_POST,0);	//form submit vaild data
			}
		}
		if(empty($_GET['items'])) $iPageItems = PAGING_NUM;
		else $iPageItems = $_GET['items'];

		if(empty($_GET['order'])) $sOrder = "group_name";
		else $sOrder = $_GET['order'];

		if(empty($_GET['sort'])) $sSort = "DESC";
		else $sSort = $_GET['sort'];

		if(empty($_GET['page'])) $iPg = 0;
		else $iPg = $_GET['page'];

		if(empty($_GET['goid'])) $goid = 0;
		else $goid = $_GET['goid'];

		$aSearchSql = array();
		$action = isset($_GET['action'])?$_GET['action']:'';
		
		if($action === 'search'){
			$aSearchSql = $this->aGetSearchSql($_POST);
			$sSearchSql = $this->sGetSearchSql($_POST);


		}else
			$sSearchSql ='';

		//得到某筆資料是在第幾頁
		if($goid){
			if($sSearchSql!=='') $sWhereSql = "WHERE $sSearchSql";	//no default filter
            $iPg = $oDB->iGetItemAtPage("example_group","group_no",$goid,$iPageItems,$sSearchSql,"ORDER BY $sOrder $sSort");
            // $iPg = $oDB->iGetItemAtPage("example_group","group_no",$goid,$iPageItems,$sSearchSql,$aSearchSql,"ORDER BY $sOrder $sSort");
		}

		//共幾筆
		$iAllItems = CGroup::iGetCount($sSearchSql,$aSearchSql);
		$iStart=$iPg*$iPageItems;

		if($iAllItems!==0){
			$sPostFix = "ORDER BY $sOrder $sSort LIMIT $iStart,$iPageItems";	//sql postfix
			$Smarty->assign("aCGroups",CGroup::aAllGroup($sSearchSql,$aSearchSql,$sPostFix));
		}

		//assign frame attribute
		$Smarty->assign("NowOrder",$sOrder);
		$Smarty->assign("NowSort",$sSort);

		$Smarty->assign("OrderUrl",$_SERVER['PHP_SELF']."?func=".$_GET['func']."&action=".$action."&page=$iPg");
		$Smarty->assign("OrderSort",(strtoupper($sSort)=="DESC")?"ASC":"DESC");

		$Smarty->assign('searchKey',	$session->get("s_group_key") );
		$Smarty->assign('searchTerm',	$session->get("s_group_terms") );

		$Smarty->assign("Total",$iAllItems);
		$Smarty->assign("PageItem",$iPageItems);

		$Smarty->assign("StartRow",$iStart+1);
		$Smarty->assign("EndRow",$iStart+$iPageItems);

		$Smarty->assign("iPg",$iPg);
		$Smarty->assign('PageBar',	CMisc::sMakePageBar($iAllItems, $iPageItems, $iPg, "func=".$_GET['func']."&action=".$action."&order=$sOrder&sort=$sSort"));

		return $output = $Smarty->fetch('./admin/'.get_class($this).'/group_list.html');
	}

	/*
		check if the search string is vaild
	*/
	/**
	 * @desc 群組查詢
	 * @created 2017/02/24
	 */	
	private function vaildGroupSearch($postData=array(),$return_type=0){

		$aErrorMsg = array();

		if(strlen(trim($postData['s_key'])) == 0){
			$aErrorMsg[]=_LANG_RULE_VAILD_SEARCH_KEY;
		}
		$sErrorMsg = "";

		//client javascript vaild data
		if($return_type==1) {
			$sErrorMsg = implode("<BR>",$aErrorMsg);
			echo $sErrorMsg;
			exit;
		}
		//form submit vaild data
		if(count($aErrorMsg) > 0){
			$sErrorMsg = implode('\n',$aErrorMsg);
			throw new Exception(sprintf($sErrorMsg),self::BACK_TO_LIST);
		}
	}

	/*
		change search group name into sql string
	*/
	/**
	 * @desc 搜尋關鍵字群組 
	 * @created 2017/02/24
	 */	
	private function sGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_group_key");
			$sTerms =  $session->get("s_group_terms");
		}
		$sSql = "";

		if(!$sKey) {
			$session->set("s_group_key","");
			$session->set("s_group_terms","");
			return $sSql;
		}
		$session->set("s_group_key",$sKey);
		$session->set("s_group_terms",$sTerms);

		switch($sTerms){
			default :
				// $sSql = $sSql." ($sTerms LIKE ?)";
				$sSql = $sSql." ($sTerms LIKE '%".$sKey."%')";
				break;
		}
		return $sSql;
	}
	/**
	 * @desc 搜尋關鍵字群組 
	 * @created 2017/02/24
	 */
	private function aGetSearchSql($aPost){
		$session = self::$session;

		if(count($aPost)){
			$sKey = trim($aPost['s_key']);
			$sTerms = trim($aPost['s_terms']);
		}else{
			$sKey = $session->get("s_group_key");
			$sTerms =  $session->get("s_group_terms");
		}
		$sSql = "";

		if(!$sKey) {
			$session->set("s_group_key","");
			$session->set("s_group_terms","");
			return $sSql;
		}
		$session->set("s_group_key",$sKey);
		$session->set("s_group_terms",$sTerms);

		return array("%$sKey%");
	}
}
?>
