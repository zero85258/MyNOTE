<?php
include_once('../inc/model/CGalaxyClass.php');
class CLang extends CGalaxyClass
{
	
	
	static public $iBackendLang ; //現在是前台或後台
	protected $iLangId;
	public $sFunc;	
	public $sType;
	public $sName;
	public $sValue;
	public $sDesc;
	public $iBackend; //1只屬於後台 0前台後台共用
	static public $aInstancePool = array();

	static protected $sDBName = 'EXAMPLE';

	public function __construct ($multiData){	
		parent::__construct($multiData);
		if(!is_array($multiData))
			throw new Exception("CLang: __construct failed, require an array");

		

		if(isset($multiData['lang_id']))
			$this->iLangId = $multiData['lang_id'];
		else
			$this->iLangId =0;	
		
		//initialize optional member
		$this->sFunc = $multiData['lang_func'];
		$this->sType = $multiData['lang_type'];
		$this->sName = $multiData['lang_name'];
		$this->sValue = $multiData['lang_value'];
		$this->sDesc = isset($multiData['lang_desc'])?$multiData['lang_desc']:'';
		$this->iBackend = isset($multiData['is_where'])?$multiData['is_where']:'';
		
	}

	static public  function oGetLang($iLangId = 0){
		
		if($iLangId === 0 ) return null;
		$oDB = self::oDB(self::$sDBName);
		$iDbq = $oDB->iQuery("SELECT * FROM lang
								WHERE lang_id='$iLangId'");
		
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow === false || $oDB->iNumRows($iDbq)==0)
			return null;
		
		$oCLang = new CLang($aRow);
		return $oCLang;
	}
	
	static public  function oGetLangByName($sLangName = ""){
		
		if($sLangName === "") return null;
		$oDB = self::oDB(self::$sDBName);
		
		$iDbq = $oDB->iQuery("SELECT * FROM lang
								WHERE lang_name='$sLangName'");
		

		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow === false || $oDB->iNumRows($iDbq)==0)
			return null;
		
		$oCLang = new CLang($aRow);

		return $oCLang;
	}

	static public  function vDefineLang($sFunc="",$sType="",$iBackend=0,$sDesc="",$sName="",$sValue=""){
		
		
		if($sFunc==="" || $sType==="" || $sName==="" || $sValue==="")
			return;
			
		if($iBackend!=self::$iBackendLang &&  $iBackend) return; //iBackend =1只屬於後台,並且如果現在是iBackendLang =0 前台
		
		$oCLang = self::oGetLangByName($sName);
		
		if(!$oCLang){
				
			$oDB = self::oDB(self::$sDBName);
			try{
				// $aValues = array(	'lang_func'=>$sFunc,
				// 				'lang_type'=>$sType,
				// 				'lang_name'=>$sName,
				// 				'lang_value'=>$sValue,
				// 				'lang_desc'=>$sDesc,
				// 				'is_where'=>$iBackend
				// 			);
				$aField = array(	'lang_func',
									'lang_type',
									'lang_name',
									'lang_value',
									'lang_desc',
									'is_where'
									);

				$aValues = array(	$sFunc,
									$sType,
									$sName,
									$sValue,
									$sDesc,
									$iBackend
							);

				
				$oDB->sInsert("lang",$aField,$aValues);

				define($name,$value);
			}catch (Exception $e){
				
				throw new Exception('oCLang->vDefineLang: '.$e->getMessage());
			}	
		}else{
			if (!defined($oCLang->sName))
				define($oCLang->sName,$oCLang->sValue);
			
		}

	}

	
	/**
	 * @desc 取得總筆數
	 * @controller/CuserAdmin.php 拋值過來
	 *  @param int $sSearchSql where 條件
	 *  @param int $aBinds 搜尋的關鍵字
	 * @created 2017/02/24
	 */	
	static public function iGetCount($sSearchSql='',$aBinds=array()){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT count(lang_id) as total FROM lang";
		
		if($sSearchSql!==''){
			$sSql .= " WHERE $sSearchSql";
			// echo 'sSearchSql:'.$sSearchSql;exit;
		}
			

		$iDbq = $oDB->iQuery($sSql,$aBinds);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow!==false)
			$iCount = (int)$aRow['total'];
		else
			$iCount = 0;
		return $iCount;
	}

	/**
	 * @desc 取得提示訊息、能搜尋特定提示訊息
	 * @controller/CLangAdmin.php 拋值過來
	 *  @param int $sSearchSql where 條件
	 *  @param int $aBinds 搜尋的關鍵字
	 *  @param int $sPostFix limit  的限制
	 * @created 2017/02/24
	 */		
	static public function aAllLang($sSearchSql='',$aBinds=array(),$sPostFix=''){
		
		$oDB = self::oDB(self::$sDBName);
		$aAllLang= array();
		$sSql = "SELECT * FROM `lang`";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		if($sPostFix!=='')
			$sSql .= " $sPostFix";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		while($aRow = $oDB->aFetchAssoc($iDbq)){
			if(!isset(self::$aInstancePool[$aRow['lang_id']]))
				self::$aInstancePool[$aRow['lang_id']] = new CLang($aRow);
			$aAllLang[] = self::$aInstancePool[$aRow['lang_id']];
		}
		return $aAllLang;
	}

	/*
		add $oCRule data to example_user DB
		if you want to create a new rule in DB, new a CRule and call this function
		return insert id(rule_no), user may find this new CRule by rule_no
	*/
	/**
	 * @desc 新增提示訊息
	 * @created 2017/02/24
	 */	
	public function iAddLang(){
		$oDB = self::oDB(self::$sDBName);
		
		$aField = array(	'lang_func', 
							'lang_type',
							'lang_name',
							'lang_value'
							);
		$aValues = array(	$this->sFunc, 
							$this->sType,
							$this->sName,
							$this->sValue
							);



		try{
			$oDB->sInsert("`lang`",$aField, $aValues);
			$this->iLangId = $oDB->iGetInsertId();
			return $this->iLangId;
		}catch (Exception $e){
			throw new Exception("CLang->iAddLang: ".$e->getMessage());
		}
	}

	public function vOverwrite($oCLang){
		//if not a CPuppets object or uuid not match
		if(get_class($oCLang)!=='CLang' || $this->iLangId!==$oCLang->iLangId)
			throw new Exception('CLang->vOverwrite: fatal error');
			
		foreach ($this as $name => $value) {
			if($name==='iLangId')
				continue;
			$this->$name = $oCLang->$name;	//overwrite
		}
	}


	
	public function vUpdateLang(){
		$oDB = self::oDB(self::$sDBName);
		
		try{
			
			$aField = array(	
							'lang_func',
							'lang_type',
							'lang_name',
							'lang_value'
						
							);
			$aValues = array(	
							$this->sFunc,
							$this->sType,
							$this->sName,
							$this->sValue
						
							);

			// $oDB->vUpdate("lang",array_keys($aValues),array_values($aValues),"lang_id='{$this->iLangId}'");
			$oDB->sUpdate("lang",$aField,$aValues,"lang_id='{$this->iLangId}'");
		}catch (Exception $e){
			
			throw new Exception('CLang->vUpdateLang: '.$e->getMessage());
		}
	}
	
	
	
	
}
?>