<?php
// error_reporting(E_ALL & ~(E_NOTICE|E_STRICT|E_DEPRECATED));
error_reporting(E_ALL);
ini_set('display_errors', true);
defined('PATH_ROOT')|| define('PATH_ROOT', realpath(dirname(__FILE__) . '/..'));

define('IS_RELEASE',false);


/**
 * SMARTY 設定
 */
define('PATH_SMARTY_TPL', PATH_ROOT.'/tpl');


/**
 * 連線到後台資料庫儲存DB 設定 *Required*
 */
define('_EXAMPLE_HOST',	'172.16.2.68');
define('_EXAMPLE_DB',	'example_db');
define('_EXAMPLE_USER',	'robot');
define('_EXAMPLE_PASS',	'robot');


define('_EXAMPLE_LOG_HOST',	'172.16.2.69');
define('_EXAMPLE_LOG_DB',	'example_user_log_db');
define('_EXAMPLE_LOG_USER',	'robot');
define('_EXAMPLE_LOG_PASS',	'robot');

//php self path
defined('PHP_SELF_PATH')
|| define('PHP_SELF_PATH', dirname($_SERVER['PHP_SELF']));
//根目錄
defined('PATH_ROOT')
|| define('PATH_ROOT', realpath(dirname(__FILE__) . '/..'));
/**
 * 定義分頁的資料序號
 */
define("PAGE_INPUT_TYPE_NO", 142);
defined('_LANG_NEXT_PAGE')||define('_LANG_NEXT_PAGE', '下一頁');
defined('_LANG_LAST_PAGE')||define('_LANG_LAST_PAGE', '最後一頁');


/**
 * session db config *Required*
 */
defined('_SESSION_HOST')||define('_SESSION_HOST',	'172.16.2.68');
defined('_SESSION_DB')||define('_SESSION_DB',	'example_user_log_db');
defined('_SESSION_USER')||define('_SESSION_USER',	'robot');
defined('_SESSION_PASS')||define('_SESSION_PASS',	'robot');

/**
 * date.timezone
 */
date_default_timezone_set("UTC");
/**
 * php script exec unlimit in time
 */
set_time_limit( 0 );


/**
 * ProgressBar Style
 */
define('PAGING_NUM', 			10);


/**
 * layout 語言
 */
define('_LANG', 'tw');

?>
