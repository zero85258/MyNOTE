<?php

/********************************************************************
 * @heading(標題):
 *			CUserLog 人員log檔
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			人員log檔
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) :
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/model/CGalaxyClass.php');

class CUSerLog extends CGalaxyClass
{
	
	private $iLogNo;
	public $sTableName;
	public $sTableUuid;
	public $sFunc;
	public $sAction;

	private $__oCRule;

	//extra member not in DB
	private $sYear;
	private $sMonth;

	//database setting
	static protected $sDBName = 'EXAMPLE_LOG';

	/*
		get $oCUserLig by certain log_id
	*/
		
	static public function oGetLog($iLogNo,$sYear,$sMonth){
		$oDB = self::oDB(self::$sDBName);
		if(!$oDB->bIsTableExist("user_log_{$sYear}_{$sMonth}"))
			return null;
		$sSql = "SELECT * FROM user_log_{$sYear}_{$sMonth} WHERE log_id='$iLogNo'";
		$iDbq = $oDB->iQuery($sSql);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow ===false || $oDB->iNumRows($iDbq)>1)
			return null;
		$oLog = new CUserLog($aRow,$sYear,$sMonth);
		return $oLog;
	}

	static public function aAllLog($sSearchSql='',$aBinds=array(),$sPostFix='',$sYear,$sMonth){
		echo 'IN';exit;
		$oDB = self::oDB(self::$sDBName);
		if(!$oDB->bIsTableExist("user_log_{$sYear}_{$sMonth}"))
			return array();
		$sSql = "SELECT * FROM user_log_{$sYear}_{$sMonth}";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		if($sPostFix!=='')
			$sSql .= " $sPostFix";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		$aAllLog = array();
		while($aRow = $oDB->aFetchAssoc($iDbq)){
			$aAllLog[] = new CUserLog($aRow,$sYear,$sMonth);
		}
		return $aAllLog;
	}

	public function __construct($multiData,$sYear='',$sMonth=''){
		parent::__construct($multiData);
		$this->iLogNo = isset($multiData['log_id'])?$multiData['log_id']:0;
		$this->sTableName = $multiData['table_name'];
		$this->sTableUuid = $multiData['table_id'];
		$this->sFunc = $multiData['table_func'];
		$this->sAction = $multiData['table_action'];
		//extra member
		if($sYear!=='')
			$this->sYear = $sYear;
		if($sMonth!=='')
			$this->sMonth = $sMonth;
		//galaxy class memeber
		$this->sModifiedTime = isset($multiData['modifiedtime'])?$multiData['modifiedtime']:'';
	}

	public function __get($varName)
    {
        return $this->$varName;
    }

    /*
    	set & get rule of this log
    */
    public function oRule(){
    	if(empty($this->__oCRule)){
    		$aCRule = CRule::aAllRule("func='{$this->sFunc}' AND action='{$this->sAction}'");
    		if(count($aCRule)>1)
				return null;
    		$this->__oCRule = $aCRule[0];
    	}
    	return $this->__oCRule;
    }

    /**
	 * @desc 新增人員log檔
	 * @created 2017/02/24
	 */
	public function iAddLog(){
		$oDB = self::oDB(self::$sDBName);
		// $aValues = array(	'table_name'=>$this->sTableName,
		// 					'table_id'=>$this->sTableUuid,
		// 					'table_func'=>$this->sFunc,
		// 					'table_action'=>$this->sAction,
		// 					'user_no'=>$this->__iUserNo,
		// 					'modifiedtime'=>date("Y-m-d H:i:s")
		// 					);

		$aField = array(	'table_name',
							'table_id',
							'table_func',
							'table_action',
							'user_no',
							'modifiedtime',
							);

		$aValues = array(	$this->sTableName,
							$this->sTableUuid,
							$this->sFunc,
							$this->sAction,
							$this->__iUserNo,
							date("Y-m-d H:i:s")
							);


		// echo  "<pre>";print_r($aValues).'<br>';
		try{
			$oDB->vBegin();
			if(!$oDB->bIsTableExist('user_log_'.date('Y').'_'.date('m'))){
				self::vAddLogTable(date('Y'),date('m'));
			}
			$oDB->sInsert('user_log_'.date('Y').'_'.date('m'),$aField,$aValues);
			$this->iLogNo = $oDB->iGetInsertId();
			$oDB->vCommit();
			return $this->iLogNo;
		}catch (Exception $e){
			// echo Exception("CUserLog->iAddLog: ".$e->getMessage());
			$oDB->vRollback();
			throw new Exception("CUserLog->iAddLog: ".$e->getMessage());
		}
	}

	/**
	 * @desc 建立log檔的table
	 * @param str $sYear 年
	 * @param str $sMonth 月
	 * @created 2017/02/24
	 */	
	static private function vAddLogTable($sYear,$sMonth){

    	$oDB = self::oDB(self::$sDBName);
    	$aTableInfo = $oDB->aGetCreateTableInfo("user_log");
		if(!empty($aTableInfo['Create Table'])){

			$aTableInfo['Create Table'] = preg_replace("/user_log/i", "user_log_{$sYear}_{$sMonth}", $aTableInfo['Create Table']);
			// echo 'db:'.$aTableInfo['Create Table'].'<br>';
		}
		$oDB->iQuery($aTableInfo['Create Table'].";\n\n");
    }
}
?>