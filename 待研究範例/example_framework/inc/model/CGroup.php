<?php

/********************************************************************
 * @heading(標題):
 *			CGroup 群組
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			群組功能
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) :
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/model/CGalaxyClass.php');
include_once('../inc/model/CUser.php');
include_once('../inc/model/CRule.php');

class CGroup extends CGalaxyClass
{
	//vital member
	private $iGroupNo;	//group_no in example_user.example_group, read only after construct
	//optional member
	public $sName;

	public $sDesc;
	//members that set only when corresponding function is called
	protected $__aCUser = array();
	protected $__aCRule = array();	//CRule
	//database setting
	static protected $sDBName = 'EXAMPLE';

	//instance pool
	static public $aInstancePool = array();

	/*
		get $oCGroup by certain group_no
	*/
	/**
	 * @desc 取得群組
	 * @created 2017/02/24
	 */	
	static public function oGetGroup($iGroupNo){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT * FROM example_group WHERE group_no = '$iGroupNo'";
		$iDbq = $oDB->iQuery($sSql);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow ===false || $oDB->iNumRows($iDbq)>1)
			return null;
		$oCGroup = new CGroup($aRow);
		self::$aInstancePool[$iGroupNo] = $oCGroup;

		return $oCGroup;
	}

	/*
		get all group in an array
		if $sSearchSql is given, query only match groups
		example:$aCGroups = CGroup::aAllGroup("ststem_name='創世紀'");
	*/
	/**
	 * @desc 搜尋群組
	 * @param str $sSearchSql where 條件
	 * @created 2017/02/24
	 */	
	static public function aAllGroup($sSearchSql='',$aBinds=array(),$sPostFix=''){
		$oDB = self::oDB(self::$sDBName);
		$aAllGroup = array();
		$sSql = "SELECT * FROM example_group";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		if($sPostFix!=='')
			$sSql .= " $sPostFix";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		while($aRow = $oDB->aFetchAssoc($iDbq)){
			if(!isset(self::$aInstancePool[$aRow['group_no']])){
				self::$aInstancePool[$aRow['group_no']] = new CGroup($aRow);
			}
			$aAllGroup[] = self::$aInstancePool[$aRow['group_no']];
		}
		return $aAllGroup;
	}

	/*
		get count of group which match query
	*/
	/**
	 * @desc 群組數量
	 * @param str $sSearchSql where 條件
	 * @created 2017/02/24
	 */	
	static public function iGetCount($sSearchSql='',$aBinds=array()){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT count(group_no) as total FROM example_group";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow!==false)
			$iCount = (int)$aRow['total'];
		else
			$iCount = 0;
		return $iCount;
	}

	/*
		constructor of $oCGroup
		some class member are essential, must be initialized, or throw exception
		some class member are optional, may not be initialized
	*/
	public function __construct($multiData){
		parent::__construct($multiData);
		if(!is_array($multiData))
			throw new Exception("CGroup: __construct failed, require an array");
		//initialize vital member
		$this->iGroupNo = $multiData['group_no'];
		$this->sName = isset($multiData['group_name'])?$multiData['group_name']:"";
		$this->sDesc = isset($multiData['group_desc'])?$multiData['group_desc']:"";
		//galaxy class memeber
		// $this->bStatus = ($multiData['group_status'])?"1":"0";
		$this->bStatus = isset($multiData['group_status'])?$multiData['group_status']:0;
	}

	//php default function, let private member become read-only class member for others
    public function __get($varName)
    {
        return $this->$varName;
    }

    /*
    	set & get user of this $oCGroup
    */

    /**
	 * @desc 取得該群組的人員
	 * @created 2017/02/24
	 */	
    public function aUser(){
    	$oDB = self::oDB(self::$sDBName);
    	if(empty($this->__aCUser)){
    		$sSql = "SELECT * FROM example_group_user_rel WHERE group_no = {$this->iGroupNo}";
			$iDbq = $oDB->iQuery($sSql);
			while($aRow = $oDB->aFetchAssoc($iDbq)){
				$this->__aCUser[$aRow['user_no']] = CUser::oGetUser($aRow['user_no']);
			}
    	}
    	return $this->__aCUser;
    }

    /*
    	set & get rules of this $oCGroup
    */
     /**
	 * @desc 該群組有哪些存在的系統
	 * @created 2017/02/24
	 */	
    public function aCRule(){

    	$oDB = self::oDB(self::$sDBName);
    	if(empty($this->__aCRule)){
    		$sSql = "SELECT * FROM example_group_rule_rel WHERE group_no = {$this->iGroupNo}";
			$iDbq = $oDB->iQuery($sSql);
			while($aRow = $oDB->aFetchAssoc($iDbq)){
				$this->__aCRule[$aRow['rule_no']] = CRule::oGetRule($aRow['rule_no']);
			}
    	}
    	return $this->__aCRule;
    }

	/*
		update group data in this CGroup to example_user DB
		if you want to update group data in DB, get a CGroup of that group, change member value, and call this function
		$oCGroup->iGroupNo is not changeable
	*/
	/**
	 * @desc 更新群組
	 * @created 2017/02/24
	 */	
	public function vUpdateGroup(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');

		// $aValues = array(	'group_name'=>$this->sName,
		// 					'group_status'=>($this->bStatus)?"1":"0"
		// 					);
		$aField = array(	'group_name',
							'group_desc',
							'group_status'
							);
		$aValues = array(	$this->sName,
							$this->sDesc,
							($this->bStatus)?"1":"0"
							);

		try{
			$oDB->vBegin();
			$oDB->sUpdate("`example_group`",$aField, $aValues, "`group_no` = {$this->iGroupNo}");
			//$this->vUpdateGroupUser();	//update users
			$this->vUpdateGroupRule();	//update rules
			$oDB->vCommit();
			$oCurrentUser->vAddUserLog("example_group",$this->iGroupNo,'group','edit');
		}catch (Exception $e){
			$oDB->vRollback();
			throw new Exception("CGroup->vUpdateGroup: ".$e->getMessage());
		}
	}

	/*
		update group user in this CGroup to example_user DB
		if you want to update group user in DB, get a CGroup of that group, call vSetUsers(), and call this function
	*/
	/**
	 * @desc 更新群組人員
	 * @created 2017/02/24
	 */	
	public function vUpdateGroupUser(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');

		try{
			$oDB->vBegin();
			$oDB->vDelete('example_group_user_rel',"`group_no`={$this->iGroupNo}");
			foreach ($this->__aCUser as $oCUser) {
				if($oCUser->bStatus==='0')
					continue;	//does not insert deprecated user
				$aUsrValues = array(	'group_no'=>$this->iGroupNo,
										'user_no'=>$oCUser->iUserNo
										);
				$oDB->sInsert('example_group_user_rel',$aUsrValues);
			}
			$oDB->vCommit();
			$oCurrentUser->vAddUserLog("example_group",$this->iGroupNo,'group','edituser');
		}catch (Exception $e){
			$oDB->vRollback();
			throw new Exception("CGroup->vUpdateGroupUser: ".$e->getMessage());
		}
	}

	/*
		update group rule in this CGroup to example_user DB
		if you want to update group rule in DB, get a CGroup of that group, call vSetRules(), and call this function
	*/
	/**
	 * @desc 更新群組的系統
	 * @created 2017/02/24
	 */		
	public function vUpdateGroupRule(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');

		try{
			$oDB->vBegin();
			$oDB->vDelete('example_group_rule_rel',"`group_no`={$this->iGroupNo}");
			foreach ($this->__aCRule as $oCRule) {
				// $aRulValues = array(	'group_no'=>$this->iGroupNo,
				// 						'rule_no'=>$oCRule->iRuleNo
				// 						);
				$aField = array(	'group_no',
									'rule_no'
										);
				$aRulValues = array(	$this->iGroupNo,
										$oCRule->iRuleNo
										);
				$oDB->sInsert('example_group_rule_rel',$aField, $aRulValues);
			}
			$oDB->vCommit();
			$oCurrentUser->vAddUserLog("example_group",$this->iGroupNo,'group','editrule');
		}catch (Exception $e){
			$oDB->vRollback();
			throw new Exception("CGroup->vUpdateGroupRule: ".$e->getMessage());
		}
	}

	/*
		add $oCGroup data to example_user DB
		if you want to create a new group in DB, new a CGroup and call this function
		return insert id(group_no), user may find this new CGroup by group_no
	*/
	/**
	 * @desc 新增群組
	 * @created 2017/02/24
	 */	
	public function iAddGroup(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');

		// $aValues = array(	'group_name'=>$this->sName,
		// 					'group_status'=>($this->bStatus)?"1":"0"
		// 					);

		$aField = array(	'group_name',
							'group_desc',
							'group_status'
							);
		$aValues = array(	$this->sName,
							$this->sDesc,
							($this->bStatus)?"1":"0"
							);

		try{
			$oDB->vBegin();
			$oDB->sInsert("`example_group`", $aField, $aValues);
			$this->iGroupNo = $oDB->iGetInsertId();
			$this->vUpdateGroupRule();	//update rules
			$oDB->vCommit();
			$oCurrentUser->vAddUserLog("example_group",$this->iGroupNo,'group','add');
			return $this->iGroupNo;
		}catch (Exception $e){
			$oDB->vRollback();
			throw new Exception("CGroup->iAddGroup: ".$e->getMessage());
		}
	}

	/*
		activate this oCGroup
	*/
	/**
	 * @desc 更改群組狀態
	 * @created 2017/02/24
	 */	
	public function vActivate(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');

		if($this->bStatus==='1')
			$this->bStatus='0';
		else
			$this->bStatus='1';
		// $aValues = array('group_status'=>$this->bStatus);
		$aField = array('group_status');
		$aValues = array($this->bStatus);

		try{
			$oDB->sUpdate("`example_group`",$aField, $aValues, "`group_no` = {$this->iGroupNo}");
			$oCurrentUser->vAddUserLog("example_group",$this->iGroupNo,'group','activate');
		}catch (Exception $e){
			throw new Exception("CGroup->vActivate: ".$e->getMessage());
		}
	}

	/*
		set users by array(user_no)
	*/
	public function vSetUsers($aUserNos){
		if(!is_array($aUserNos))
			return;
		$this->__aCUser = null;	//clear all user
		foreach ($aUserNos as $iUserNo) {
			$this->__aCUser[$iUserNo] = CUser::oGetUser($iUserNo);
		}
	}

	/*
		set rules by array(rule_no)
	*/
	/**
	 * @desc 將rule排成陣列
	 * @created 2017/02/24
	 */		
	public function vSetRules($aRuleNos){
		if(!is_array($aRuleNos))
			return;
		$this->__aCRule = array();	//clear all rule
		foreach ($aRuleNos as $iRuleNo) {
			$this->__aCRule[$iRuleNo] = new CRule(array('rule_no'=>$iRuleNo));
		}
	}
}
?>
