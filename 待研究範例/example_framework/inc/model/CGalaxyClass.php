<?php

/********************************************************************
 * @heading(標題):
 *          CGalaxyClass 
 * @author(作者) :
 *          Jerry He
 * @purpose(目的) :
 *          人員log檔
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *          修改日期:
 *          修改人姓名:
 *          修改內容:
 * @copyright(版權所有) :
 *          銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) :
 * @created(建立日期) :
 *          2017/02/24
 ********************************************************************/

include_once(PATH_ROOT.'/inc/model/CUser.php');
include_once(PATH_ROOT.'/inc/CDbShell.php');

Class CGalaxyClass
{
	static protected $aDB = array();
    static public $session;

    public $sCreateTime;
	public $sModifiedTime;
	public $bStatus;

	protected $__iUserNo;
	protected $__oLastUser;

	public function __construct($multiData){
        if(!isset($multiData['user_no'])){
            if(!is_null(self::$session))
                $oCUser = self::$session->get('oCurrentUser');
            if(!is_null($oCUser))
                $multiData['user_no'] = $oCUser->iUserNo;
        }
        $this->__iUserNo = isset($multiData['user_no'])?$multiData['user_no']:'';
        // $this->__iUserNo = $multiData['user_no'];
	}

	public function __get($varName)
    {
        return $this->$varName;
    }

    /*
        set & get LastUser data, which is an array from user DB
    */
    public function oLastUser(){
        if(is_null($this->__oLastUser)){
           
            $this->__oLastUser = CUser::oGetUser($this->__iUserNo);
        }
        return $this->__oLastUser;
    }

    public function sLocalCreateTime(){
         if(strtotime($this->sCreateTime)<0) return $this->sCreateTime;
        return date('Y-m-d H:i:s',strtotime('+8hour',strtotime($this->sCreateTime)));
    }

    public function sLocalModifiedTime(){
        if(strtotime($this->sModifiedTime)<0) return $this->sModifiedTime;
        return date('Y-m-d H:i:s',strtotime('+8hour',strtotime($this->sModifiedTime)));   
    }

    static public function oDB($sDBName){
        if(!isset(self::$aDB[$sDBName])){
            self::$aDB[$sDBName] = new CDbShell(    constant('_'.$sDBName.'_DB'),
                                                    constant('_'.$sDBName.'_HOST'),
                                                    constant('_'.$sDBName.'_USER'),
                                                    constant('_'.$sDBName.'_PASS')
                                                    );
        }
        return self::$aDB[$sDBName];
    }
}
?>