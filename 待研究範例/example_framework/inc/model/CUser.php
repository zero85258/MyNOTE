<?php

/********************************************************************
 * @heading(標題):
 *			CUser 人員
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			人員資料
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) :
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/model/CGalaxyClass.php');
include_once('../inc/model/CGroup.php');
include_once('../inc/model/CRule.php');
include_once('../inc/model/CUserLog.php');

class CUser extends CGalaxyClass
{
	//vital member
	protected $iUserNo;	//user_no in example_user.example_user, read only after construct
	public $sName;	//user name
	protected $sAccount;	//account to authorized user, read only after construct
	//optional memeber
	private $sPassword;	//password to authorized user, sha1 in hex
	public $sEmail;	//email address
	public $sTel;	//telephone #
	public $sMobile;	//mobile phone #

	//members that set only when corresponding function is called
	protected $__aCGroup = array();
	protected $__aCRule = array();
	//database setting
	static protected $sDBName = 'EXAMPLE';

	//instance pool
	static public $aInstancePool = array();

	//static functions, most are used to find&get $oCUser
	/*
		get $oCUser by certain user_no
	*/

	/**
	 * @desc 定義人員資料
	 * @created 2017/02/24
	 */
	 	
	public function __construct($multiData){

		parent::__construct($multiData);
		if(!is_array($multiData))
			throw new Exception("CUser: __construct failed, require an array");
		//initialize vital member
		if(isset($multiData['user_no']))
			$this->iUserNo = $multiData['user_no'];
		else
			$this->iUserNo = 0;
		$this->sName = $multiData['user_name'];
		$this->sAccount = $multiData['user_account'];
		if(!isset($this->iUserNo) || !isset($this->sName) || !isset($this->sAccount))
			throw new Exception("CUser: __construct failed, lack of vital member");
		//initialize optional member
		$this->sPassword = isset($multiData['user_password'])?$multiData['user_password']:'';
		$this->sEmail = $multiData['user_email'];
		$this->sTel = $multiData['user_tel'];

		$this->sMobile = $multiData['user_mobile'];

		$this->user_name = $multiData['user_name'];
		//galaxy class memeber
		$this->bStatus = $multiData['status'];
		$this->sCreateTime = isset($multiData['createtime'])?$multiData['createtime']:'';
		
	}

	public function __destruct(){
		unset($this->__aCGroup);
		unset($this->__aCRule);
	}

	//php default function, let private member become read-only class member for others
    public function __get($varName)
    {
        return $this->$varName;
    }


	/**
	 * @desc 取得人員
	 * @created 2017/02/24
	 */	
	static public function oGetUser($iUserNo){
		$oDB = self::oDB(self::$sDBName);
		//if already queryed
		// if(!is_null(self::$aInstancePool[$iUserNo]))
		if(isset(self::$aInstancePool[$iUserNo]))
			return self::$aInstancePool[$iUserNo];

		$sSql = "SELECT * FROM example_user WHERE user_no=$iUserNo";
		$iDbq = $oDB->iQuery($sSql);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow === false || $oDB->iNumRows($iDbq)>1)
			return null;
		
		$oCUser = new CUser($aRow);
		self::$aInstancePool[$iUserNo] = $oCUser;
		return $oCUser;
	}

	/*
		get all iwant user in an array
		if $sSearchSql is given, query only match users
		example1: $aCUsers = CUser::aAlluser('addr_id=12')
		example3: $aCUsers = CUSer::aAlluser('','ORDER BY createtime DESC LIMIT 0,10')
		CAUTION: this function may query lots of data from example_user DB, make sure you need all of these users
	*/
	/**
	 * @desc 取得人員、能搜尋特定會員
	 * @controller/CuserAdmin.php 拋值過來
	 *  @param int $sSearchSql where 條件
	 *  @param int $aBinds 搜尋的關鍵字
	 *  @param int $sPostFix limit  的限制
	 * @created 2017/02/24
	 */		
	static public function aAllUser($sSearchSql='',$aBinds=array(),$sPostFix=''){
		// echo  "<pre>";print_r($aBinds);
		$oDB = self::oDB(self::$sDBName);
		$aAllUser = array();
		$sSql = "SELECT * FROM `example_user`";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		if($sPostFix!=='')
			$sSql .= " $sPostFix";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		while($aRow = $oDB->aFetchAssoc($iDbq)){
			if(!isset(self::$aInstancePool[$aRow['user_no']]))
				self::$aInstancePool[$aRow['user_no']] = new CUser($aRow);
			$aAllUser[] = self::$aInstancePool[$aRow['user_no']];
		}
		return $aAllUser;
	}
	
	/*
		get all group user in an array
		if $sSearchSql is given, query only match users
		example1: $aCUsers = CUser::aGroupUser('example_group_user_rel.group_no=3')
		example2: $aCUsers = CUser::aGroupUser('example_group_user_rel.group_no IN (3,26)')
		example3: $aCUsers = CUser::aGroupUser('example_group_user_rel.group_no=3','ORDER BY example_user.createtime DESC LIMIT 0,10')
		caution: this function may query lots of data from example_user DB, make sure you need all of these users
	*/

	static public function aGroupUser($sSearchSql='',$aBinds=array(),$sPostFix=''){
		// echo 'in';
		$oDB = self::oDB(self::$sDBName);
		$aAllUser = array();

		$sSql = "SELECT * FROM `example_user` INNER JOIN `example_group_user_rel` ON example_user.user_no=example_group_user_rel.user_no";
		if ($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		if($sPostFix!=='')
			$sSql .= " $sPostFix";	
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		while($aRow = $oDB->aFetchAssoc($iDbq)) {
			$aAllUser[] = new CUser($aRow);
		}
		return $aAllUser;
	}

	/*
		get count of user who match query
	*/

	/**
	 * @desc 取得人員總筆數
	 * @controller/CuserAdmin.php 拋值過來
	 *  @param int $sSearchSql where 條件
	 *  @param int $aBinds 搜尋的關鍵字
	 * @created 2017/02/24
	 */	
	static public function iGetCount($sSearchSql='',$aBinds=array()){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT count(user_no) as total FROM example_user";
		
		if($sSearchSql!==''){
			$sSql .= " WHERE $sSearchSql";
			// echo 'sSearchSql:'.$sSearchSql;exit;
		}
			

		$iDbq = $oDB->iQuery($sSql,$aBinds);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow!==false)
			$iCount = (int)$aRow['total'];
		else
			$iCount = 0;
		return $iCount;
	}
	
	/*
		login $oCurrentUser, and set in $session
	*/

	/**
	 * @desc 使用者輸入帳密登入
	 * @admin/login.php 拋值過來
	 *  @param int $sUserAccount 帳號
	 *  @param int $sUserPassword 密碼
	 * @created 2017/02/24
	 */
	 	
	static public function vLogin($sUserAccount,$sUserPassword){
		$session = self::$session;

		$oCUser = self::oFindUserByAcc($sUserAccount);	//find user by acc
		if(!isset($oCUser))
			throw new Exception("CUser: not such user account($sUserAccount)");
		if($oCUser->bStatus=='0')
			throw new Exception("CUser: user account($sUserAccount) is deprecated");

		$oCUser->vAuthorize($sUserPassword);	//check the password

		//set oCurrentUser in session
		$session->sess_unset();
		$session->set('oCurrentUser', 	$oCUser);

		//for genesis & queue
		$session->set('user_name',$oCUser->sName);
		$session->set('user_no',$oCUser->iUserNo);
		$session->set('user_password',$sUserPassword);
	}

	/*
		find & get $oCUser by user_account
	*/

	/**
	 * @desc 取出人員帳號資料
	 * @param int $sUserAccount 使用者輸入的帳號
	 * @created 2017/02/24
	 */	
	static public function oFindUserByAcc($sUserAccount){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT * FROM example_user WHERE user_account='$sUserAccount'";
		// $sSql = "SELECT * FROM example_user WHERE `user_account`=?";
		$iDbq = $oDB->iQuery($sSql,array($sUserAccount));
		$aRow = $oDB->aFetchAssoc($iDbq);
	
		if(!$aRow || $oDB->iNumRows($iDbq)>1)
			return null;

		$iUserNo = $aRow['user_no'];
		
		// if(!is_null(self::$aInstancePool[$aRow['user_no']]))
		if(isset(self::$aInstancePool[$aRow['user_no']]))
			return self::$aInstancePool[$aRow['user_no']];

		$oCUser = new CUser($aRow);
		self::$aInstancePool[$iUserNo] = $oCUser;
		return $oCUser;
	}

	/*
		find & get $0CUser by user_account without @domain.name
	*/
	static public function oFindUserByShortAcc($sShortAccount){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT * FROM example_user WHERE `user_account` LIKE ?";
		$iDbq = $oDB->iQuery($sSql,array("$sShortAccount%"));
		$aRow = $oDB->aFetchAssoc($iDbq);
		if(!$aRow || $oDB->iNumRows($iDbq)>1)
			return null;

		if(!is_null(self::$aInstancePool[$aRow['user_no']]))
			return self::$aInstancePool[$aRow['user_no']];

		$oCUser = new CUser($aRow);
		self::$aInstancePool[$iUserNo] = $oCUser;
		return $oCUser;
	}

	/*
		find & get $oCUser by any field and any value targeted
	*/
	static public function oFindUserByField($sField,$sValue){
		$oDB = self::oDB(self::$sDBName);
		$sSql = $sSql = "SELECT * FROM example_user WHERE `$sField`=?";
		$iDbq = $oDB->iQuery($sSql,array($sValue));
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow === false || $oDB->iNumRows($iDbq)>1)
			return null;

		if(!is_null(self::$aInstancePool[$aRow['user_no']]))
			return self::$aInstancePool[$aRow['user_no']];

		$oCUser = new CUser($aRow);
		self::$aInstancePool[$iUserNo] = $oCUser;
		return $oCUser;
	}

	/*
		constructor of $oCUser
		some class member are essential, must be initialized, or throw exception
		some class member are optional, may not be initialized
	*/

	//class function
	/*
		public function to check if $oCUser match the password
	*/

	/**
	 * @desc 確認密碼
	 * @從function Vlogin 呼叫
	 * @param int $sUserPassword 使用者輸入的密碼
	 * @created 2017/02/24
	 */		
	public function vAuthorize($sUserPassword){
		if(!isset($this->sPassword))
			throw new Exception("CUser->vAuthorize: this user has no password; not allowed to login!");
		if($this->sPassword!==md5($sUserPassword))
		// if($this->sPassword!==$sUserPassword)
			throw new Exception("CUser->vAuthorize: user and password not match!");
	}
	
	/* 
		set and get group of $oCUser
	*/

	/**
	 * @desc 該人員所屬的群組
	 * @created 2017/02/24
	 */	
	public function aGroup(){
		$oDB = self::oDB(self::$sDBName);
		if(empty($this->__aCGroup)){
			$sSql = "SELECT * FROM example_group_user_rel WHERE user_no = '{$this->iUserNo}'";
			$iDbq = $oDB->iQuery($sSql);
			while($aRow = $oDB->aFetchAssoc($iDbq)) {
				$this->__aCGroup[$aRow['group_no']] = CGroup::oGetGroup($aRow['group_no']);
			}
		}
		return $this->__aCGroup;
	}

	/*
		set and get rule of $oCUser
	*/

	public function aRule(){
		
		$oDB = self::oDB(self::$sDBName);
		if(empty($this->__aCRule)){
			$sGroups = array();
			foreach($this->aGroup() AS $oCGroup) {
				$aGroups[] = $oCGroup->iGroupNo;
			}
			
			if(empty($aGroups))
				return $this->__aCRule;

			$sGroups = implode(",", $aGroups);
			$sSql = "SELECT * FROM example_group_rule_rel WHERE group_no in ($sGroups)";
			$iDbq = $oDB->iQuery($sSql);
			while($aRow = $oDB->aFetchAssoc($iDbq)) {
				$this->__aCRule[$aRow['rule_no']] = CRule::oGetRule($aRow['rule_no']);
			}
		}
		return $this->__aCRule;
	}

	/*
		check if $oCUser is allow to access target page
		if not, throw exception
	*/
	/**
	 * @desc 該人員所屬的群組,是否有權限進入人員資料
	 * @從admin/index.php 呼叫,會給予頁面及執行的動作
	 * @param str $sFunc 頁面
	 * @param str $sAction 動作
	 * @created 2017/02/24
	 */
	public function IsPermit($sFunc,$sAction){

		if($sFunc=='' && $sAction=='')
			return;
		
		if($this->iUserNo==1) return;
		
		//get rule object by func and action
		$aRule = CRule::aAllRule("func='$sFunc' AND action='$sAction'");
		
		if(!empty($aRule)){
			$oDB = self::oDB(self::$sDBName);
			foreach ($aRule as $oRule) {
				$sSql = "SELECT * FROM example_group_rule_rel AS a 
					LEFT JOIN example_group_user_rel AS b
					 ON a.group_no=b.group_no 
					 WHERE a.rule_no='$oRule->iRuleNo'
					  AND b.user_no='$this->iUserNo'";
				$iDbq = $oDB->iQuery($sSql);
				if($oDB->iNumRows()!=0)
					return;
			}
		}
		throw new Exception("CUser->IsPermit: current user is not allow to this page");
	}

	/*
		write log about what this user is doing
	*/
	/**
	 * @desc 寫入log檔
	 * @從admin/index.php 呼叫,會給予頁面及執行的動作
	 * @param str $sTableName table名稱
	 * @param str $sTableId 該table變更的id
	 * @param str $sTableFunc 更改的形式會員、群組、權限
	 * @param str $sTableAction 變更的狀態為
	 * @created 2017/02/24
	 */	
	public function vAddUserLog($sTableName="",$sTableId="",$sTableFunc="",$sTableAction=""){
		try{
			$aLogInfo = array(	'user_no'=>$this->iUserNo,
								'table_name'=>$sTableName,
								'table_id'=>$sTableId,
								'table_func'=>$sTableFunc,
								'table_action'=>$sTableAction
                            	);
			// echo  "<pre>";print_r($aLogInfo).'<br>';exit;
			$oCUSerLog = new CUserLog($aLogInfo);
			$oCUSerLog->iAddLog();
		}catch (Exception $e){
			throw new Exception("CUser->vAddUserLog: ".$e->getMessage());
		}
	}

	/*
		update user data in this CUser to example_user DB
		if you want to update user data in DB, get a CUser of that user, change member value, and call this function
		$oCUser->iUserNo & sAccount are not changeable
	*/
	/**
	 * @desc 更新人員
	 * @created 2017/02/24
	 */
	public function vUpdateUser(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');
		
		// $aValues = array(	'user_name'=>$this->sName, 
		// 					'user_password'=>$this->sPassword,
		// 					'user_email'=>$this->sEmail,
		// 					'user_tel'=>$this->sTel,
		// 					'user_mobile'=>$this->sMobile,
		// 					'status'=>$this->bStatus
		// 					);
		
		$aField = array(	'user_name', 
							// 'user_account',
							'user_password',
							'user_email',
							'user_tel',
							'user_mobile',
							'status'
							);
		$aValues = array(	$this->sName, 
							// $this->sAccount,
							$this->sPassword,
							$this->sEmail,
							$this->sTel,
							$this->sMobile,
						    $this->bStatus
							);

		// echo  "<pre>";print_r($aValues);exit;

		try{
			$oDB->vBegin();
			$oDB->sUpdate("example_user", $aField ,$aValues, "`user_no` = {$this->iUserNo}");
			$oDB->vDelete('example_group_user_rel',"`user_no`={$this->iUserNo}");
			foreach ($this->__aCGroup as $oCGroup) {
				// $aGpValues = array(	'group_no'=>$oCGroup->iGroupNo,
				// 						'user_no'=>$this->iUserNo
				// 						);
				
				$aGpField = array(	'group_no',
										'user_no'
										);
				$aGpValues = array(	$oCGroup->iGroupNo,
										$this->iUserNo
										);

				$oDB->sInsert('example_group_user_rel',$aGpField ,$aGpValues);
			}
			$oDB->vCommit();

			$oCurrentUser->vAddUserLog("example_user",$this->iUserNo,'user','edit');
		}catch (Exception $e){
			$oDB->vRollback();
			throw new Exception("CUser->vUpdateUser: ".$e->getMessage());
		}
	}


	/**
	 * 更新使用者密碼
	 *
	 * @param int $user_no 使用者序號
	 * @param int $UserData 使用者資料
	 */
	static public function UpdateUser($user_no, $UserData) {
		$oDB = self::oDB(self::$sDBName);
			
		$aField = array_keys($UserData);
		$aValue = array_values($UserData);
		
		$sql = $oDB->sUpdate("`example_user`", $aField, $aValue, "`user_no` = $user_no");	

		if(!$sql) trigger_error("update user fail", E_USER_ERROR);	
	}
	
	/*
		add $oCUser data to example_user DB
		if you want to create a new user in DB, new a CUser and call this function
		if account already exist, throw exception
		return insert id(user_no), user may find this new CUser by user_no
	*/
	/**
	 * @desc 新增人員
	 * @created 2017/02/24
	 */	
	public function iAddUser(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');
		$oCUser = self::oFindUserByAcc($this->sAccount);	//find user by acc
		if(isset($oCUser))
			throw new Exception("CUser->vAddUser: user account($this->sAccount) already exist");
		// $aValues = array(	'user_name'=>$this->sName, 
		// 					'user_account'=>$this->sAccount,
		// 					'user_password'=>$this->sPassword,
		// 					'user_email'=>$this->sEmail,
		// 					'user_tel'=>$this->sTel,
		// 					'user_mobile'=>$this->sMobile,
		// 					'createtime'=>date("Y-m-d H:i:s"),
		// 					'status'=>$this->bStatus
		// 					);

		$aValues = array(	$this->sName, 
							$this->sAccount,
							$this->sPassword,
							$this->sEmail,
							$this->sTel,
							$this->sMobile,
							date("Y-m-d H:i:s"),
							$this->bStatus
							);

		$aField = array(	'user_name', 
							'user_account',
							'user_password',
							'user_email',
							'user_tel',
							'user_mobile',
							'createtime',
							'status'
							);

		try{
			$oDB->vBegin();
			$oDB->sInsert('example_user',$aField,$aValues);
			$this->iUserNo = $oDB->iGetInsertId();
			foreach ($this->__aCGroup as $oCGroup) {
				$aGpValues = array(	'group_no'=>$oCGroup->iGroupNo,
										'user_no'=>$this->iUserNo
										);
				$oDB->sInsert('example_group_user_rel',$aGpValues);
			}
			$oDB->vCommit();
			$oCurrentUser->vAddUserLog("example_user",$this->iUserNo,'user','add');

			return $this->iUserNo;
		}catch (Exception $e){
			$oDB->vRollback();
			throw new Exception("CUser->iAddUser: ".$e->getMessage());
		}
	}

	/*
		delete user data from DB by user_no, does not require a oCuser to 
	*/

	/*
		activate this oCUser
	*/
	/**
	 * @desc 更新人員狀態
	 * @created 2017/02/24
	 */	
	public function vActivate(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');
		
		if($this->bStatus==='1')
			$this->bStatus='0';
		else
			$this->bStatus='1';
		// $aValues = array('status'=>$this->bStatus);
		$aField = array('status');
		$aValues = array($this->bStatus);

		try{
			$oDB->sUpdate("example_user",$aField, $aValues, "`user_no` = {$this->iUserNo}");
			$oCurrentUser->vAddUserLog("example_user",$this->iUserNo,'user','activate');
		}catch (Exception $e){
			throw new Exception("CUser->vActivate: ".$e->getMessage());
		}
	}

	/*
		change password
	*/
	/**
	 * @desc 更新人員密碼
	 * @created 2017/02/24
	 */		
	public function vChangePassword($sPassword){
		// $this->sPassword = $sPassword;
		$this->sPassword = md5($sPassword);
	}

	/*
		set groups by array(group_no)
	*/
	public function vSetGroups($aGroupNos){
		if(!is_array($aGroupNos))
			return;
		$aGroupNos = array_unique($aGroupNos);
		$this->__aCGroup = array();	//clear all group
		foreach ($aGroupNos as $iGroupNo) {
			$this->__aCGroup[$iGroupNo] = new CGroup(array('group_no'=>$iGroupNo));
		}
	}
}
?>