<?php

/********************************************************************
 * @heading(標題):
 *			CCategory 分類
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			分類
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) : 分類
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/model/CGalaxyClass.php');

class CCategory extends CGalaxyClass
{
	static private $aCCategoryPool = array();

	private $iCategoryNo;
	public $sName;
	public $iParentCategoryNo;

	public $aChildCategory =array();
	public $aCRule = array();

	//database setting
	static protected $sDBName = 'EXAMPLE';

	//instance pool
	static public $aInstancePool = array();

	/*
		get $oCCategory by certain category_no
	*/
	/**
	 * @desc 取得所有分類
	 * @created 2017/02/24
	 */	
	static public function oGetCategory($iCategoryNo){
		// echo 'IN'.$iCategoryNo;exit;
		$oDB = self::oDB(self::$sDBName);
		// $sSql = "SELECT * FROM example_rule_category WHERE 1=1";
		$sSql = "SELECT * FROM example_rule_category WHERE category_no = $iCategoryNo";
		$iDbq = $oDB->iQuery($sSql);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow ===false || $oDB->iNumRows($iDbq)>1)
			return null;
		$oCCategory = new CCategory($aRow);
		self::$aInstancePool[$iCategoryNo] = $oCCategory;
		return $oCCategory;
	}

	/*
		get all category in an array
		if $sSearchSql is given, query only match categories
		CAUTION: this function returns a map , instead of array(for tree recursive function)
	*/
	/**
	 * @desc 撈出分類 
	 *  @param int $sSearchSql ,where條件
	 * @created 2017/02/24
	 */	
	static public function aAllCategory($sSearchSql='',$aBinds=array(),$sPostFix=''){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT * FROM example_rule_category";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		if($sPostFix!=='')
			$sSql .= " $sPostFix";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		$aAllCategory = array();
		while($aRow = $oDB->aFetchAssoc($iDbq)){
			if(!isset(self::$aInstancePool[$aRow['category_no']])){
				self::$aInstancePool[$aRow['category_no']] = new CCategory($aRow);
			}
			$aAllCategory[$aRow['category_no']] = self::$aInstancePool[$aRow['category_no']];
		}
		return $aAllCategory;
	}

	/*
		get count of category which match query
	*/
	/**
	 * @desc 分類的個數 
	 *  @param int $sSearchSql ,where條件
	 * @created 2017/02/24
	 */		
	static public function iGetCount($sSearchSql='',$aBinds=array()){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT count(category_no) as total FROM example_rule_category";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow!==false)
			$iCount = (int)$aRow['total'];
		else
			$iCount = 0;
		return $iCount;
	}	

	public function __construct($multiData){
		parent::__construct($multiData);
		$this->iCategoryNo = $multiData['category_no'];
		$this->sName = $multiData['category_name'];
		$this->iParentCategoryNo = $multiData['parent_category_no'];
		$this->bStatus = $multiData['status'];
		$this->sCreateTime = $multiData['createtime'];
		$this->sModifiedTime = $multiData['modifiedtime'];
	}

	public function __get($varName)
    {
        return $this->$varName;
    }
}
?>