<?php

/********************************************************************
 * @heading(標題):
 *			CDate 日期
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			日期
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) : 日期
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

class CDate 
{

	public function __construct()
	{
	
	}
	
	
	/**
	* @desc ­pºâ¨â­Ó¤é´Á®t´X¤Ñ
	* @created 2012/07/23
	*/
	 /**
	 * @desc 起訖的天數
	 * @created 2017/02/24
	 */
	public static function dateDiff($startTime, $endTime) {
		$start = strtotime($startTime);
		$end = strtotime($endTime);
		$timeDiff = $end - $start;
		return floor($timeDiff / (60 * 60 * 24));
	}

	





}
?>
