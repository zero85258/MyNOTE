<?php

/********************************************************************
 * @heading(標題):
 *			CRule 系統
 * @author(作者) :
 * 			Jerry He
 * @purpose(目的) :
 *			系統功能
 * @usage(用法) :
 * @reference(參考資料) :
 * @restriction(限制) :
 * @revision history(修改紀錄) :
 *			修改日期:
 *			修改人姓名:
 *			修改內容:
 * @copyright(版權所有) :
 *			銀河互動網路股份有限公司 iWant-in inc.
 * @note(說明) :
 * @created(建立日期) :
 *			2017/02/24
 ********************************************************************/

include_once('../inc/model/CGalaxyClass.php');
include_once('../inc/model/CCategory.php');
class CRule extends CGalaxyClass
{
	//vital member
	private $iRuleNo;	//rule_no in example_user.example_rule, read only after construct
	//optional member
	public $sName;
	public $sDesc;
	public $sFunc;
	public $sAction;
	public $iCategoryNo;
	private $__oCCategory;
	//database setting
	static protected $sDBName = 'EXAMPLE';

	//instance pool
	static public $aInstancePool = array();

	/*
		get $oCRule by certain rule_no
	*/
	static public function oGetRule($iRuleNo=null){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT * FROM example_rule WHERE rule_no = '$iRuleNo'";
		$iDbq = $oDB->iQuery($sSql);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if(!$aRow || $oDB->iNumRows($iDbq)>1)
			return null;
		$oCRule = new CRule($aRow);
		self::$aInstancePool[$iRuleNo] = $oCRule;

		return $oCRule;
	}

	/*
		get all rule in an array
		if $sSearchSql is given, query only match rules
		example1:$aCRules = CRule::aAllRule("category_no=5");
		example2:$aCRules = CRule::aAllRule("func='puppets'");
	*/
	/**
	 * @desc 會給予sql example_rule的結果 
	 *  @param int $sSearchSql ,where條件
	 * @created 2017/02/24
	 */
	static public function aAllRule($sSearchSql='',$aBinds=array(),$sPostFix=''){
		$oDB = self::oDB(self::$sDBName);
		$aAllRule = array();
		$sSql = "SELECT * FROM example_rule";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		if($sPostFix!=='')
			$sSql .= " $sPostFix";

		$iDbq = $oDB->iQuery($sSql,$aBinds);
		while($aRow = $oDB->aFetchAssoc($iDbq)){
			if(!isset(self::$aInstancePool[$aRow['rule_no']])){
				self::$aInstancePool[$aRow['rule_no']] = new CRule($aRow);
			}
			$aAllRule[] = self::$aInstancePool[$aRow['rule_no']];
		}
		return $aAllRule;
	}

	/*
		get all rule sorted by category
	*/

	/**
	 * @desc 找出分類 
	 * @param int $sSearchSql ,where條件
	 * @created 2017/02/24
	 */
	static public function aAllRuleInCategory(){
		$oDB = self::oDB(self::$sDBName);
		$aCategoryPool = CCategory::aAllCategory();


		$aCategoryTree =array();
		foreach ($aCategoryPool as $iCategoryNo => $oCCategory) {
			//echo $iCategoryNo;
			if($oCCategory->iParentCategoryNo==='0'){
				$aCategoryTree[$iCategoryNo] = $oCCategory;
			}else{	
				$oParentCategory = $aCategoryPool[$oCCategory->iParentCategoryNo];	//pointer to parent category object
				$oParentCategory->aChildCategory[$iCategoryNo] = $oCCategory;	//add child category pointer to parent's child category array
			}
		}




		$sSql = "SELECT * FROM example_rule";
		$iDbq = $oDB->iQuery($sSql);
		while($aRow = $oDB->aFetchAssoc($iDbq)){
			$oCRule = new CRule($aRow);
			$oCCategory = $aCategoryPool[$oCRule->iCategoryNo];	//pointer to that category object
			$oCCategory->aCRule[$oCRule->iRuleNo] = $oCRule;
		}

		return $aCategoryTree;
	}


	/*
		get count of rule which match query
	*/

	/**
	 * @desc 算出個數 
	 * @param int $sSearchSql ,where條件
	 * @created 2017/02/24
	 */
	static public function iGetCount($sSearchSql='',$aBinds=array()){
		$oDB = self::oDB(self::$sDBName);
		$sSql = "SELECT count(rule_no) as total FROM example_rule";
		if($sSearchSql!=='')
			$sSql .= " WHERE $sSearchSql";
		$iDbq = $oDB->iQuery($sSql,$aBinds);
		$aRow = $oDB->aFetchAssoc($iDbq);
		if($aRow!==false)
			$iCount = (int)$aRow['total'];
		else
			$iCount = 0;
		return $iCount;
	}

	/*
		constructor of $oCRule
		some class member are essential, must be initialized, or throw exception
		some class member are optional, may not be initialized
	*/
	/**
	 * @desc 定義每個資料讀取的名稱 
	 * @param int $sSearchSql ,where條件
	 * @created 2017/02/24
	 */	
	public function __construct($multiData){
		parent::__construct($multiData);
		if(!is_array($multiData))
			throw new Exception("CRule: __construct failed, require an array");
		//initialize vital member
		$this->iRuleNo = $multiData['rule_no'];
		$this->sName = isset($multiData['rule_name'])?$multiData['rule_name']:"";

		$this->sDesc = isset($multiData['rule_desc'])?$multiData['rule_desc']:"";
		$this->sFunc = isset($multiData['func'])?$multiData['func']:"";
		$this->sAction = isset($multiData['action'])?$multiData['action']:"";
		$this->iCategoryNo = isset($multiData['category_no'])?$multiData['category_no']:0;
		//galaxy class memeber
		$this->bStatus = isset($multiData['status'])?$multiData['status']:0;
	}

	//php default function, let private member become read-only class member for others
    public function __get($varName)
    {
        return $this->$varName;
    }

    /*
		set & get category of this rule
    */
	public function oCategory(){
		if(empty($this->__oCCategory)){
			$this->__oCCategory = CCategory::oGetCategory($this->iCategoryNo);
		}
		return $this->__oCCategory;
	}

	/*
		update rule data in this CRule to example_user DB
		if you want to update rule data in DB, get a CRule of that rule, change member value, and call this function
		$oCRule->iRuleNo is not changeable
	*/
	/**
	 * @desc 更新系統
	 * @created 2017/02/24
	 */	
	public function vUpdateRule(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');

		// $aValues = array(	'rule_name'=>$this->sName, 
		// 					'rule_desc'=>$this->sDesc,
		// 					'func'=>$this->sFunc,
		// 					'action'=>$this->sAction,
		// 					'category_no'=>$this->iCategoryNo,
		// 					'status'=>$this->bStatus
		// 					);

		$aField = array(	'rule_name', 
							'rule_desc',
							'func',
							'action',
							'category_no',
							'status'
							);
		$aValues = array(	$this->sName, 
							$this->sDesc,
							$this->sFunc,
							$this->sAction,
							$this->iCategoryNo,
							$this->bStatus
							);

		try{
			$oDB->sUpdate("`example_rule`", $aField, $aValues, "`rule_no` = {$this->iRuleNo}");
			$oCurrentUser->vAddUserLog("example_rule",$this->iRuleNo,'rule','update');
		}catch (Exception $e){
			throw new Exception("CRule->vUpdateRule: ".$e->getMessage());
		}
	}

	/*
		add $oCRule data to example_user DB
		if you want to create a new rule in DB, new a CRule and call this function
		return insert id(rule_no), user may find this new CRule by rule_no
	*/
	/**
	 * @desc 新增系統
	 * @created 2017/02/24
	 */	
	public function iAddRule(){
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');

		// $aValues = array(	'rule_name'=>$this->sName, 
		// 					'rule_desc'=>$this->sDesc,
		// 					'func'=>$this->sFunc,
		// 					'action'=>$this->sAction,
		// 					'category_no'=>$this->iCategoryNo,
		// 					'status'=>$this->bStatus
		// 					);

		$aField = array(	'rule_name', 
							'rule_desc',
							'func',
							'action',
							'category_no',
							'status'
							);
		$aValues = array(	$this->sName, 
							$this->sDesc,
							$this->sFunc,
							$this->sAction,
							$this->iCategoryNo,
							$this->bStatus
							);



		try{
			$oDB->sInsert("`example_rule`",$aField, $aValues);
			$this->iRuleNo = $oDB->iGetInsertId();
			$oCurrentUser->vAddUserLog("example_rule",$this->iRuleNo,'rule','add');
			return $this->iRuleNo;
		}catch (Exception $e){
			throw new Exception("CRule->iAddRule: ".$e->getMessage());
		}
	}

	/**
	 * @desc 更新系統狀態
	 * @created 2017/02/24
	 */
	public function vActivate() {
		$oDB = self::oDB(self::$sDBName);
		$oCurrentUser = self::$session->get('oCurrentUser');
		try{
			if($this->bStatus==='1')
				$this->bStatus='0';
			else
				$this->bStatus='1';
			$aField = array('status');
			$aValues = array($this->bStatus);

			$oDB->sUpdate("`example_rule`",$aField, $aValues,"`rule_no`={$this->iRuleNo}");
			$oCurrentUser->vAddUserLog("example_rule",$this->iRuleNo,'rule','activate');
		}catch(Exception $e){
			throw new Exception("CRule->vActivate: ".$e->getMessage());
		}

	}

	
	public function vOverWrite($oCRule){
		//if not a CAccounts object or uuid not match
		if(get_class($oCRule)!=='CRule' || $this->iRuleNo!==$oCRule->iRuleNo)
			throw new Exception('CRule->vOverWrite: fatal error');
			
		foreach ($this as $name => $value) {
			if($name==='iRuleNo')
				continue;
			$this->$name = $oCRule->$name;	//overwrite
		}
	}
}
?>