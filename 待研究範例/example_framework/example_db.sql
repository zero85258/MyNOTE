-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: 2016 年 06 月 09 日 08:17
-- 伺服器版本: 5.1.73
-- PHP 版本: 5.5.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `example_db`
--

-- --------------------------------------------------------

--
-- 資料表結構 `example_group`
--

CREATE TABLE IF NOT EXISTS `example_group` (
  `group_no` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '群組編號',
  `group_name` varchar(100) NOT NULL COMMENT '群組名稱',
  `group_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '群組狀態',
  PRIMARY KEY (`group_no`),
  KEY `dept_name` (`group_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='群組' AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 資料表結構 `example_group_rule_rel`
--

CREATE TABLE IF NOT EXISTS `example_group_rule_rel` (
  `group_no` int(10) NOT NULL,
  `rule_no` int(10) NOT NULL,
  KEY `group_no` (`group_no`),
  KEY `rule_no` (`rule_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='群組和權限的關聯表';

-- --------------------------------------------------------

--
-- 資料表結構 `example_group_user_rel`
--

CREATE TABLE IF NOT EXISTS `example_group_user_rel` (
  `group_no` int(10) NOT NULL,
  `user_no` int(10) NOT NULL,
  KEY `group_no` (`group_no`),
  KEY `user_no` (`user_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='群組和使用者的關聯表';

-- --------------------------------------------------------

--
-- 資料表結構 `example_rule`
--

CREATE TABLE IF NOT EXISTS `example_rule` (
  `rule_no` int(10) NOT NULL AUTO_INCREMENT COMMENT '權限序號',
  `rule_name` varchar(100) NOT NULL COMMENT '權限名稱',
  `rule_desc` varchar(100) NOT NULL COMMENT '權限描述',
  `func` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL,
  `category_no` int(10) NOT NULL COMMENT '分類編號',
  `status` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rule_no`),
  KEY `func` (`func`),
  KEY `action` (`action`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='權限' AUTO_INCREMENT=63 ;

-- --------------------------------------------------------

--
-- 資料表結構 `example_rule_category`
--

CREATE TABLE IF NOT EXISTS `example_rule_category` (
  `category_no` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(30) NOT NULL,
  `parent_category_no` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createtime` datetime NOT NULL,
  `modifiedtime` datetime NOT NULL,
  PRIMARY KEY (`category_no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- 資料表結構 `example_user`
--

CREATE TABLE IF NOT EXISTS `example_user` (
  `user_no` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `user_name` varchar(100) NOT NULL,
  `user_account` varchar(60) NOT NULL COMMENT '帳號',
  `user_password` varchar(70) NOT NULL COMMENT '密碼',
  `user_email` varchar(45) NOT NULL DEFAULT ' ' COMMENT '電子郵件',
  `user_tel` varchar(45) NOT NULL DEFAULT ' ' COMMENT '電話',
  `user_mobile` varchar(15) NOT NULL DEFAULT ' ' COMMENT '手機號碼',
  `createtime` datetime NOT NULL COMMENT '建立日期',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '狀態 1開/0關',
  PRIMARY KEY (`user_no`),
  KEY `admin_user` (`user_account`),
  KEY `admin_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='基本使用者' AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- 資料表結構 `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `lang_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang_func` varchar(32) NOT NULL DEFAULT '',
  `lang_type` varchar(8) NOT NULL DEFAULT '',
  `lang_name` varchar(64) NOT NULL DEFAULT '',
  `lang_value` varchar(255) NOT NULL DEFAULT '',
  `lang_desc` varchar(255) DEFAULT NULL,
  `is_where` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `lang_name` (`lang_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
