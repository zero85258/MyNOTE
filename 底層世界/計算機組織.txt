合語言不區分大小寫!!!!!!!!MOV=mov

字節BYTE(8bit)
字型2 BYTE(16bit)
雙字(32bit)


CPU跟reg靠以下3條線傳輸
數據總線(資料)
位址總線(0x????)
控制總線(操作碼)

一般指令:可直接轉成機械碼
假指令:交給編譯器處理,無法直接轉成機械碼

BIOS(屬韌體):Basic Input/Output System
韌體:控制硬體的軟體

CPU暫存器{
  通用暫存器: AX,BX,CX,DX (幾乎千古不變)
  AX,BX,CX,DX存放一般數據(4個暫存器都是16bit)
  AX,BX,CX,DX-->AH,AL,BH,BL........(為了連8位元系統也可以使用)
  節段暫存器:CS(代碼),DS(資料),SS(堆疊),ES
}


地址加法器(節段位址"CS",偏移位址"IP"){
  實際位址(20bit)＝節段位址CS(16bit) * 10H(Lshift4bit) + 偏移位址IP(16bit);
  //CS:IP->>>2000:1F60  
  //ex: 21F60H = 2000H * 10H + 1F60H
  return 實際位址(20bit);  //實際位址 == cpu 與 reg之間 交流用的
}//被CS:IP指到的是 ”指令”(就是程式碼)


CPU運作:       (Youtube???言零基?教程10（小甲?主?） ?清)
1.CPU 找到 reg實際位址(IP指向的位置) 
2.從 "實際位址" 取得 "操作碼"  假設:實體位置 == mov a1,a2 所以操作碼就是"mov"
3.把"操作碼"告訴"指令緩衝器"然後執行"mov a1,a2"
4.因為"mov a1,a2"有3個字節(共3byte),IP知道了以後就IP=IP+3 (IP=IP+所讀取的指令長度)
5.下一道指令  ex:jp begin;


只有"跳躍指令"能改CS:IP
修改CS,IP內容{
  jmp CS:IP
  //ex:jmp 2AE3:3; (跳到2AE33H =2AE3H*10H+3H)
//ex:jmp 3:0B16; (跳到0B46H =3H*10H+0B16H)
}
修改IP內容{
  jmp AX
}

節段暫存器:CS(代碼暫存器),DS(暫存器),SS,ES

“數” 不能直接放到 ”節段暫存器”
只能 “數” -> “AX,BX,CX,DX(通用暫存器)” ->”節段暫存器”
//ex:mov ax,1000h
//  mov ds,ax


MOV AX,[0] (0是偏移位址) ([0]是內存單元)

堆疊暫存器SS:SP(大陸:棧,指向頂端):
SS:SP(堆疊頂端節段位址"SS",堆疊頂端偏移位址"SP")
push ax; (放入字型2 BYTE(16bit)) sp-2(向上)
pop ax;(取出字型2 BYTE(16bit)) sp+2(向下)

//ex:將10000h~1000FH當作堆疊,ax,bx,ds推入堆疊中
//ex mov ax,1000h
//  mov ss,ax
//  mov sp,0010h(設置堆疊旗標)
//  push ax
//  push bx
//  push ds
!!!暫存器歸0方法(ax=0;)
(最簡單)Mov ax,0(3字節)
(進階)sub ax,ax(2字節)
(進階)xor ax,ax(2字節)


Assume cs:段名
段名 segment
Start: mov…
	 Add……
	 Mov….
	 Mov ax,4c00h (這是程序返回)
	 Int 21h  (Int是中斷,不是c語言變數宣告)
段名 ends //編譯器的一段結束
End  //編譯器程序結束


//N的2^n次方(N,n){
	Mov ax,N
	For(n){
		Add ax,ax
	}
}