* 經典數字濾波器 FIR.IIR
* 現代數字濾波器 LMS.RLS

# FIR數字濾波器	有限脈衝響應濾波器

* 非遞歸(No-Recursive)型
* 屬於 線性非時變濾波器(LTI濾波器)
* Output只和當前及以前的Input信號有關。

* 〔優點〕
穩定(stable)
線性相位變化(linear phase shift)
* 〔缺點〕
需要較高階數來滿足濾波條件
計算效率較低




# IIR數字濾波器	無限脈衝響應濾波器

* 遞歸(Recursive)型          
* 屬於 線性非時變濾波器(LTI濾波器)
       
* Output不僅和當前及以前的Input信號有關，而且還和以前的Output信號有關。還採用了以前的Output作為反饋來得到當前的Output。

* 〔優點〕
僅需較低階數來滿足濾波條件
計算效率較高，
但運算要持續用到整段訊號
* 〔缺點〕
可能不穩定(unstable)，分母項為0
非線性相位變化(nonlinear phase shift
種類:
類比低通:
	Butterworth(簡稱BW LP)
	ChebyshevI,II(簡稱CBI,CBII)


# 卷積算法
短卷積 (加法會太多)
1.CooK-Toom
2.winograd



