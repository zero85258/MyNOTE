### 從軟到硬通用
```
x86 32位元的統稱。
```
### 硬的要命
```
STM32 arm的開發板

USB, I2C, SPI, UART protocol 及韌體測試。

I2C,SPI 都是一種 串列(序列)介面  這二者與主機間通訊用的 非同步串列通訊埠RS-232(UART) 非常不一樣
I2C 主要是為了讓MCU或CPU以較少的接腳數 連接眾多 的 低速週邊裝置之用(最主要的市場在電視機及 PC顯示器的控制器上). 

I2C裝置 分成 master裝置,slave裝置

SIMD單指令流多資料流
OpenCL和CUDA分別是目前最廣泛使用的開源和專利通用圖形處理器（GPGPU）運算語言

IC廠的韌體工程師~ 其實包山包海~

最底層 driver , 數位訊號使用 I2C ,SPI, USB, UART, PWM, IIS, ....，
撰寫系統 BIOS, RTOS, 
通訊相關， 2.4G RF , BLE等應用。
這些都可以用在 MCU上， 或說現在的8051也挺強大的。
C compiler, assembly(好多家的), Host API, MFC demo program.

BLE 一種低工耗的藍芽通訊協議

例如，8 bits 運算的CPU，應該儘量用 8 bits 的型態的變數(慣用int不是好習慣)；
有些CPU無有號運算的指令，沒必要就不要使用有號變數和運算(翻出來的程式碼會人嚇一跳)

OpenGL（英語：Open Graphics Library，譯名：開放圖形庫或者「開放式圖形庫」）是用於彩現2D、3D向量圖形的跨語言、跨平台的應用程式編程介面（API）
```

### 有硬有軟
```
嵌入式系統(ARM)開發 講白了就是linux核心
樹莓派 小電腦(有cpu(arm),ram,網卡......)
Shell 是一個用C語言編寫的程序，它是用戶使用Linux的橋樑。 Shell既是一種命令語言，又是一種程序設計語言。
Shell 是指一種應用程序，這個應用程序提供了一個界面，用戶通過這個界面訪問操作系統內核的服務。

GNU(一種OS)
S3c6410微處理機 一種ARM 11 S3C6410開發板
OK6410-A  一種ARM Linux開發板
OCR文字辨識軟體
```

### 軟的要命
```
nginx一種web server
MySQL資料庫的安裝  xampp
appserv
grunt、gulp、webpack 
ExtJS

yii 一種php框架
thinkphp 一種php框架
ci 一種php框架
MVC 架構是將整個專案分成 model、view、controller 三層,對於專案,大體上可以將 model 放資料庫邏輯,view 放顯示邏輯,controller放商業邏輯。


AWS 是安全的雲端服務平台，提供運算能力、資料庫儲存、內容交付及其他協助企業擴展和成長的功能
GCP Google雲端平台(Google Cloud Platform) 
Azure 微軟的雲端平台

RWD 響應式網頁設計,一般的手機版網站設計都是與電腦上的網站區隔開來另外再製作,所有事都得做2次   ex:appsgeyser??
PhoneGap 一個開放原始碼的"行動裝置開發框架"  ex:appsgeyser??
WTFast	一種遊戲專用vpn,為了增進遊戲網路效能

DLL 動態連結函式庫

caddy 一種webServer聽說想取代nginx


具有各方面應用的類別 (class)
Java API 為隨編譯器下載的官方類別庫，裡頭常見的應用包括
圖形介面
網頁
輸入及輸出
資料結構
時間處理
資料庫DB
數學計算MATH
網路
伺服器SERVER

DirectX 一種API 是Windows OS 專用的進階多媒體 API,它讓程式開發人不需要自行撰寫硬件程式碼，便可以使用各種硬件的功能

主從式架構(Client–server model),客戶端/伺服器(Client/Server)結構 簡稱C/S結構，是一種 網絡架構，
它把客戶端 (Client) （通常是一個採用圖形用戶界面的程序）與伺服器 (Server) 區分開來。
每一個客戶端軟體的實例都可以向一個伺服器或應用程式伺服器發出請求。
有很多不同類型的伺服器，例如文件伺服器、遊戲伺服器等。
```



### 其他觀念
```
MathLab
2. 有影像處理, 3A(AE,AF,AWB)背景尤佳


運算思考 (Computational Thinking)、理解演算法、深度學習等 AI 相關知識
Algorithmia 一家國外的"演算法商店"
```

### 與現世有趣的現象
```
x86作業系統 只能支援到4GB(或是更少的)記憶體容量，
而x64作業系統可以支援到超過4GB記憶體。
由於目前上市的各種筆電、桌機，很多電腦出貨時都已經安裝4GB記憶體了，
若是不安裝x64作業系統，那4GB的記憶體就似乎被……浪費了。
是的，這是正確的。
```
	
