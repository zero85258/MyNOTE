### 硬體快取
```
當CPU處理資料時，它會先到Cache中去尋找，
如果資料因之前的操作已經讀取而被暫存其中，就不需要再從  隨機存取記憶體（Main memory）中讀取資料

if(資料因之前的操作已經讀取而被暫存其中){	//就不需要再從隨機存取記憶體（Main memory）中讀取資料
	
}

因{
	一般CPU的執行速度>主記憶體讀取速度，主記憶體儲器周期（存取主記憶體儲器所需要的時間）為數個時鐘周期。
}果{
	因此若要存取主記憶體的話，就必須等待數個CPU周期從而造成浪費。
}


因{
	記憶體中「程式執行 與 資料存取 的 局域性行為」
}果{
	所以 提供「快取」,為了讓 資料存取速度 適應 CPU處理速度
}

即一定程式執行時間和空間內，被存取的代碼集中於一部分。

為了充分發揮快取的作用，不僅依靠「暫存剛剛存取過的資料」，
使用 硬體指令預測() 與 資料預取技術()	//儘可能把將要使用的資料預先從記憶體中取到快取裡。
```

### 網頁快取 
```
	if(首次_載入頁面){	//server端
		快取檔案則會寫 applocation/cache 目錄之中。
	}else{
		快取檔案取出
	}
	
	Server把這頁面(response)給 送出請求(request)的客戶端。
	
	if(請求的檔案已經過期){	//(expired)
		舊的快取頁面.刪除()
		Server更新快取()
		response_to(客戶端)
	}

	---頁面快取 (Page Cache)---Server Side--------------------------------------
	Cache 的核心思想其一：保存執行結果
	若執行內容牽涉耗時項目 (e.g., 資料庫查詢、Disk I/O)，Server 肯定直接爆掉 😱…，
	於是，一個簡單、容易實作的想法 — 保存當時產出的 HTML 頁面，
	甚至是經過 內容編碼 (e.g., gzip) 的表示資料，此作法又稱 頁面快取 (Page Cache)。
		
```
