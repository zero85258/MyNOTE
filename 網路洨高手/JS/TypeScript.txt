=== 概念 ==============================

	//教學網站
	https://github.com/kkbruce/TypeScript/blob/master/doc/zh-tw/Handbook.md#2.7

	副檔名.ts

=== 型別 ==================================

	//陣列泛型
	var list: Array<number> = [1, 2, 3];

	//某種程度上就是 泛型
	var notSure: any = 4;
	
=== 介面 ===================================


=== 類別 ====================================



//--- .ts ------------------------------------
class Greeter {
	greeting: string;
	constructor(message: string){
		this.greeting = message;
	}
    	greet(){
		return "Hello, " + this.greeting;
	}
}

	var greeter: Greeter;
	greeter = new Greeter("world");
	alert(greeter.greet());

//--- .js ------------------------------------
var Greeter = (function () {
	function Greeter(message) {
		this.greeting = message;
	}
	Greeter.prototype.greet = function () {
		return "Hello, " + this.greeting;
	};
	return Greeter;
})();

	var greeter;
	greeter = new Greeter("world");
	alert(greeter.greet());



=== 存取器get,set ====================================

class Person {
    protected name: string;
    constructor(name: string) { this.name = name; }	//建構子
}


var passcode = "secret passcode";

class Employee {
	private _fullName: string;

	//get
	get fullName(): string {
		return this._fullName;
	}
  
	//set
	set fullName(newName: string) {
        	if(passcode && passcode == "secret passcode") {
			this._fullName = newName;
		}else{
            		alert("Error: Unauthorized update of employee!");
        	}
    	}
}