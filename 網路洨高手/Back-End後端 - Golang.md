### 特色
* [link](https://yami.io/php-to-golang/#%E5%AE%9A%E7%BE%A9%E8%AE%8A%E6%95%B8variables)

* Goroutine (`go`關鍵字)  
有點像是輕量化的Thread  
一個Go程式可以執行超過數萬個 Goroutine  
* Channel (算是一種佇列,chan關鍵字)  
先執行完的,就會 先存進Channel
* code style很嚴格 沒達成 連編譯都不會過  
有沒使用到的變數(要使用 _ 取代)  
	
* 準備資料
* 結構.方法()
	
### 建置相關 
```bash
go get	//有點像是composer,npm 安裝依賴的東東
go build *.go -o app //把所有*.go 包成 名稱app 的2進制檔 
	
# 背景執行 
nohup ./app > /dev/null 2>&1 & echo $! > run.pid  &
```

### Goroutine (go關鍵字) 
```go
// 有點像是輕量化的Thread(並不是真的同步進行,有點像內文切換)
// 一個Go程式可以執行超過數萬個 Goroutine

//令人簡易使用的併行設計
func main() {
	// 透過 go 關鍵字，我們可以把這個函式異步執行，
	// 如此一來這個函式就不會blocking程式的執行。
	go loop()

	// 執行另一個迴圈。
	loop()
}
```

### Channel (算是一種佇列,chan關鍵字)
```go
//存入
變數 <- "兔子"
//取出
被賦予的變數 := <- 變數 
	
//創立一個Channel
goal := make(chan string)
	
totalStep := 10

go 跑者1(goal)		//同步執行
go 跑者2(goal)		//同步執行

//先執行完的,就會 先存進Channel,先打印
fmt.Printf("%s 抵達終點\n", <- goal)
fmt.Printf("%s 抵達終點\n", <- goal)
	
//---Buffered Channel 可限定容納多少資料---------------
變數 := make(chan int, 2)
	
//---select (協調多個 Channel)-----------------------------------------
select {
	case p1 := <- clerk1:
		fmt.Printf("消費了生產者一的 (%d)\n", p1)
	case p2 := <- clerk2:
		fmt.Printf("消費了生產者二的 (%d)\n", p2)
	case <- time.After(3 * time.Second):	//過了3秒,其他case還是阻斷就觸發
		fmt.Printf("消費者抱怨中…XD")		
}
	
//---單向 Channel (只能發送的 Channel)------------------------------------------------------
//只能發送的 Channel
變數 chan<- int 
//只能接收的 Channel
變數 <-chan int 
	
func consumer(clerk <-chan int) {
	fmt.Println("消費者開始消耗整數......")
	for i := 1; i <= 10; i++ { 
		fmt.Printf("消費了 (%d)\n", <- clerk )
	} 
}
```

### select(有點像給chan專用的Switch)
```go
go func() {
        time.Sleep(time.Second * 1)
        c1 <- "one"
}()
go func() {
        time.Sleep(time.Second * 2)
        c2 <- "two"
}()

for i := 0; i < 2; i++ {
        select {
        case msg1 := <-c1:
            	fmt.Println("received", msg1)
        case msg2 := <-c2:
            	fmt.Println("received", msg2)
        }
    }
```

### 基本 
```go
//打印
fmt.Printf("%s %d 床前明月光,疑是地上霜\n", str1 ,i )
	
//可以把變數宣告外面加上誇號 來提高辨識度
var(
        a bool = false // 記得要不同行，不然會錯
        b int
        c = "hello"
)

//  := 代表 宣告+賦值 (預算符)
// 也可一次 賦多值
c, python, java := true, false, "no!"
```

### function
```go
// 函式
func 函數名(參數1,參數2,參數3 型別)(Rtn型別1,Rtn型別2,Rtn型別3){
	return x,y,z
}
	
// 立即函式IIFE
func 函數名(參數1 型別,參數2 型別,參數3 型別)(Rtn型別1,Rtn型別2,Rtn型別3){
	return x,y,z
}(參數1,參數2,參數3)

// callback
func 函數名([]string friends, callback func(string)) {
	for _, n := range friends {
		callback(n)
	}
}
```

### 只有for迴圈 
```go
//當while使用
for sum > 1 {
	
}
	
//當for使用
for i := 0; i < 10; i++ {
	sum += i
        }
	
//當foreach使用
for key, value := range rows {
	key value
}
```

### Slice切片 (動態陣列)

	//把 4,5 push進 slice1
	slice1 = append(slice1, 4, 5)
	
	//
	slice1 := []int{1,2,3}		//創立內容為1,2,3
    	slice2 := make([]int, 2)	//創立大小為2
    	copy(slice2, slice1)		//把slice1複製到slice2
    	fmt.Println(slice1, slice2)	// [1,2]	(因為容量只有2)

### Map集合 (雜湊陣列)
```go
// map[string] 	
['apple','orange']	
		
// map[string]int -------------------------------------
['stitch':626, 'mimi':627]
	
// map[string]map[string]int --------------------------
[
	data:[
		id:1
	]
]
```

### defer、panic、recover(取代try-catch) 

	//defer 程式結束時執行,清除資源,延遲執行 (有點像"函式"的解構子,)
	//panic 恐慌中斷(有點像是throw,一但你執行了 panic() 你的程式就會宣告而終，程式結束的時候會呼叫 defer)
	//recover 恢復流程(有點像是catch)

	defer 有點像"函式"的解構子
	panic有點像 "拋出"例外
	Go語言提供了recover內置函數，
	前面提到，一旦panic，邏輯就會走到defer那，
	那我們就在defer那等著，調用recover函數將會捕獲到當前的panic（如果有的話），
	被捕獲到的panic就不會向上傳遞了，於是，世界恢復了和平。你可以乾你想幹的事情了
	
	panic();
	
	defer func(){
		變數 := recover;
	}

### struct結構
```go
// 宣吿結構
type Rhythm struct {
	Groove	string		`json:"groove"`
	Bpm	uint8		`json:"bpm"`
	Count	string		`json:"count"`
	Style	[]string	`json:"style"`
}
// 宣告方法
func (r *Rhythm) setBpm (uint8 speed) (uint8) {
	r.Bpm = speed
	return r.Bpm
}

// 賦值
funkGroove := &Rhythm{
	"funk",
	120,
	"4/4"
	["breakbeat", "rudiment"],
}

// 執行方法
returnSpeed := funkGroove.setBpm(115)
```

### interface (啥型別都可以放)


### 指標 
```go
//RedisPool 放入指標 (Pool是個struct)
var RedisPool *redis.Pool

func createRedisPool() {
	RedisPool = &redis.Pool{
		MaxIdle:     10,
		MaxActive:   1500,
		IdleTimeout: 300 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", Config.REDIS_HOST)
		},
	}
}
```
### package (讓別的檔案可以引入)

	package helloworld
	要把他放到 ~/go/src 裡面，
	並且取個資料夾名字，
	而且名字不能隨便亂取喔，以這邊的例子來看，你名字要取 helloworld，
	也就是說你要把這個程式碼放到 ~/go/src/helloworld/ 裡面，然後你在其他地方就可以引入了囉～
	
### 一些套件
```go

mgo	//go的mongo drive

}
```

### 其他
```go
//in old go compiler, it is a must to enable multithread processing
runtime.GOMAXPROCS(runtime.NumCPU())
```
