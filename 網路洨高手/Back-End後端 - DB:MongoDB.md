
	#mongoDB driver
	sudo pecl install mongodb
	
# 概念

	Json存取
	
	mongo沒有schema
	以空間換取時間		//找ObjectId("59cdf17254b4944c1c69d5ea")立馬找到東西
	
	這張表列出了傳統 SQL 與 MongoDB 的概念性對應關係：
	SQL		MongoDB
	database庫	database
	table	表	collection
	row	筆	document
	column		field
	
	
	[				//整個叫collection
		{name:john,age:18},	//document 1
		{name:mimi,age:6},	//document 2
		{name:fufu,age:10},	//document 3
	]
	
	MongoDB Replica Set  #備源機制
	MongoDB 切片  把多個MongoDB當成一個完整的DB,分到多台機器(解決 CPU,RAM,IO 瓶頸，有點像是Partition,但是一台機器是一個切片)
	mongostat #查看狀態
	
### MongoDB Shards 數據分片(定位上有點像partition)

	將數據集分佈到多個節點(機器)上來分散訪問壓力。
	這裡的每個節點也稱作切片，每個分片都是一個獨立的DB。所有的分片組合在一起才是一個完整的DB。

	MongoDB的分片框架中有3個角色：

	1）Query Routers：路由
	2）Config servers：元數據服務器
	3）Shards：數據節點

	工作機制：Query Routers用於接收用戶的request，將route到對應的分片上（shards）執行，然後將結果返回給Client。 
	
	Config servers存儲server集群的元數據，Query Routers通過使用這些元數據將請求定位至特定的shard節點。 
	Shards節點存儲數據，為了提供高可用性和數據一致性，每個shard都可以是一個 副本集(Replica Set)。
	在生產環境中，為了避免單點故障，Query Routers和Config servers往往有多個節點。
	
### MongoDB Replica Sets(Replication == 複製)------------------------------

* 第1種 Three Member Replica Sets 是最常用的架構，由三個 MongoDB 節點組成，每個節點其實都是 Mongod 服務。
* 第2種 由更多的Node組成，備援能力更強，但我沒有研究，無從介紹。
* 第3種 Distributed Replica Sets 主要滿足異地備援的需求，這個聽起來不錯。

今天來介紹 MongoDB 最典型的副本架構，主要由三個節點構成，如下：

正常情況對 PrimaryNode 進行讀寫，資料會自動複製 (Replication) 到另外兩組 SecondaryNode，  
這些 SecondaryNode 彼此透過 Heartbeat (每隔一段時間試探節點是否存活的方式) 檢測狀態，  
正常的情況每 2 秒會送出 Heartbeat，若是超過 10 秒沒有回覆，那就是 GG 了。  
```	
function AutomaticFailover(){
	if(PrimaryNode.status() == 故障){
		會選出一台 SecondaryNode 成為 PrimaryNode 接手工作
	}
}
```	
### ARBITER(仲裁者機制)
* Primary 主節點，一個Replica Set有且僅有一台服務器處於Primary狀態，`只有主節點才對外提供讀寫服務`。
```
if(主節點掛掉){
	$Secondary = replicaSetVoteAndGetSecondary($Secondarys)
	setPrimary($Secondary)
}	
```		
* Secondary 備用節點，複製集允許有多台Secondary，每個備用節點的數據與主節點的數據是完全同步的。 
	Recovering 恢復中，
```
if(Replica Set某台服務器掛掉 || 掉線後數據無法同步)，
	重新恢復服務後從其他成員複製數據，//這時就處於恢復過程，
	數據同步後，該節點又回到備用狀態。
```

* Arbiter 仲裁節點，該類節點可以不用單獨存在，如果配置為仲裁節點，就`主要負責在復本集中監控其他節點狀態`，投票選出主節點。該節點將不會用於存放數據。
```
Arbiter == NULL ? `Vote工作`將由All節點共同進行。
```

* Down 無效節點，當服務器掛掉或掉線時就會處於該狀態。複製集的從節點讀請求，也是在各個Driver層設置slaveOk的值來實現的。
	
### journal日誌(預寫入機制) 
	運行MongoDB如果開啟了journaling日誌功能，  
	MongoDB的先在RAM保存寫操作，並記錄journal日誌到disk，然後才會把數據改變刷入到disk上的數據文件。
	為了保證journal日誌文件的一致性，寫日誌是一個原子操作
	
# 命令
```
# 查看狀態
rs.status()

#選擇 TESST 資料庫
use TESST

#列出 database 所有的 collections，可用以下幾種方式：
show collections
show tables

#查詢整個 MongoDB 中所儲存的資料量大小等系統資訊
db.stats()

#---新增資料-------------------------------------- 

#插入資料
db.collection名.insert(Json格式)

#回傳
WriteResult({ "nInserted" : 1 })

#---查找資料--------------------------------------

#條件查找 cuisine = Italian || address.zipcode = 10075
db.restaurants.find({ 
	$or:[
		{"cuisine": "Italian"}, 
		{"address.zipcode": "10075"} 
	] 
})
		
#條件查找 10 < age < 15 
db.getCollection('ec:purchase').find({ 
	"age": {  
		$gt : 10,
		$lt : 15 
	}
})

#排序  1=代表遞增，-1=代表遞減
db.getCollection('ec:purchase').find().sort(
{"borough": 1, "address.zipcode": 1 }
)

#複雜的
db.getCollection('ec:purchase').find({
	'createdAt' : {
		"$gte" : new Date(2018,2,1),
		"$lte" : new Date(2018,4,10)
	},
	'products' : {$not : {$size: 0}},
	'products.0.id' : {'$eq' : 7} 
})

#---刪除資料--------------------------------------
#---更新資料--------------------------------------
```
