### 概念

* 是一種資料庫種類
* 分散式資料庫
* 不使用SQL來查詢語句
* 效能好(存取TMD的快)
* 可用JSON 進/出資料庫


### 常用的NoSQL資料庫
* Google -> BigTable
* Hadoop -> HBase
* Amazon -> Dynamo、Cassandra、Hypertable
