# PEAR 
PHP 擴充功能的函式庫集合
裡面的功能被包裝成 一個一個的Package

* Pear是PHP的上層擴展
* Pecl是PHP的底層擴展。
* phpize是用來擴展php擴展模塊的，通過phpize可以建立php的外掛模塊
* php-devel 是擴展包
* php-pear 

# 套件&擴展
* `Whoops` 就是laravel的debug畫面
* `Monolog` PHP最流行的日志庫
* `Phar`(PHP Archive)它是PHP的extension，就像是Java的jar一樣可以用來打包程式碼，可讓專案易於散佈並安裝使用
* `Swoole` PHP的異步網絡通信引擎(常跟nodeJs比較)
* `php ob函式`

# 規範
* PSR-0已被棄用
* 現在已改為 PSR-4

# 其他
* [爬蟲](https://github.com/zero85258/MyNOTE/blob/master/%E7%B6%B2%E8%B7%AF%E6%B4%A8%E9%AB%98%E6%89%8B/Back-End%E5%BE%8C%E7%AB%AF%20-%20PHP%20%E7%88%AC%E8%9F%B2.md)
* [異步編程swoole](https://github.com/zero85258/MyNOTE/blob/master/%E7%B6%B2%E8%B7%AF%E6%B4%A8%E9%AB%98%E6%89%8B/Back-End%E5%BE%8C%E7%AB%AF%20-%20PHP%20%E4%BD%B5%E7%99%BC.md)
* [生成器]()

# 在docker 安裝環境
```bash
docker-php-ext-install pdo pdo_mysql mbstring json zip mcrypt soap exif
docker-php-ext-enable mongodb xdebug
```
### 生成器
```php
<?php

// @link http://php.net/manual/en/language.generators.overview.php#112985
function getLines($file) {
    $f = fopen($file, 'r');
    try {
        while ($line = fgets($f)) {
            yield $line;
        }
    } finally {
        fclose($f);
    }
}
?>
```
