### Composer 是什麼
  
	可解決每個檔案要Include一大堆東西(相依性)
	有一個 composer.json 的相依套件描述檔案。
	並在安裝過套件後，會產生一份 composer.lock
	
 #### 運行機制 
```
	function install(){	//composer install 指令執行時
		if(isExist(composer.lock)){	//檔案存在
			根據 lock 檔內描述安裝指定套件版本
			已存在套件可能會 更新版本()
			若有新增套件，安裝新套件
		}else{		//檔案不存在
			改以執行 composer update 來建立
		}
	}
  
  	function 更新版本(){	//當執行 composer update 時
		檢查 composer.json 檔案
		根據 composer.json 內的套件版本規格，安裝最新一版
		安裝後更新資訊到 composer.lock 檔案內
	}
```
### 概念 
* 剛從網路載下來專案都要先composer install 下載依賴
* 否則 沒依賴 程式跑個鬼? (上傳全部依賴又太大包)
	
* laravel 的依賴檔 放 /vendor/ 底下
  
### 命令
```bash
#秀出相關安裝包名
composer search 軟體名	

#秀出 安裝包 詳情
composer show laravel/laravel

#安裝
composer install

#更新
composer update
```

### Composer 使用方法
1. 安裝好composer
2. 建立一個composer.json檔
3. 再來，在terminal輸入`composer install`
4. 執行成功之後，你會看到一個vendor資料夾，內含一個autoload.php。
  載入 require 'vendor/autoload.php';



ㄟ等等，我寫的類別都放在my_library裡面了，other_library都是網路上copy下來的現成類別。
我想要用
Google API的Client類別、
Doctrine資料庫管理抽象層類別、還有
guzzlehttp的發送request類別。
我連去下載這些檔案、然後丟進這個資料夾都懶得做了，我根本不想手動建立other_library這個資料夾。


查詢一下那幾個套件在 [packagist](https://packagist.org/) 的名稱、還有你需要的版本號。
把剛剛的composer.json改成這樣：

### composer.json
```json
{
    "require": {
        "google/apiclient": "1.0.*@beta",
        "guzzlehttp/guzzle": "~4.0",
        "doctrine/dbal": "~2.4"
    },

    "autoload": {
        "classmap": [
            "my_library"
        ]
    }
}
```

然後’composer install’指令除了自動載入你的類別之外、
還會自動下載你需要的類別、然後自動載入它們。
一樣require ‘vendor/autoload.php’就可以了。

composer實在是太棒了。
