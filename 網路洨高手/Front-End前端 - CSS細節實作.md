# 水平置中 

### inline元素 
```css
text-align: center;
```

### block元素 
```css
margin: 0 auto;
```

### RWD 
父元素加上	position: relative;
子元素加上 	transform: translateX(-50%);


# 一些好看的字體 
```css
font-family: Hack, monospace, sans-serif;	//用於code
font-family: "Noto Sans TC", serif;		//用於中文字 現代風格
font-family: "cwTeXHei", sans-serif;		//用於中文字 現代風格
```
