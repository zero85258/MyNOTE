### 基本 
```html
<!--註解文字(不會編譯)-->
< 屬性=值>

<h1>標題(字體會最大)</h1>
<h9>標題(字體會最小)</h9>
<div> </div>		<!--佔據一行,區塊-->
<span>行內標籤</span>	<!--不佔據一行-->
<p>文字段落</p>

<br />		<!--換行-->
&nbsp;		<!--打印空白-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<!--打印5個空白-->
```

### html5結構(常見的層次)
```html
<body><main><article>
		
<header> 文件置頂的描述區域 </header>
<nav> 為導覽區塊，通常用作側邊欄 </nav>
<aside>定義文章區塊以外的內容，通常使用於內容相關的邊欄</aside>
<article>一個文件內可以有很多的<article>，通常在文件內容很多時，可以作為區分內容之用</article>
<div>
<h1>
<p>
		
<section> 為內容區域，裡頭的段落文章再用 </section>
<article>
		
<picture>
<source>
<img>
	
<footer> 則是置底的描述區域 </footer>
```

### 感覺很不常用
```html
	<code>代表此字串是程式碼(不會被 google翻譯() 給 翻譯 )<code>
	<em>斜體字</em>  
	<strong>粗體字</strong>
	<hr />  	<!--加條橫線-->
	<address>地址信息</address>
	<blockquote>引用文本</blockquote>
```

# 列表類
```html
	<ul>	<!--URL List-->
		<li>加入一個列表信息</li>
		<li>加入一個列表信息</li>
   		<li>加入一個列表信息</li>
	</ul>


	<ol>   <!--每條列表信息前會有數字-->
		<li>加入1個列表信息。</li>	//無序列表
		<li>加入2個列表信息。</li>
	</ol>


	<table summary="這是摘要,不會顯示在導覽器">	<!--加入表格-->
		<caption>表格標題</caption>
		<tbody>	<!--當表格非常多時,表格會下載一點顯示一點,但加上<tbody>,這個表格就要等表格內容全部下載完才會顯示-->
			<tr>
				<th>寬1</th>
				<th>寬2</th>
				<th>寬3</th>
			</tr>
			<tr>
				<td>寬1的 直排1</th>
				<td>寬1的 直排2</th>
				<td>寬1的 直排3</th>
				<td>寬1的 直排4</th>
			</tr>
			<tr>
				<td>寬2的 直排1</th>
				<td>寬2的 直排2</th>
				<td>寬2的 直排3</th>
				<td>寬2的 直排4</th>
			</tr>
		</tbody>
	</table>
```

### 超連結,資源 等等
```html
	<a href="目標網址" target="在何處打開鏈接文檔" title="鼠標指到顯示的字>導覽器顯示的字串</a>   

	target = "_blank"		<!--在新導覽器窗口打開-->
	target = "_self"		<!--在當前窗口打開新資源-->

	但#後不加任何"文字"  代表不連到任何bookmark  相當於該連結連到網頁本身  原地踏步通常情況下是用來做javascript效果的
	<a href="#">			bookmark就是"我的最愛"

	//當螢幕尺寸600情況下 把圖片src替換掉
	<source srcset="替換圖片網址" media="(max-width: 600px)">	
	<img src="圖片網址" alt="下載失敗的替換文本" title = "鼠標只到時顯示的字串">
```

### 表單 (與後台交流) 
```html
	<form>             <!--表單,與後台交流用-->
  		<input  type=""  name="給後台用"  value="框框內預設字串"/>    <!--type可以是text,password,reset-->
  		<label>顯示字串在導覽器上</label>
  		<input type="radio" value="提交給server的值" name="為控件命名,給後台使用"/>  <!--radio單勾選,當有checked="checked會預設被選"-->
  		<input type="checkbox" value="提交給server的值" name="為控件命名,給後台使用"/>  <!--checkbox可複選,當有checked="checked會預設被選-->
		<input type="submit"   value="提交">   <!--只有當type值設置為submit時，按鈕才有提交作用-->
  		<input type="reset" value="重置"  />	 <!--只有當type值設置為reset時，按鈕才有重置作用-->
  		<input type="button" value="顯示內容" onclick=呼叫JS函數名() >
  		同一組的單選按鈕，name 取值一定要一致，  	<br />	
  		比如上面例子為同一個名稱“radioLove”，		<br />	
  		這樣同一組的單選按鈕才可以起到單選的作用。	<br />	
	</form>  
	
	<form   method="數據傳送的方法" action="導覽者輸入的數據被傳送到的地方.php">

	<textarea  rows="多行輸入域的行數" cols="多行輸入域的列數"> 
		可輸入多行字串  
	</textarea>

	<select>   <!--下拉單-->
		<option value="提交給server的值或字串1">顯示的選項1</option>  
		<option value="提交給server的值或字串2">顯示的選項2</option>
		<option value="提交給server的值或字串3"  selected="selected">顯示的選項3</option>   <!--有selected="selected"被設為預選-->
		<option value="提交給server的值或字串4">顯示的選項4</option>
	</select>
```

---

```html
<link href="引用的檔名.css" rel="stylesheet" type="text/css" />     <!--類似引用c語言的.h檔-->
```

### iframe 框架(內置框架||內聯框架) (少用,速度慢)

	用來在網頁內嵌入另外一個網頁，
	常見的應用方式，例如
	現在非常流行的嵌入 Facebook 粉絲團、部落格、個人網站、在網站內容加上按讚、分享的按鈕
	、顯示一些非同步且跨站的資訊
	HTML iframe 可以自己設定要嵌入的網頁所佔空間，如寬度與高度，是否要顯示邊框或捲軸
	
