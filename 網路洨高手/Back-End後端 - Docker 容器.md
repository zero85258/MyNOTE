# 概念
* [教學網址](https://www.youtube.com/watch?v=k5iwKUZY9tk#t=49.181333)
* [dry好用的cli](https://hackernoon.com/docker-cli-alternative-dry-5e0b0839b3b8)
* 容器有點像是輕量化的vm
* 要先開 鯨魚(OS之上的一層VM) 才能 跑容器 
* 以後用alpine linux(超輕量化OS) 來寫dockerfile 
* 可用`docker-php-ext-enable`來裝php擴展
* Kubernetes == K8S
* Docker Machine
* Docker Compose
* Docker Swarm

# 命令

	--- 版本相關 pull push commit-----------------------------------------------
	#把檔案pull下來
	docker pull 專案

	#把檔案push上去
	docker push 倉庫名/專案:tag版本號

	#把檔案commit
	docker commit 容器id 提交後的新名字
	docker commit -m "提交訊息" -a "作者名稱" 45dfgdgs4d35f 名稱
	
	
	--- 查看 ------ images > ps -a > ps -----------------------------------------------
	#列出所有本機 映像檔(images)
	docker images
	
	#查看 容器(container) 已啟動的 + 含未啟動
	docker ps -a
	
	#查看 容器(container) 已啟動的
	docker ps
	
	
	#修改名稱 tag (容器原本只有ID)77
	docker tag -it 45dfgdgs4d35f 自己命名
	docker tag 映像id zero85258/gitlab_ci
	
	//查看底下運行東西的log
	docker logs 容器名稱
	
	--- 使用相關 ---- run = create + start -----------------------------
	#把image拿來 建立()|啟動() 容器,綁定在邊準輸入上(終端機)
	docker run -it 鏡像名稱 bash
	
	#直接對 容器 內下指令 (指令:使用/bin/bash)
	docker exec -it 容器名稱 /bin/bash
	
	#創建 容器	(概念上docker run = docker create + docker start)
	docker create -v 本機目錄:容器目錄 --name 自己命名容器 要用哪個image
	
	#從 容器 出來 
	exit
	
	#砍掉這個 容器process
	docker kill 名稱


	--- 終止 啟用---------------------------------
	#終止
	docker stop 45dfgdgs4d35f

	#啟用
	docker start 45dfgdgs4d35f

	--- 移除 ---(-f是強制的意思)--------------------------------------------------
	docker rmi -f 45dfgdgs4d35f	#移除 映像檔(image) or tag	(docker images看到的)
	docker rm -f 45dfgdgs4d35f	#移除 容器(container)	(docker ps -a看到的)
	docker kill -f 45dfgdgs4d35f	#移除 正在運行的容器	(docker ps 看到的)	

	--- 把東西放到容器內 -----------------------------------------
	#把東西放到 容器 內(容器ID可由 ps指令查詢)
	docker cp 檔案.php 容器ID(1FS11D8F):容器內的路徑 

	--- 其他 -------------------------------------
	-d == 背景執行 #daemon 有點類似 進入無窮迴圈(防止關閉)

	--- 服務要開機起來指令 -------------------------------------------
	docker run -dit --restart unless-stopped -p 80:9000 realtime
	
# Dockerfile 

	有點像批次檔的概念,一口氣執行多的docker命令	
	
	--- 如何執行 ----------------------------------------
	先touch Dockerfile 	#建Dockerfile檔
	cd 到存放 Dockerfile 的路徑中
	執行 docker build 指令。
	
	如此一來，不用透過人工操作，
	Docker 會自動執行 Dockerfile 裡面的步驟，幫我建立 Docker Image
	
	#創建映像檔
	docker build -t iMaGe名稱:tAg名稱 .

	--- 語法 ----------------------------------------
	FROM 來源鏡像 	#就是pull下來的image
	
	ADD 	#添加文件
	COPY 本地文件 容器路徑	#拷貝文件
	
	--- RUN , CMD , ENTRYPOINT 差異---------------------
	RUN 		執行命令|創建新的鏡像層，RUN 經常用於安裝軟件包。
	CMD 		設置容器啟動 後默認執行的 命令其參數，但 CMD 能夠被 docker run 後面跟的命令行參數替換。
	ENTRYPOINT 	配置容器啟動時 運行的命令。
	
	
	RUN apt-get install -y nginx 				#執行CMD命令
	CMD ["npm","start","--production"]			#執行CMD命令
	ENTRYPOINT ["/usr/sbin/nginx","-g","daemon off;"] 	#在前台執行,而不是daemon執行
		
	----------------------------------------------------
	EXPOSE 80 443 	#暴露端口(假如東西是伺服器)
	VOLUME mount point

	WORKDIR 指定路徑
	MAINTAINER維護者
	ENV	設定環境變量
	
	USER 指定用戶id
	

	--- 實際範例 --------------------------------------------
	FROM ubuntu
	MAINTAINER 維護者

# Volume 
	
	#掛載(-v)
	docker run -d --name 自己命名 -v 容器內部地址 
	
	#檢查 容器狀態
	docker inspect 容器名
	
	#   ($PWD是指當前目錄 , -d是指daemon)
	docker run -p 本機端口:容器端口 -d -v 本機目錄:容器目錄 nginx
	docker run -p 80:80 -d -v $PWD/html:/usr/share/nginx/html nginx
	
	#從 B容器 加載東西 到 A容器(--volumes-from) 
	docker run --volumes-from B容器 A容器 /bin/bash
	
	
	---
	
# 多容器 docker-compose 
	
	-d  #背景執行

	docker-compose up -d mysql redis nginx	# 啟動相關容器
	docker-compose ps # 查看容器進程
	docker-compose exec 容器名字 sh		# 進入相關容器
	docker-compose up -d mysql redis nginx＃
	docker-compose up -d mysql redis nginx
	
	//出問題的話,先暫停了它
	docker-compose stop
	
	//設置完docker-compose.yml檔 不需重新build 只要下
	docker-compose up -d --force-recreate

# laradock 
```
	//要把laradock放在 專案 裡
	.env	#是設定所有相關的檔案

	//要執行測試指令之類請進到 容器workspace
	docker exec -it laradock_workspace_1 bash
```

# 特別個案 
```bash
#CentOS 建造容器時要 下特別參數
docker run -d -e "container=docker" --privileged=true -v /sys/fs/cgroup:/sys/fs/cgroup 5a4b4c4cbcc2 /usr/sbin/init
```

# docker-compose.yml 常見

```yaml
version: "3"

services:
    nginx:
        image: nginx
        volumes:
            - "./available-sites/:/etc/nginx/conf.d/:ro"
            - "./ats:/code/ats"
            - "./tf:/code/tf"
            - "./atm.testing.local:/code/atm.testing.local"
        ports:
            - "80:80"
    mongo:
        image: "mongo:3.4"
    redis:
        image: redis
    atm:
        build:
            context: ./atm/
        volumes:
            - "./atm:/app/"
        depends_on:
            - mongo
            - redis
            - nginx
    ats_db:
        image: mysql:5.6
        environment:
            - MYSQL_ALLOW_EMPTY_PASSWORD=true
            - MYSQL_DATABASE=ats
        ports:
            - "3306:3306"
    ats:
        build:
            context: ./ats/
        volumes:
            - "./ats:/code/ats"
        depends_on:
            - ats_db
            - nginx
    tf_db:
        image: mysql:5.6
        environment:
            - MYSQL_ALLOW_EMPTY_PASSWORD=true
            - MYSQL_DATABASE=tf
    tf:
        build:
            context: ./tf/
        volumes:
            - "./tf:/code/tf"
        depends_on:
            - tf_db
            - nginx
    realtime:
        build:
            context: ./realtime/
        depends_on:
            - mongo
            - redis
```
