### 同步
```js
# 時間線 ->
# buyBuger -> eatBurger -> throwTheJunk

var buger = buyBuger(),
	junk = eatBurger(buger);
throwTheJunk(junk);

```

### 非同步
```js
# 時間線 ->
# buyBuger -> eatBurger -> throwTheJunk
# playMobile

buyBuger(function(buger) {
	eatBurger(buger, throwTheJunk);
});
playMobile();
```
