類{
	屬性;
	方法();
}

介面{
	常數 (constant) 
	方法 (method)
}

=== 

	this是指向 物件實例 的一個指標，在 產生實體 的時候來確定指向；  //偽變量 表示物件自身
	self是對 類本身的一個引用，一般用來指向類中的靜態變數；
	parent是對父類的引用，一般使用parent來調用父類的構造函數。

	public  所有東西都可訪問
	protected 可 自身 訪問,或 子類 訪問
	private 只有 自己能訪問


	建構子ok
	解構子ok
	物件的引用? 用於訪問 物件的 屬性 和方法
	$obj1 = $obj2; 表示將 物件obj2 賦值給 物件obj1 ,會有兩個物件，這兩個物件指向同一個地址。
	$obj1 =& $obj2; 表示obj1和obj2是同一個物件。  obj2算是別名??
	$obj1 = clone($obj2)，是有兩個物件，這兩個對象完全獨立，不同的地址值;



<?php

class human{
	//定義屬性
  
	//方法
	public function eat($food){
		echo $this->name."s eating ".$food."\n";
	}
}

class nbaplayer extends human{  //extends human == 繼承human這個類

	//定義屬性
	public $name="jodan";
	public $h="198";	
	public $w="98";	
	public $team="bull";	
	public $num="23";	  
  
	function __construct($name,$h,$w,$team,$num){  //建構子,物件被實例化時發動(預設設定)
		$this->name = $name;  //$this在產生實體的時候來
		$this->h = $h;	
		$this->w = $w;	
		$this->team = $team;	
 		$this->num = $num;
  	}

	function __destruct(){  //解構子 程式執行結束時,發動 (清理程序使用的資源,節省資源)
		echo "destroying".$this->name."\n";
	}

	//方法
	public function run(){
 	}

	public function jump(){
	}

	public function shoot(){
	}
	
} //class end

 	$jodan = new nbaplayer();
	echo $jodan->name."\n";
	$jodan->jump();
	$jodan->shoot();

	$物件 = null;  //給物件null,物件視同被解構

	$jodan->eat("shit");  //直接使用 父類成員eat 吃shit

?>

=== 鍊式操作(如何製作) ===========================================

	//法1
	//方法() 最後 return $this;
	public function 法1($name)
	{
		$this->var['name'] = $name;
		return $this;
	}
	
	//法2 (靜態方法)
	public static function setName($name)
	{
		self::$var['name'] = $name;
		return new self;	//一定要new self
	}
	
	public static function getVar()
	{
		return self::$var;
	}
	

------------------------------------------------------------------------
重寫(overwrite)  子類 可以 修改,調整 父類 繼承到身上的 類成員 (人 都會 吃() 但是夫夫不要 吃(),夫夫要 吃的爽() )
訪問控制
數據訪問


Final關鍵字	//防止被 方法()被 覆寫 


若你真的需要繼承某class，但又希望別人無法new它，就將它宣告成abstract；
若你沒有需要繼承某class，只希望N個class都要求有某些功能,就先宣告一個interface，讓各class去實作它！

==物件接口  interface,implements (制定軟體組件的共同規則 || 解決 多重繼承問題,多個父)==========================================

	//通常讓 多種類似的方法 可以用統一的方式呼叫
	//例如mysql_connect()跟mysqli_connect()
	//都直接呼叫 一個自訂的connect()
	
interface 接口名稱{ 
    function 被取用的_某個_父類中_的_函式();
}

class 類名 implements 接口名稱{ 

    public function 被取用的_某個_父類中_的_函式(){
        return 0;
    }
}


==多型 polymorphism===========================================================
//範例1
Animal[] animals = new Animal[5];
animals[0] = new Dog();	//可以放 任何 Animal的 子類別
animals[1] = new Cat();	//可以放 任何 Animal的 子類別	
animals[2] = new Wolf();	//可以放 任何 Animal的 子類別
animals[3] = new Hippo();	//可以放 任何 Animal的 子類別
animals[4] = new Lion();	//可以放 任何 Animal的 子類別

for (int i=0 ; i<animals.length ; i++){
	animals[i].eat();
        animals[i].sleep();
}
//範例2
class Vet{ // Vet==獸醫
	public void giveshot(Animal a){
	  a.makeNoise();
	}
}

class PetOwner{
	public void start(){
		Vet v = new Vet();
		Dog 水君 = new Dog();
		Cat 炎帝 = new Cat();

		v.giveshot(水君);
		v.giveshot(炎帝);
	}
}

-------------------------------------
抽像類

--把物件傳入函式----------------------------------------------------------------------------------------------
function(objname){
	objname.parameter1 = ;
	objname.parameter2 = ;
	objname.parameter3 = ;
}




=== 多型polymorphism,多載overload,重載override==(一直搞錯) ================
用 參數 控制函式行為的，就叫 "多載overload"。{
	bool Add( int a, int b );
	bool Add( float a, float b );
}


用 類別 控制函式行為的，就叫 "多型polymorphism"。{
	相同的訊息可能會送給多個不同的類別之物件，而系統可依據物件所屬類別，引發對應類別的方法，而有不同的行為
	喇叭.PLAY()
	吉他.PLAY()
	//PLAY()出來的聲音不一樣
}

覆寫,重載override{
	寫一個 父類 相同名稱的函數
	可覆蓋extend過來的函數用法
	//當 父類->方法() 被宣告 Final關鍵字 就不會被 override
}

=== trait 性狀 ============================

	--- 本體 -------------------
	trait 性狀名稱
	{
		#方法A{
		}
	
		#方法B{
		}
	}
	
	--- 調用 --------------------
	use 性狀名稱
