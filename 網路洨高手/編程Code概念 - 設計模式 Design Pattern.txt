=== 分門別類 ==============================================

	//常用{
		//創建型模式{
		在軟體工程中，是處理物件創建機制的設計模式。
		此類型模式試圖根據適合的情況來決定建立物件。
		單純的物件創建常會導致一些設計問題或增加設計的複雜度。
		創建型模式則藉由控制物件的生成方式來解決這問題。
		
			//單例模式Singleton,
			//工廠模式,
			//建造者模式,
			//原型模式,
		},
		//結構型模式{	藉由一以貫之的方式來了解元件間的關係，以簡化設計。
			//適配器模式Adapter,
			//裝飾模式Decorator,
			//組合模式,
			//代理模式Proxy,
		},
		//行為型模式{	用來識別對象之間的常用交流模式並加以實現
			//命令模式,
			//迭代器模式Iterator,
			//觀察者模式,
			//中介者模式Mediator,
			//模板方法模式
		}
	}


===============================================================================================
工廠模式{
	用字串指定型別定建立物件的一種方法
	//靜態工廠{
		就是把類成員宣告成static,不再需要大量實例話,僅需呼叫
	}
}

單例模式Singleton{
	不讓有些物件重複建立,而浪費資源
	EX:connect_SQL()
}

註冊樹模式{		//註冊模式
	//將所有 物件 放到 註冊樹 ,可配任何地方訪問
	//把 物件 set 映射到 註冊樹 上
	//把 物件 unset   從 註冊樹 上解除
}

適配器Adapter / Wrapper{
	EX:將MYSQL,MYSQLi,PDO封裝成統一的接口
}

策略模式{
	將一組特定的行為和算法封裝成類,以適應某些特定的上下文環境
}

觀察者模式{	//像是redux就採用了觀察者模式
	觀察者模式定義了物件之間的一對多關係，
	如此一來，
	if(一個物件改變狀態){
		其他相依者都會收到通知;
		自動被更新;
	}
}

裝飾模式Decorator{	//前端常用
	在執行期間動態調整物件，添增新功能
}

代理模式Proxy{
	包裝成一個物件以控制存取權，藉由將操作組合在一起，或是真正有需要時才執行，降低資源浪費
}

中介者模式Mediator{
	促進降低耦合性，方法是讓你的物件不和其他物件直接對話，
	而統一透過一個 mediator 物件溝通
}
