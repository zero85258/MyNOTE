### 概念 	
##### 能有哪些方案
* Laravel --控制-> phpunit-selenium --控制-> Selenium Server --控制-> ChromeDriver --控制-> Chrome
* Laravel --控制-> phpunit-selenium --控制-> Selenium Server --控制-> Firefox

```
Laravel 並沒辦法直接控制 Chrome , 	必須藉由 Selenium Server
Selenium 目前沒直接支援 PHP，		必須透過 phpunit-selenium 套件去控制 Selenium Server。
Selenium 預設只支援 Firefox，		必須裝 ChromeDriver 才能讓 Selenium 控制 Chrome。
Selenium 是用 Java 寫成的，		一定要在你的OS裝 Java，
```

### 測試 
```
	前端BDD框架等?你去使用，像Jasmine/Mocha/QUni

	TDD測試驅動開發 #是種 "開發方式"

	Unit 單元測試{	//TDD流程的一部份	#單元測試優點 http://blog.turn.tw/?p=2821
		程式是否有預期的結果
	}

	Selenium{	//導覽器自動化	E2E(End to End)測試的一種
		模擬使用者行為
	}	
```

### selenium
```

	selenium{
		開啟方法{
			>>>cd F:\xampp\htdocs\ifchic-3-0\phpunit\tests/SeleniumTest
			>>>java -jar selenium-server-standalone-2.53.1.jar
			>>>java -jar selenium-server-standalone-3.5.0.jar
		}
		執行方法{
			cd到ifchic-3-0/phpunit/tests/SeleniumTest
			>>>phpunit 要測試的.php		#輸入完後執行之後看程式表演

			cd到ifchic-3-0/phpunit/tests
			>>>phpunit SeleniumTest/要測試的.php	#輸入完後執行之後看程式表演
		}
		測試檔{
			public function setUp() //有點像是每次執行的"建構子"
			public function tearDown() //有點像是每次執行的"解構子"
			public function test測試名稱() //測試腳本寫這
		}
	}
```

### 常用腳本指令
```php
	$this->webDriver->get($this->url);	//打開導覽器
        $this->closePopup();	//關閉ifchic首頁的廣告

	$ff = $this->webDriver->findElement(WebDriverBy::xpath('//*[@id="menu"]/li[6]/a'));
	$ff->click();

	$ff = $this->webDriver->findElement(WebDriverBy::xpath('xPaTh路徑'))->click();
```
### 注意事項

	ChromeDriver 2.3版的
	+底線的不用動
	ifchic-3-0\config\settings.inc.php   //db選項
