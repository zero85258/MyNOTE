# 爬蟲庫crawler
composer 下的第三方庫

* Guzzle ：
功能很完善的 httpclient ，帶異步並發功能，別的腳本語言找不到這麼好的 httpclient
* Goutte ：
對 symfony 的 dom-crawler 和 css-selector 的簡單封裝，
你也可以直接用 symfony 的 css-selector 來抽取 html 的 dom 元素
* symfony/process ： symfony 出品的 php 開進程的庫（封裝的 proc_open ），
兼容 windows ，要知道 pcntl 擴展不支持 windows 的


[原文網址](https://kknews.cc/zh-tw/other/38bx5yy.html)


# 基本PHP cURL
```php
// 建立CURL連線
$ch = curl_init();

// 設定擷取的URL網址
curl_setopt($ch, CURLOPT_URL, "http://tw.yahoo.com/");
curl_setopt($ch, CURLOPT_HEADER, false);

// 執行
curl_exec($ch);

// 關閉CURL連線
curl_close($ch)
```
