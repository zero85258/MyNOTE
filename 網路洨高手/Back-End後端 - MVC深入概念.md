### MVC肥大 
	
	現在大部分的framework都預設有放library以及helper的地方。
	不使用framework的話你也可以自己開這兩個資料夾。

	多個model常做的`同幾件事`可以包起來寫成library，
	而單純的`資料格式轉換`（甚至是產生相關html/css）之類的任務可以寫成helper。

	讓model主要專注在`商業邏輯`（這是最花功夫的地方），
	其餘的搬到library、helper、或是有些framework稱之為third_party或package的什麼地方都好。
	別丟進controller！更別丟進view。	

	Q7: 我知道library跟helper的地位何在了。但，library跟helper的差別又何在？
	軟體開發時，
	有時會寫幾個function或method解決某些任務，有時候覺得這樣太鬆散，會包成class。
	library常常是class、helper常常是一組function。
	以我自己來說，
	常在Controller或View使用helper，
	常在Model使用library。

	

	Q8: 關於model，最後有沒有要提醒的？
	不要在model裡面用session variable。
	用下去，那model根本不再reusable(重複使用)（其他controller用之前還要先設定好相關session）。連unit testing都毀了。
	解決之道是dependency injection。
	session variable某種程度來說根本就是global variable，濫用global variable是很不好的。
	註1：這段話可能很有問題。我在startup工作，開發上極度注重「速度」，常為了拼上線時間而捨棄其他事情。
	
	
### Model減重5個方法 
* [道場](https://www.youtube.com/watch?v=e0qVLniXbHw)
```
	Presenter{	//Presenter主持人
		//日期,金額,名稱之類的呈現抽出
	}
	Repository{
		//查詢query的邏輯,取得entity的抽出
	}
	form{
		//參數驗證validation的邏輯,
		//例如字串長度,日期,金額大小等等都抽出
		//laravel 5建議封裝進Request類
	}
	service{
		//把施加在多種entity上 || 複雜的商業行為 抽離出來
	}
	package{
		//概念獨立於目前專案
	}
```
	
	
