PyCON TW 是一個Python社群
點選Django Environment
並輸入python開始打
輸入exit()離開編輯

    #這是單行註解
    """這也是多行註解"""
    
    print "印字串" 
    print x,y	#印x,y值

    str = input("请输入：");
    print "你输入的内容是: ", str

=== 陣列list (可更動大小) ========================

    A = []	#宣告陣列A
    A.append(5)	#增加陣列長度 
    len(A)		#可以算list A長度
    sum(A)		#可以計算list A中所有數值的加總(但list中的元素都需為數值，不可與字串混合)
    A.count(shit)	#則是可以計算list A中shit元素出現次數

=== 字串處理 ===========================================

=== 判斷式、迴圈 =============================================

    每個流程的結尾是用冒號:
    屬於該流程底下的執行動作不需要任何括號，而是使用"縮排"
    縮排可以使用Tab或四格Space，"但不可混用"，
    建議是把編輯器設定成Tab對應四格空白
    A=[]
    for i in range(0,10):  		"""//for(i=0;i<10;i++)"""
        A.append(i+1)

    if A[0]==1 and len(A)<10:
        A[0]+=1
        print "1 state"
    elif (10 in A) or not(len(A)==10):
        print "2 state"
        print "range(i,j) is i~j-1"
    else:
        print "3 state"  
        print "none of above"


    #類似foreach用法
    for i in A:      """//for(i=0;i<A.length();i++)"""
        print i,          """//cout<<A[i]"""
        print


=== 函式 ========================================

def 函式名(x,y):
    return x-10,y+10	#可回傳兩個數

    x,y = 函式名(10,20)
    print x,y


=== [類別Class] =============================================
Class的  初始化函式  是由兩條底線包含著init做宣告。

 # encoding: utf-8

class Student: 
    #__init__是建構子
    def __init__(self, name, grade, age): 
        self.name = name  		#self 相當c++的this
        self.grade = grade  
        self.age = age  
    def set_name(self, name):  
        self.name = name  

student_objects=[]
student_objects.append( Student('john', 'B', 15) )
student_objects.append( Student('dave', 'A', 12) )
student_objects.append( Student('jane', 'A', 10) )
student_objects[0].set_name('John')

for i in student_objects:
    print i.name,i.grade,i.age 

=== 方法,靜態方法 ==================================

    @classmethod
    def aa():
		      print('1') 
        
    @staticmethod
	   def bb():
		      print('2') 

=== 導入外部資源import ================
可以用import直接導入整個python檔中所有的函式
或是用from檔案import函式，插入特定的函式

 # encoding: utf-8

import sys """插入sys檔案中所有函式，使用sys檔中的write函式前須加檔名"""
from time import time """從time檔案插入time()函式，使用time()前不需要加檔名"""

sys.stdout.write( str(time()) + "\n" )
Output:
1409796132.99 #當下的time


=== 寫檔/讀檔 ====================================================

    fo = open("foo.txt","wb")
    fo.write( "www.runoob.com!\nVery good site!\n");
    str = fo.read(10);      #讀10個字節
    str = fo.read();        #讀到檔案結尾
    
    print "讀取的字符串是:",str
    fo.close()
    
    #回傳 文件當前位置,(就是pwd)
    fo.tell();

=== 文件 ====================================================
import os
    
    #改變 當前 目錄位置,(就是cd)
    os.chdir("/home/newdir")
 
    # 建立資料夾
    os.mkdir("test")
    
    # 刪除資料夾
    os.rmdir( "/tmp/test"  )
 
    # 重命名文件test1.txt到test2.txt。
    os.rename( "test1.txt", "test2.txt" )
    
    # 刪除文件
    os.remove(file_name)
    
    
=== sqlite ==============================================================
import sqlite3
    
    #連線
    conn = sqlite3.connect('test.db')

    #執行語句
    conn.execute(SQL語句)

    #斷線
    conn.close()

=== GUI ====================================================================
