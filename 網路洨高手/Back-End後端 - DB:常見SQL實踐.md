### 基數 偶數 
```sql
WHERE mod(欄位,2) = 0;	#撈出偶數
WHERE mod(欄位,2) = 1;	#撈出奇數
```
### 每個部門薪水最多的人,Rank值相同還會,名次一樣 
```sql
SELECT a.rank AS 'Rank'
     	, a.name
     	, NumAV
FROM (SELECT name
	, NumAV
	, @prev := @curr
	, @curr := NumAV
	, @rank := IF(@prev = @curr, @rank, @rank + 1) AS rank
	FROM ithelp1010a
	, (SELECT @curr := null
		, @prev := null
		, @rank := 0) s
	ORDER BY NumAV DESC) a
HAVING Rank <= 3; 
```
### 轉置
[轉置](https://stackoverflow.com/questions/1241178/mysql-rows-to-columns)


### group by 統計
```sql
SELECT *, COUNT(distinct sex) 
FROM table 
GROUP BY sex
``` 
