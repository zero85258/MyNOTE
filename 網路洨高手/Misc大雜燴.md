### 概念
* 通常命名Misc都是些沒歸檔,不知道如何分類的大雜燴
	
---

### 好用套件

#### the_silver_searcher
[gitLink](https://github.com/ggreer/the_silver_searcher)
* 安裝指令`brew install the_silver_searcher`
```bash
ag -Q -- '要查的'
```

# 開工前`強烈建議`先去裝 (有好用的東西建議補上大家一起分享)
* [phpstorm]() (php開發用,會讓你上癮的IDE)
* [iTerm2](https://www.iterm2.com/) (比較好用的終端機,請棄用mac內建的)
* [fish](https://hackercodex.com/guide/install-fish-shell-mac-ubuntu/) or [zsh]() (比bash好用,適合像我一樣打字很慢的笨蛋) (`brew install fish`)
* [On my fish](https://suitejay.io/2017/05/10/%E4%BD%BF%E7%94%A8-fish-%E6%89%93%E9%80%A0%E8%B6%85%E6%A3%92%E7%94%A8%E6%88%B6%E9%AB%94%E9%A9%97%E7%B5%82%E7%AB%AF%E6%A9%9F%EF%BC%9A%E5%AE%89%E8%A3%9D%E7%AF%87/) (fish的套件管理工具)
* [insomnia](https://insomnia.rest/) (打api用的,`很潮`)
* [htop]() 有顏色的top指令(看cpu,ram之用的) (`brew install htop`)
* [tig]() 好用git cmd-line  (`brew install tig`)
* [XMind](https://actsmind.com/blog/xmind/xmind3download) 心智圖軟體,有些軟體試用這個
* [Cyber Duck](https://cyberduck.io/) 沒有那麼肥的FTP GUI
* [NosqlBooster](https://nosqlbooster.com/downloads) 請棄用Robo3T(MongoGUI) 
* [dry](https://hackernoon.com/docker-cli-alternative-dry-5e0b0839b3b8)(docker的好用軟體)
* [pydf]()看disk使用情況`sudo apt install pydf`
* [jira CLI](https://github.com/keepcosmos/terjira?fbclid=IwAR3AlTiairrqSbYe3sPISN3FPsKTGcg0to1RcIUz0kZS2eH19_947fHTLiw)
* [ncdu]()(du df)
* [mysql CLI Tool](https://www.mycli.net/)
* [CLI ui](https://github.com/Shopify/cli-ui)
* [BAT](https://github.com/sharkdp/bat)(A cat(1) clone with syntax highlighting and Git integration.)
* [dive](https://github.com/wagoodman/dive) (docker分析工具？？)

* [可來這邊挖挖](https://itnext.io/10-cli-tools-that-you-will-love-d214bc73d856)

### 一些常用

```bash
# ssh
ssh-keygen

# 安裝composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# 安裝brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# 安裝awscli
apt-get install python-setuptools python-dev build-essential -y
easy_install pip 
pip install awscli
aws configure
# 填入金鑰相關
```


### 不知如何整理
* CURD 增刪改查


* 從 grunt -> gulp -> webpack
* gulp好像已經幹掉grunt
