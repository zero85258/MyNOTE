# swoole
[Swoole官網](https://www.swoole.com/)
[php-fpm VS Swoole](https://laravel-china.org/articles/9450/php-fpm-vs-swoole)

### CPU多核的利用
* node.js沒有內置對 多thread/多process 的支持，用戶必須使用cluster/child_process等擴展自行實現並行
* swoole 內置對 多thread/多process 的支持，用戶僅需配置參數即可

對於熟悉並行編程的程序員使用node.js cluster/child_process可以解決問題。  
但畢竟不是官方提供的，難免會產生BUG，需要開發者自己負責  
對於不熟悉並行編程的程序員，並行會變得困難。很多技術人員採用了啟動多個程序實例來解決此問題。  

### 同步阻塞的支持
* swoole同時支持 同步/異步 2種模式
* node.js僅支持異步
為什麼強調 同步阻塞模式 的支持。

---
* `多process` + `sync` + `blocking` 模式 是Unix世界40多年曆史中最成熟的一種編程模式。
配套的調試工具非常豐富完善，穩定性、成熟度、調度公平性、開發調試效率都是最佳的 適合 `業務邏輯` 很重的程序。
* `多thread`、`異步callback`、`協程` 等模式編程雖然可以帶來一定的`性能提升`，but`複雜度過高`，開發`調試困難`。


協程本質上也是一種異步IO，無法利用現有的工具如strace，gdb進行調試
swoole中對於復雜業務邏輯，推薦使用同步阻塞

### 自動協議的支持
node.js沒有內置通用協議處理的支持，需要自行實現代碼
swoole內置了通用協議處理的支持，可以藉助swoole提供的功能輕鬆實現

### TCP心跳檢測
swoole內置了對TCP心跳檢測的支持可
