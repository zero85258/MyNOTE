### Laravel的種種

	Laravel.Artisan		//Laravel內建的cli命令列
	Laravel.Migration 	//資料庫遷移,像 資料庫的git
	Laravel.Composer	//為了解決套件相依性include,require(以autoload取代之)
				https://www.ptt.cc/bbs/PHP/M.1416476586.A.C3E.html
	Laravel.Eloquent(ORM)	//將 SQL 加以物件導向化，讓我們以習慣的方法與屬性存取資料庫
				//講白話就是,讓SQL語句看起來比較像程式碼
	Laravel.Blade		//樣版,類似Smarty
	Laravel.TDD		//測試驅動開發
	Laravel.Authentication	//框架內建一組會員驗證機制，從登入、登出、註冊到會員資料表一整套解決方案。
	Laravel.homestead	//官方提供的虛擬機,可以省去建置的麻煩
	Laravel.Seeding		//用來提供產生大量測試資料,否則以前要自己網資料庫倒入大量假資料很不方便
	Laravel.Wagon		//一個可攜式的開發環境(免安裝),裡面包含Artisan等等一堆東西
	
### 學習網站

* [道場](http://www.laravel-dojo.com/workshops/201507-ntpc)
* [幕課 Laravel 基本篇](http://www.imooc.com/learn/697)
* [幕課 Laravel 表單篇](http://www.imooc.com/learn/699)
* [幕課 Laravel 高級篇](http://www.imooc.com/learn/702)
	
### 資料查詢法
* Raw SQL Queries	原始查詢(DB Facade)
* query builder 查詢構建器
* Eloquent ORM.

