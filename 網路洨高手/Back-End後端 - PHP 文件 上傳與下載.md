### 概念
`client端`的文件`上傳`到`server端` ,將`server端` 的`臨時文件`移動到指定目錄即可

### 客戶端config(`client端`)
```html
<form action="發送目的.php" method="post" enctype= "multipart/form-data">
  <input type="file" name="MYYFILE" />  <br/>
  <input type="submit" value="按我上傳" />
<form>
```

### 接收端config(`server端`)

```php
<?php
  print_r($_FILES);  //print_r 用來打印陣列 在導覽器上
  
  $filename = $_FILES["MYYFILE"]["name"];	//上傳檔案的原始名稱。
  $type = $_FILES["MYYFILE"]["type"];		//上傳的檔案類型。
  $size = $_FILES["MYYFILE"]["size"];		//上傳的檔案原始大小。
  $tmp_name = $_FILES["MYYFILE"]["tmp_name"];	//上傳檔案後的暫存資料夾位置。
  $error = $_FILES["MYYFILE"]["error"];		//如果檔案上傳有錯誤，可以顯示錯誤代碼。
  
  //將 伺服器上 的 臨時文件 移動到 指定目錄 下
  //move_uploaded_file($_FILES["file"]["tmp_name"],目標位置);
  move_uploaded_file($tmp_name,"uploads/".filename);   //移到uploads這個資料夾
?>
```

### 服務器端config

