# CLI邪道
* 安裝`brew install tig`
```bash
alias glas='git log --all --date-order --decorate --graph --oneline --simplify-by-decoration'
alias gg='git log --graph --full-history --decorate --all --color --date=short --pretty=format:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%ad %s | %x1b[36m%an%Creset"'
alias ggs='git log --simplify-by-decoration --graph --full-history --decorate --all --color --date=short --pretty=format:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%ad %s | %x1b[36m%an%Creset"'
```

* 東西less化 `--simplify-by-decoration`

### 待釐清 
```
git remote  #遠端操作相關

里程碑 (ex:現在版本為v19.0.1) (api變動,新增功能,hotfix)
$ git checkout <branch>		#在最新的Git版本中，chekout 命令的參數下指定遠端數據庫的分支，就可以從遠端數據庫複製分支到本地端數據庫建立分支

$ git push <repository> <refspec>
#加上 -u ，可以將遠端數據庫的分支設為追蹤目標。這樣，在 push 或 fetch/pull 命令時即使省略 repository，也可以正確的顯示/讀取修改內容。
#在 <repository>，除了 remote add 命令所添加的數據庫名稱以外，也可以直接指定 URL，省略 <repository> 也可以成為遠端數據庫指定的追?對象。
#在 <refspec> 可以指定分支名稱。省略 refspec 的話，遠端數據庫和本地端數據庫所存有的分支在預設裡會被列為目標。
```

### 檢出項目到伺服器本地 
```
cd test/	#進入到test資料夾
git clone 專案URL 	#把專案載到到當前目錄

git pull  { (等於以下指令)      #把目前專案更新成 雲端上的
  git fetch + 
  git merge origin/分支名
}
請改用git pull --rebase

git fetch     #下載最新版
git fetch -p  #下載最新版 + 把遠端沒有的branch 本地刪掉
```

### 查看工作區狀態
```
git status	  #查看工作區狀態
git add 日記	 #建立跟蹤
#這時會問你
#要不要使用 git reset HEAD <file> 撤銷

git commit	- m  #提交這次變更,提交後會要填寫變更評論
git push	#提交到git上 
```

### 回到過去,帶著過去的代碼回到未來
```
git log		#看過去有哪些提交,大概就是可以看有誰寫了什麼code
git reset --hard <commit編碼>		#--hard當前環境版本的"指標" (就像時光機的時間點,可供改變)
git reset --hard HEAD   #回到該branch頂端

git reflog	#觀看開發者全部 git 的操作記錄，裡面詳細記載你曾經下過的 git 指令
git log -p

git checkout <commitId> <file>  #指定的檔案回到指定的commit
```


### 分枝 branch 
```
git branch 分枝名  #創立一個分枝 
git checkout 分枝名  #切換到該分支

git checkout -b 分枝名 #branch + checkout

git branch -D 本地branch名   #刪除本地分支
git push -d origin          #刪除遠端分支
git rebase --onto adbert-release 現在分支剪斷點 #剪下分支接到某個地方
```

### 合併 merge vs rebase 
```
git merge   #只合併 頂點
git merge --no-ff #比較好
git rebase  #合併 每個節點

  if(發生衝突){
    #去發生衝突的檔案裡把檔案修成自己想要的
    git add 修正的檔案1 修正的檔案2 ....
    git commit    #不用打任何東西, :q! 離開
  }
```

### rebase -i (互動模式) 
```
git rebase -i #批量整理branch
s 會合併他上一個節點
e 僅編輯commit內文
```

### 暫存 
```
git stash       #把目前更動 放入 暫存堆疊
git stash list  #查看 暫存堆疊
git stash apply stash{0}  #把stash{0} 取出
git stash pop
```

### submodule 
```
git submodule update --recursive  #更新所有專案底下的submodule
```

### tag v2.1.0 (api變動,新增功能,hotfix) 
```
git tag v2.2.7 eaefb763a7d84389ee14910dcb50639b15e9e1e9 #把某個點上tag
git push origin v2.2.7
```
