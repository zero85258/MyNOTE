' Requirements:
'   * Windows 7 or later. (Might work on Vista and XP.)
'   * Python 3 installation. (Will prompt for installation path.)
'     The installation path should be both readable and *writable*.
'   * msysGit installation.

Function GetPath(ByVal prompt)
    ' Collect path.
    path = InputBox(prompt)

    ' Trim trailing backslash.
    If StrComp(Right(path, 1), "\") = 0 Then
        path = Left(path, Len(path) - 1)
    End If

    If StrComp(path, "") = 0 Then
        WScript.Quit 1
    End If
    GetPath = path
End Function

Set fs = CreateObject("Scripting.FileSystemObject")
Set shell = CreateObject("WScript.Shell")

home = shell.ExpandEnvironmentStrings("%HOMEPATH%")

defaultName = "Django Environment"
'name = InputBox("Enter link name: [" & defaultName & "] ")
'If Len(Trim(name)) = 0 Then
'    name = defaultName
'End If
name = defaultName

' Collect Git path.
gitPath = GetPath("Enter Git path: ")

' Collect Python path.
pythonPath = GetPath("Enter Python path: ")

' Create "python3" "alias".
batPath = pythonPath & "\python3.bat"
Set bat = fs.CreateTextFile(batPath)
bat.Write pythonPath & "\python.exe %*"
bat.Close

' Create bat file.
batPath = pythonPath & "\env.bat"
Set bat = fs.CreateTextFile(batPath)
pathString = Join(Array(pythonPath, pythonPath & "\Scripts", gitPath & "\cmd"), ";")
bat.Write "set PATH=" & pathString & ";%PATH%" & vbCrLf & "cd /D " & home
bat.Close

' Create shortcut to bat file.
' Desktop name on Windows XP (maybe Vista?) is localized, and installation will
' fail as-is. Need to patch this manually.
linkPath = home & "\Desktop\" & name & ".lnk"
Set link = shell.CreateShortcut(linkPath)
link.TargetPath = shell.ExpandEnvironmentStrings("%COMSPEC%")
link.Arguments = "/A /Q /K " & batPath
link.Save
