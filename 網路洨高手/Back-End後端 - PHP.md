### 概念
* [PHP正確之道](http://laravel-taiwan.github.io/php-the-right-way/#the_basics)
* [生態](https://github.com/zero85258/MyNOTE/blob/master/%E7%B6%B2%E8%B7%AF%E6%B4%A8%E9%AB%98%E6%89%8B/Back-End%E5%BE%8C%E7%AB%AF%20-%20PHP%E5%B8%B8%E7%94%A8%E5%A5%97%E4%BB%B6%E8%88%87%E7%94%9F%E6%85%8B.md)
* `使用APC`
PHP 是 Script 語言，執行時才會開始編譯，所以和其他預先 編譯好的語言(靜態語言) 比起來速度會比較慢。 
而如果 code 沒有變更，每次編譯其實是多餘的，如果能將重複編譯的時間和資源省下來，就可以讓 PHP 加速不少。
* [記憶體消耗議題](https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/238856/)

### 基本
```php
<?php
  //寫程式
?>
```
### 引入外部文件  
```
	--- 古代php -----------------------------------------
	include 和 require 語句是相同的，除了錯誤處理方面：
	
	require_once //會生成致命錯誤（E_COMPILE_ERROR）會"停止程式"
	include_once //只生成警告（E_WARNING），並且程式會繼續

	include_once 'filename';
	require_once 'filename';
	
	--- 現代秒殺方案 ----------------------
	composer install
	require 'vendor/autoload.php';
	
	--- 預處理器 --------------------------
	//跟c #define 差不多
	define("Pi",3.1415926);	
	
	--- namespace ,use ------------------------------------
	namespace ;	//定義
	use ;		//使用被定義的
```
### 一些常用函式 

	//檢查 空值
	isset($a);	//判斷是否 為 不為null || 設置過 , 與is_null完全相反
	is_null($a);	//判斷是否 為 null || 未初始化 , 與isset完全相反
	empty($a);  	//判斷是否 為 所有狀態0(包刮未設定) , 與if($a)完全相反
	in_array(這個東西,是陣列的元素嗎);	//回傳true false
	

|  | empty ==null | is_null ===null | isset | array_key_exists |
| -------- | -------- | -------- | -------- | -------- |
|未初始化 |   T   |   T   |   F   |   F   
|   null |   T   |   T   |   F   |   T   
|     "" |   T   |   F   |   T   |   T   
|     [] |   T   |   F   |   T   |   T
|      0 |   T   |   F   |   T   |   T      
|  false |   T   |   F   |   T   |   T   
|   true |   F   |   F   |   T   |   T   
|      1 |   F   |   F   |   T   |   T   
|     \0 |   F   |   F   |   T   |   T   
	
	
	---檢查 字串長度--------------------------------------------------------------------
	strlen()	//計算字元長度
	mb_strlen()	//計算字的長度
	sizeof()
	
	---轉型----------------------------------------------------------
	$變數 = (int)$變數;
	
	---打印--------------------------------------------------------------------------
	print_r($陣列名);  	//這樣才會排版echo '<pre>', print_r($陣列名), '</pre>';
	var_dump($陣列名);		//這樣才會排版echo '<pre>', var_dump($陣列名), '</pre>';
	
	--- 跳轉 -------------------------------------------------------------------------
	header("location:login.php");		//跳轉到login頁面
	alertMes("雞雞硬硬","gginin.php");		//顯示字串"雞雞硬硬",跳轉到gginin.php頁面

	unset($a,$b,$c);		//銷毀變數$a,$b,$c
	
	--- 讀檔寫檔 ----------------------------------------------------------
	file_put_contents() == 和依次調用fopen（），fwrite（）以及fclose（）功能一樣。
	file_get_contents() == 和依次調用fopen（），fread（）以及fclose（）功能一樣。
 
 ### 字串處理 
	
	//字串拼接	(得到'abcdef')
	$str = 'abc' . 'def';
	$str = '{$變數1}start?{$變數2}';
	
	//字串切割 切割完丟到陣列 (設$pizza  = "piece1 piece2 piece3 piece4 piece5 piece6";)
	$array = explode(" ", $pizza);	//一遇到 空格 就切
	
	//把陣列 組成 字串
	$str = implode(" ", $array);
	
	//正規表達式 比對 回傳ture,false	,(取代ereg函式) 
	preg_match("正規式", 被檢查的變數);
	
	//盡量用mb系列
	//mb_開頭的函式,設計用來專門用來處理 Unicode 字串
	//請使用UTF8
	
	//移除字串兩側的 空白字符||其他預定義字符
	$str = trim($str);
 
### 基本使用 
 
	//給變數$a 值5 (強行轉換成int)
	$a = (int)5;
	
	//回傳多個變數
	list($a, , $c) = getVar();
	
	//印在HTML碼上 (就是印在導覽器上)
	echo "HELLO! WORLD";  
	
### 函式() 

	function 函式名(傳入參數1, 傳入參數2, ...) {
		//程式區
	
		return array(10, 20, 30, 40, 50);	//回傳多數
	}
	
	call_user_func(function ($a, $b, $c) {
                return $a . $b . $c;
            },
                "asdf",
                "hgf",
                444
            )
	
	//先組裝函式名 | 執行
	{'displayAjax'.$action}();
	
### 流程控制 
	
	//ifelse
	if ($name == 'Jollen' &&  $i == 54){
   		echo "Hi! Jollen";
	}else{
   		echo "ddddddddd";
	}
	
	//單行if-else
	$b = ($a>1) ? 'a':'b';


	while ($a <= 10) {
   		//程式碼
	}
	
	//switch
	switch ($i) {
	   	case 'A': $grade = 90;
                 	break;
   		case 'b':
   		case 'B': $grade = 80;
                	break;
   		default: $grade = 60;
               		break;
	}
	
	//---3元------------------------
	c = a ?? b 	//c = a ? a : b;
	
### foreach 

	//設定一組同學分數的陣列，陣列所引使用學生名稱
	$Hash_Array = [
		'小三'=>77 , 
		'老張'=>56 , 
		'大威'=>94 , 
		'小史'=>84 ,
		'阿花'=>35 , 
		'小明'=>60
	];
	$數字_Array = [1,2,3,4,5];

	//用法1：印出 $value
	// foreach($來源陣列 as $value) <= 一定是填 $value
	
	foreach($Hash_Array as $value){
		echo $value."<br>";	// 輸出每個分數
	}


	//用法2：印出 $KEY + $value
	// foreach($來源陣列 as $KEY => $value) 
	
	foreach($Hash_Array as $KEY => $value){
		echo $KEY."的分數：".$value."分<br>";	//同時取出索引和值
	}

	//用法3：印出 $key
	// foreach($來源陣列 as $key) <= 一定是填 $value
	
	foreach($數字_Array as $key){
		echo $key."<br>";	// 輸出每個數字
	}
	
	//=== 多變數 ====================
	$points = [
    		["x" => 1, "y" => 2],
    		["x" => 2, "y" => 1]
	];

	foreach ($points as list("x" => $x, "y" => $y)) {
    		echo "Point at ($x, $y)", PHP_EOL;
	}
	
### 陣列 

	//範例：
	$name = ["Jollen", "Paul", "Ketty"];
	//相當於：
	$name[0] = "Jollen";
	$name[1] = "Paul";
	$name[2] = "Ketty";
	
	//關聯陣列
	$hashTB = [
		'郁夫夫' => '626Alien',
		'莊宜蓁' => 'MiMicAt'
	]; 
	//$hashTB[郁夫夫] == "626Alien"

	--- 多維陣列 ---------------------------------------------------------------------------------------
	$arr = [
		["name" => "tom", "number" => "101","sex" => "男","job" => "演員"],
		["name" => "jack", "number" => "102","sex" => "男","job" => "經理"],
		["name" => "mary", "number" => "103","sex" => "女","job" => "歌手"],
	];
	
### 陣列函數 

	PHP 陣列函數 - array()、count()、current()、list()、next()、pre()、reset()、key()
	
	//把陣列裡的值 賦予變量
	list($a,$b,$c) = array("Dog","Cat","Horse");	
	
	//回傳陣列元素個數
	count($陣列名);
	
	//把陣列元素組合成一個字串
	join(,);	
	
### $GLOBALS全域變數 

	$x = 75; 
	$y = 25;
 
 	//明明 沒傳進函數 卻可以 運算
	function add() { 
  		$GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y']; 
	}
	
	add(); 
	echo $z; 

### 變數 

	static $a = 1; //靜態變數$a
	global $a;     //全域變數$a
	$_SERVER   //一種 超全域變數

### 運算子 
	
	//php調用 類 的內部 static成員，或者是類之間調用就要用 ::
	類名::方法()	//但是透過::,就不會觸發 建構子() 解構子()
	

### 特殊函式 
	
	//亂數
	rand(50,900) 50~900
	
	//mt_rand更好的亂數	14~18
	mt_rand(14,18);    
	
	//eval可把 字串 變成 php程式碼
	eval('$a = $b;'); 
	
	//控制 導覽器快取的 函式
	http://rickykwan.iworkshop.com.hk/2010/03/656.html

### 路徑,檔案 ===============================================================
	
	//創建目錄 路徑/path/  名稱de
	mkdir("/path/de", 0700);
	
	//刪除這個路徑下的 檔案
	unlink("路徑/檔案");
	
	//這個路徑下的 檔案 存在?
	file_exists("路徑/檔案");	//這個路徑下的 檔案||目錄 存在?
	is_file("路徑/檔案");	//這個路徑下的 檔案 存在?
	is_dir("路徑/檔案");	//這個路徑下的 目錄 存在?
	
	//列印  目前路徑(包含檔案)
	echo __FILE__;	
	
	//列印  目前路徑(不包含檔案)
	echo dirname(__FILE__)
	
	//php.ini寫檔
	ini_set('display_errors', 1);
	
	//file_put_contents() 函數把一個 字符串 寫入 檔案 中。
	//可以看成 $this = fopen()+fwrite()+fclose()
	file_put_contents(檔案,要寫入的資料);

### PDO 函式 

	//印出變數的相關訊息於螢幕上
	var_dump($pdo);	
	
	//執行
	$pdo->exec($sql)	//執行一條sql語句 | rtn受影響的記錄的條數,如果沒有受影響的記錄，他返回0 (比較用於INSERT,DELETE,UPDATE)
	$pdo->query($sql);	//執行一條sQL語句,返回 PDO陳述物件(比較用於SELECT)
	
	//信息
	$pdo->lastInsertId();	//回傳最後插入的id號
	$pdo->errorCode();	//返回錯誤碼
	$pdo->errorInfo();	//返回錯誤信息陣列,要用print_r()打印
	
	//預處理,以及執行
	$pdo->prepare($sql);	//預處理SQL語句,可以提升運行速度
	$pdo->execute();	//執行預處理語句
	
	//結果集
	$pdo->fetchAll();	//取出結果集 全部紀錄
	$pdo->fetch();		//取出結果集 1條紀錄
	$pdo->fetchColumn();	//獲取指定記錄裡一個字段結果
	
	//設置語句默認的獲取模式
	$pdo->setFetchMode(PDO::FETCH_ASSOC);	//FETCH_ASSOC

	$pdo->getAttribute(PDO::ATTR_AUTOCOMMIT);	//得到 資料連接屬性 PDO::ATTR_自動提交
	$pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);	//設置 資料連接屬性 PDO::ATTR_自動提交
	constant()	//返回一個常數值

	$stmt->rowCount();	//return受影響的行數


	//防注入 法1
	$sql="INSERT user(username,password,email) VALUES(:username,:password,:email)";
	$stmt->bindParam(":username",$username,PDO::PARAM_STR);	//對輸入消毒,定一個參數到指定變量

	//防注入 法2
	$sql='INSERT user(username,password,email) VALUES(?,?,?)';
	$stmt->bindValue(1,$username);	//綁定一個參數到指定變量,但是事後怎麼改,都不會改到裡面第1次的值

	$stmt->columnCount();	//return 結果集列 個數
	while($stmt->fetch(PDO::FETCH_BOUND)){
		echo '用戶名：'.$username.'-密碼：'.$password.'-郵箱：'.$email.'<hr/>';
	}
	print_r($stmt->getColumnMeta(0));	//返回結果集中的一列元數據
	$stmt->bindColumn(1, $username);	//綁定一列到一個變量
	

	$stmt->debugDumpParams();	//打印一條預處理語句
	$stmt->nextRowset();		//將結果集中指針下移

### PHP與HTML表單互動---$_POST與$_GET 

	$_POST   //一般用於修改伺服器上的資源(寫在封包本體,寄出封包)
	//method="post"有資料量 8Mb 的限制，如果你要修改這個限制可以透過 php.ini 來設定。

	<form action="post.php" method="post">     <!  MyName是給PHP的變數,傳遞方法post >
		<input type="text" name="MyName" />   <! MyName變數值 >
		<input type="submit" value="送出表單" />  <!按下 "送出表單" 將透過 post 的方法將 MyName 傳遞給 post.php 這支 PHP 程式 >
	<form/>

	<?php 
		echo $_POST["MyName"]; //取得 HTML 表單中 MyName 的值
	?>


	$_GET 是透過 傳送 網址    //一般用於訊息傳送(寫在封包標頭,寄出封包)
	<form action="get.php" method="get">     <!  MyName是給PHP的變數,傳遞方法get >
	姓名:
		<input type="text" name="MyName" />   <! MyName變數值 >
		<input type="submit" value="送出表單" />  <!按下 "送出表單" 將透過 get 的方法將 MyName 傳遞給 get.php 這支 PHP 程式 >
	<form/>

	<?php 
		echo $_GET["MyName"]; //取得 HTML 表單中 MyName 的值
	?>
	
### form進階 

	<form action="doAdminAction.php?act=addAdmin&id=10" method="post">
	//把資料傳到"檔案doAdminAction.php",並且傳變數act=addAdmin , id=10

### $_SERVER 

	//假設我們的網址是 http://www.wibibi.com/test.php?tid=333
	//則以上 $_SERVER 分別顯示結果會是

	echo $_SERVER['HTTP_HOST'];	//顯示 www.wibibi.com
	echo $_SERVER['REQUEST_URI'];	//顯示 /test.php?tid=222
	echo $_SERVER['PHP_SELF'];	//顯示 /test.php
	echo $_SERVER['QUERY_STRING'];	//顯示 tid=222

	//透過這幾個 $_SERVER，我們已經取得了網址的各個部分，接著就是把網址給組合起來
	$URL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	echo $URL;


### URL後面一坨洨的意義 
PHP在網頁的型式主要採用幾個型式來傳遞變數。其中一個是 GET的方式，而另外一個是POST的方式。
而在上一章節中我們有簡單的的談到了一些httpd的運作，有談到關於傳遞的方式。
而這一章我們會比較詳細的介紹GET這二個傳遞方法。而我們在下一篇中介紹使用表單的POST的方法。

1.使用GET的方式傳遞變數:   (不適合用來傳送大量的資料或訊號)

	就是 URL 上指定  變數名稱,變數值   給 Web Server
	就是在  網址尾部  加上一個 問號? 之後進行宣告傳送的變數。而傳送的格式是:"變數名稱=變數的值"。

	例：http://www.php5.idv.tw/php5-article/ch1/ch1-05/get.php?age=10
	網址後面   加上   ?   之後,宣告了一個  變數age   值10


	將變數的值改為11時。php就會為我們處理這個age的變數值
	例：http://www.php5.idv.tw/php5-article/ch1/ch1-05/get.php?age=11



2.用GET傳遞多個變數

	傳多的變數時，用 & 來做 變數 的分隔

	例如:
		http://www.php5.idv.tw/php5-article/ch1/ch1-05/get2.php?age=11&name=Jacch%20chen&sex=1

	值:
		age=11  &
		name=Jacch%20chen  &
		sex=1
		//%20  的意思是空白,http協定中傳送的網址是必須要經過編碼的,實際上就是Jacch chen



	大家在會發現我們在傳送Jacch%20chen的時候中間的%20代表的是一個空白。
	由此大家可以知道在http協定中傳送的網址是必須要經過編碼的，
	這個編碼我們叫他url code ; 就是所謂的網址編碼。

	在聰明的IE流覽器中，流覽器 本身在 傳送URL 的時候流覽器會在最送傳送前再經過編碼，
	所以你如果在網址列上打上空白，在IE中是不會有變化的。
	不過在IE7之後也開始和其他流覽器一樣會開始在網址上直接編碼你的網址列了。


	可以使用var_dump(變數)來張開你的變數陣列。
	你可以使用var_dump來看到所有傳進來的變數
	<?php
		echo var_dump($_GET);
	?>


### 資安函數 

	md5() 	//目前已經有破解方式
	mcrypt 	//會是更好的加密方式
	sha1()
	$pdo->quote($username);	//防注入

