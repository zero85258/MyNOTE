### 概念
* Redis是php項目中使用到的主要暫存服務
* Memcache也是常用的

### Redis Pool
pool就是connection不關留著下次用。  
高級點的pool具有更強大的功能 比如開很多連接，直接使用空閒中的不再重新建立連寫。  
鑑於redis是單進程單線程的 所以一台服務器／服務 建立多個redis連接不一定有用吧。  
樓上提到的pool也是一個db一個連接的。  

### 概念圖

```
  客戶端 -----set/get命令------> Redis,Memcache
  (後端) <------返回-----------
```

### 設置CacheServer
  
###### php
1. 安裝
2. 連接redis服務 connect(127.0.0.1,6379)   //port=6379
3. 下指令

### 指令
```bash
# 寫入CacheServer資料 
set KEY VALUE

# 範例
>>>set name 郁夫夫
>>>ok
  
//取得CacheServer資料
set KEY
//範例
>>>get name
>>>"郁夫夫"
  
//刪除CacheServer資料 
del key
  
//設置過期時間
setex name 30 郁夫夫   //30秒後資料就會消失
```

### 程式碼

```php
<?php
    $redis = new Redis();
    $redis->connect(127.0.0.1,6379);   //port=6379
      
    //寫入
    $redis->set('name',郁夫夫);
      
    //讀出
    var_dump($redis->get('name'));
?>
```   
      
      
      
