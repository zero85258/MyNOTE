# OSX 

### 安裝
[iterm2](https://iterm2.com/)

### 命令行 
```
brew	#就是linux 的apt-get
brew	
```
### 常用快捷鍵
```
工作管理員	cmd + alt + esc
搜尋	  cmd + 空白
```

# Win 10 

### 安裝
* [PowerLine](https://medium.com/@slmeng/how-to-install-powerline-fonts-in-windows-b2eedecace58)
* [Hyper.Js](https://hyper.is/)

### 常用快捷鍵
```
win鍵 + Ctrl+D：開啟一個新的虛擬桌面
win鍵 + Ctrl+F4：關閉虛擬桌面
win鍵 + Ctrl+左/右：切換虛擬桌面
{ctrl  + shift  + J}	//打開chrome console
{F6}			//跳到URL欄
{ctrl + Home}		//滑到頂端
{ctrl + End}		//滑到底端
{Win + D}       //全部視窗縮小
```

### 子系統(WSL)
[教學](https://blog.birkhoff.me/bash_on_windows_installation/)
