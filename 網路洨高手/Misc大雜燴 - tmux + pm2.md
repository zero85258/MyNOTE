# tmux 
[做任何事之前都要先按 Ctrl + b](https://larrylu.blog/tmux-33a24e595fbc)
	
---

```bash
tmux ls 	#查看有哪些 client(大畫面)
tmux a -t 1 	#連進第１個大畫面
```

---
```
	前置: >>> Ctrl + b {	
		#切割上下 小螢幕pane:
			>>> shift + %
		#切割左右 小螢幕pane:
			>>> shift + "
		#移動到另一個 小螢幕pane:
			>>> 方向鍵
		#放大/縮小 小螢幕pane:
			>>> z
		#滾動:
			>>> [	#q離開
		#顯示編號:
			>>> q
		#關閉所在 小螢幕pane:
			>>> x
		#離開 tmux:
			>>> d


		#移動大螢幕windows:
			>>> p上個  n下個
	}
```

# pm2 
```bash
pm2 start pm2-listen/backend-queue.json		#監聽
pm2 log
pm2 ls	#pm2 list
pm2 kill
pm2 monit

```
### 一些bug 
