### Google reCAPTCHA API
* [網址](http://www.smalljacky.com/programming-language/php/google-recaptcha-ajax-for-php/)

##### 概念
```
Site Key(公鑰):	//用在「客戶端」HTML
提供 reCAPTCHA 驗證碼服務 給 使用者

Secret key(密鑰)://用在「伺服器端」
網站 <---> Google 之間的通訊
	
當伺服器端接收到正確的"g-recaptcha-response"驗證碼
```

### 架構
```
index.php 把資料傳給 captcha.php
客戶端 傳送 g-recaptcha-response 給 伺服器端
確認 驗證碼 跟 IP

index.php 把資料傳給 captcha.php
```

### 客戶端


### 伺服器端
