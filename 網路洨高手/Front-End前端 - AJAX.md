### 新趨勢 
* 以前是用JQuery的$.ajax
* 現在建議使用fetch API
	
### 概念 

	--- ajax流程 -------通常會有2頁面 URL(前端頁面) URL(出口頁面)----------------------
	//輸入 URL(前端頁面)
	//顯示前端	
	//$.ajax 發出 req 
	//Server 收到 req
	//Server 回吐 res(json)
	//吐到 URL(出口頁面)
	//$.ajax 去 URL(出口頁面) 接
	//$() 操作取回的資料
	
	----------------------------------------------------------------------------
##### Client端{
```js
		//當事件觸發,就用jquery發起請求
		$.ajax({ 
			type: "POST", 	
			url: "http://127.0.0.1:8000/ajaxdemo/serverjsonp.php",
			data: {	//都是些要傳的變數
				name: $("#staffName").val(), 
				number: $("#staffNumber").val(), 
				sex: $("#staffSex").val(), 
				job: $("#staffJob").val()
			},
			dataType: "json",
			success: function(data){
				if (data.success) { 
					$("#createResult").html(data.msg);
				} else {
					$("#createResult").html("錯誤：" + data.msg);
				}  
			},
			error: function(jqXHR){     
				alert("錯誤:" + jqXHR.status);  
			},     
		});
```
	
##### Server端
```PHP
//當接收到請求
if(Server端 收到$_GET || $_POST) {
	//把資料echo出來
}
```

=== Ajax開發框架 ============== 

	fat client架構
	瀏覽器{業務邏輯,表現層}
	伺服器{資料層}
	伺服器在此架構下通常僅用於提供及儲存資料。
	優點
	程式設計師可以充分利用JavaScript搭配業務邏輯來做出特殊的使用者介面，以符合終端使用者的要求。
	缺點
	第一，JavaScript語言本身之能力可能不足以處理複雜的業務邏輯。
	第二，JavaScript的執行效能不好。
	第三，JavaScript存取伺服器資料，仍需適當的伺服器端程式之配合。
	第四，瀏覽器相容性的問題又出現


	thin client架構 , server-centric架構
	瀏覽器{使用者介面引擎}
	伺服器{表現層,業務邏輯,資料層}
	
### XHR

	用html,css 實現頁面
	用 物件XMLHttpRequest(簡稱 XHR物件) 和 web伺服器 進行 資料 異步交換 
	用JavaScript操作DOM,實現動態局部刷新

##### 不含侏儸紀導覽器IE7,FireFox,Chrome,Opera,Safari
```js
var request = new XMLHttpRequest();  //建立 XHR物件
```
##### 涉及侏儸紀導覽器
```js
var request;
if (window.XMLHttpRequest){
	request = new XMLHttpRequest();     //套用IE7,FireFox,Chrome,Opera,Safari
} else {
	request = new ActiveXObject("Microsoft.XMLHTTP"); //套用IE6,IE5等侏儸紀導覽器
}
```
### HTTP請求

	STEP.1 建立TCP連接
	STEP.2 導覽器 向 伺服器 發出請求命令
	STEP.3 導覽器 發送 請求頭信息(Headers)
	STEP.4 伺服器 響應
	STEP.5 伺服器 發送 響應頭信息(Headers)
	STEP.6 伺服器 向 導覽器 發送數據
	STEP.7 關閉TCP連接

	HTTP "請求"4部分  //TXD
	1.請求的方法或動作,例如GET或POST
	2.正在請求的URL 
	3.請求頭信息(Headers),包含客戶端環境訊息,身分驗證信息等等..
	4.請求的正文,正文可以是提交的查詢字串,表單信息等..

	HTTP "響應"3部分  //RXD
	1.一個數字和文字組成的狀態碼,用來顯示"請求"是成功還是失敗   response.readyState 
	2.響應標頭,內記錄了伺服器的訊息 伺服器類型,日期時間,內容,長度等.....
	3.響應正文


### JQuery $.ajax
```js
<input type="text" id="keyword" />
<button id="search">查詢</button>
<p id="searchResult"></p>


<input type="text" id="staffName" /><br>
<input type="text" id="staffNumber" /><br>

<select id="staffSex">
<option>女</option>
<option>男</option>
</select><br>

<input type="text" id="staffJob" /><br>
<button id="save">保存</button>
<p id="createResult"></p>

<script src="http://apps.bdimg.com/libs/jquery/1.11.1/jquery.js"></script>

<script>
$(document).ready(function(){ 
	$("#search").click(function(){ 
		$.ajax({ 
			type: "GET", 	
			url: "http://127.0.0.1:8000/ajaxdemo/serverjsonp.php?number=" + $("#keyword").val(),
			dataType: "jsonp",
			jsonp: "callback",
			success: function(data) {
				if (data.success) {
					$("#searchResult").html(data.msg);
				} else {
					$("#searchResult").html("錯誤:" + data.msg);
				}  
			},
			error: function(jqXHR){     
				alert("錯誤:" + jqXHR.status);  
			},     
		});
	});
	
	$("#save").click(function(){ 
		$.ajax({ 
			type: "POST", 	
			url: "http://127.0.0.1:8000/ajaxdemo/serverjsonp.php",
			data: {	//都是些要傳的變數
				name: $("#staffName").val(), 
				number: $("#staffNumber").val(), 
				sex: $("#staffSex").val(), 
				job: $("#staffJob").val()
			},
			dataType: "json",
			success: function(data){
				if (data.success) { 
					$("#createResult").html(data.msg);
				} else {
					$("#createResult").html("錯誤：" + data.msg);
				}  
			},
			error: function(jqXHR){     
				alert("錯誤:" + jqXHR.status);  
			},     
		});
	});
});
</script>
```

### 網路跨域問題
* jsonp 只能對$GET , $POST不可

```js
<script>     //前端
	function(){
		$.ajax({
			dataTyoe:"jsonp" ,
			jsonp:"call9527"
		});
	}
</script>
```

```php
<?php
	function 接收(){
		jsonp=$_GET["call9527"];  //jsonp 只能對$GET , $POST不可
	}

	function 返回前端(){
		result = $jsonp.'('{"success":false,"msg":"沒找到員工"}')';  //回傳json格式 物件
	}
?>
```



