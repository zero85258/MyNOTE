
  [每天摸一個](http://linux.vbird.org/linux_basic/redhat6.1/linux_06command.php#more)

##### 位置,查看 
```bash
pwd 	#當前位置
cd ~ 	#進入 家目錄
cd .	
cd ..	#回到	上層目錄
ls 	#列出 當前目錄包含哪些 文件,文件夾
ls -l	    #顯示ls + 細節
cat hello.c		#讀取文件內容,顯示到"終端"
tac hello.txt		#讀取文件內容,反向顯示到"終端"
less 檔案       #檔案查看; q離開
top  #查看cpu,ram等等的(可裝htop)
history	#查看過去下過的指令
pstree	#查看process tree的狀況
netstat -ntlp #查看服務對應的port
ps aux | grep 要找的
git log | less #東西less化
```

##### 複製,刪除,新增 
```bash
rm abcd 	            #刪除 文件abcd
cp -r hello1.c hello2.c	#hello1.c複製一份,並命名為hello2.c
touch helloworld.c	  #建立helloworld.c文件 
ln -s #軟連結(就是M$的捷徑)
mv 檔名a 檔名b	#既可以拿來複製 也可以重新命名
```

##### 執行相關,管線等等
```bash
vi helloworld.c		#編輯helloworld.c文件 
./helloworld.out	#執行helloworld.out文件 
vi MakeFile 		#打開編輯MakeFile

sh hello.sh		#執行hello.sh腳本

./hello.out && ls         #執行hello.out,假如前者成立 就執行ls
./hello.out || ls         #執行hello.out,假如前者成立 就不執行ls
./hello.out >> hello.txt  #把hello.c內容張貼到hello.txt(不覆蓋)
./hello.out > hello.txt   #把hello.c內容張貼到hello.txt(覆蓋)
./hello.out < hello.txt   #把hello.txt內容丟到hello.c執行

docker-compose up -d `類似子查詢` bash
```

##### 查找檔案,其他 
```bash
grep -n \s+	    #正則表達式 -n並且加上行號
which 檔案      #查看檔案路徑
find -name 檔案       #查看檔案路徑 (CentOS)
grep -r -n 'RedirectConversionData' *


指令\             #指令太長換行
```

##### 安裝相關 
```bash
dpkg --get-selections   #查看目前 已安裝的東西
sudo apt-get remove 東西  #解除安裝 東西
```

##### 壓縮相關 
[link](http://note.drx.tw/2008/04/command.html)

```bash
*.zip       zip 程式壓縮的檔案；
*.gz        gzip 程式壓縮的檔案；
*.bz2       bzip2 程式壓縮的檔案；
*.xz        xz 程式壓縮的檔案；
*.tar       tar 程式打包的資料，(僅打包)並沒有壓縮過；
*.tar.gz    打包 + gzip 的壓縮
*.tar.bz2   打包 + bzip2 的壓縮
*.tar.xz    打包 + xz 的壓縮
```

##### 組合使用 
```bash
ls /etc >> etc.txt 	 //把 目錄/etc 底下的所有文件名 貼到 etc.txt

echo "XDDDD"	//螢幕上顯示XDDDD
echo $?		 //列印標準錯誤流
./hello.out 1>t.txt 2>f.txt < input.txt	//讀入input.txt,執行正確就寫到t.txt,錯誤訊息寫到f.txt

ls /etc/ |  grep ab	//把ls /etc/的輸出,當成 grep ab的輸入	("|"這東西叫"管道")
```

##### vim 
```bash
:/關鍵字   #搜尋字串   按n找下一個 , shift+n找上一個
:set nu   #顯示行號
:set nu!  #取消行號

dd        #剪下整行
p         #貼上
y         #複製

gg        #到 頂端(編輯器)
shift + g #到 底端(編輯器) 
cw        #刪除現在單字 | 進入編輯模式
u         #回到上1步
```

##### 組合技 
```bash
ctrl + shift + v  #選取多行模式 visual block()  (按下之後可以上下左右) 
shift + i         #編輯多行()

修改多個row的col{
  >>>按下 ctrl + shift + v
  >>>按下 d #刪除
  >>>按下 ctrl + shift + v  #再次選取要編輯的區域
  >>>按下 shift + i 
  >>>輸入 單字
  >>>按下 esc 2次 
}

從_現在這行_刪到_第n行{
  >>>按下 d
  >>>按下 行數
  >>>按下 g 2次
} 
```

##### 環境變量 
```bash
vim ~/.bash_profile     #編輯完要source ~/.bash_profile 

  --- 範例 --------------------------------------
  #bin通常底下放的是.sh執行檔
  export PATH=$PATH:/usr/local/Cellar/php71/7.1.11_22/bin/:$HOME/.composer/vendor/bin
  alias ll="ls -lah"
```

##### ssh(ssh連線相關) 
```bash
/.ssh/config
```

##### 用scp 把東西從ec2 上傳下載 
```bash
scp stg.adbert:~/stg-20180328.sql ~/Desktop #下載
scp stg-20180328.sql stg.adbert:~           #上傳
```

##### crontab(排程功能) 
```bash
crontab -e	#開啟排程列表(user的)
/etc/crontab	#(系統的)

#每分鐘執行
*/1 * * * * /bin/bash /home/ec2-user/check-services/checker.sh new-live.adbert-backend 172.31.4.247/fbg httpd > /dev/null 2>&1
```

##### 自動啟動項設定位置 
```bash
/etc/rc.d/rc.local
```

##### 硬碟空間使用量 相關 
```bash
du 檔案 #查看檔案大小
du -sh #查看當前目錄底下的大小
du -sh /var/www/html/center-control/* | sort -rn | head   #查看指定目錄下的使用空間
du -sh ./* | sort -rn | head	#查看當前目錄底下的使用空間
df    #列出所有槽
df -h #列出所有槽

```

##### process相關 
```bash
ps -ef | grep go #搜尋process
pstree
kill	-9 [id]  #程序的強制終止指令(暴力砍掉)
```

##### curl 
```bash
curl -I http://localhost	//查看curl的結果Status

curl -X GET "http://172.31.28.239/ckr" -H "accept: application/json"	#發GET
curl -X POST "http://172.31.28.239/ckr" -H "accept: application/json"	#發POST
```

##### 設置虛擬記憶體 
```bash
swapon -s		#查看目前使用的虛擬記憶體 資料
fallocate -l 4G /swapfile #在根目錄底下建4G 大小的swap檔案
	
swapoff /swapfile		#關閉off
chmod 600 /swapfile
mkswap /swapfile		#格式化
swapon /swapfile		#打開on
```

##### systemd服務相關 
```bash
systemctl [start|stop|restart] unit.service	#啟動/關閉/重新啟動 的方式為：
systemctl [enable|disable] unit.service		#設定預設啟動/預設不啟動的方式為： 
systemctl list-units --type=service 		#查詢系統所有啟動的服務用
systemctl list-unit-files --type=service	#而查詢所有的服務 (含不啟動) 使用 
```

##### 其他 
```bash
# 簡單來說的意思, 是將左邊程式的所有標準輸出 stdout, 及標準錯誤輸出 stderr 導向到 /dev/null, 
# 即左邊的程式只會執行, 而不會輸出任何程式執行的結果。
程式 > /dev/null 2>&1 

# 讓程式登出後可繼續執行,並且背景執行
nohup 程式 &
```

##### 符號 
```bash
$! 代表最後執行的後台命令的PID
```

##### 感覺不是很常用～～ 
```bash
tail -87 顯示檔案最後的87 行
tail -f #持續顯示 有點像pm2 log
```
