	
	//資料庫連線相關
	config/database.php 讀取 .env 裡面的連線參數
	
	//連線相關參數(host,資料庫名,username,password)
	//在.env下更改
	
	//每一個Model 會對應一個 資料表table

---讀寫連接--------------------------------------------

有時您可能希望為SELECT語句使用一個 數據庫連接，
另一個用於INSERT，UPDATE和DELETE語句。

Laravel使之變得輕而易舉，無論您是使用
	原始查詢()
	查詢構建器()
	Eloquent ORM()
將始終使用正確的連接。

要了解 如何配置讀/寫連接，我們來看這個例子：

'mysql' => [
    'read' => [
        'host' => '192.168.1.1',
    ],
    'write' => [
        'host' => '196.168.1.2'
    ],
    'driver'    => 'mysql',
    'database'  => 'database',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
],


---多個數據庫連接-----------------------------------------------

