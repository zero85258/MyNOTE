===Seeding是啥================
	
	拿來產生大量測試資料的
	所有Seeder檔案放在database/seeds/資料夾底下

===命令==========================
	
	#產生seeder檔
	php artisan make:seeder {name}
	
	#執行 "產生測試資料"
	php artisan db:seed

===怎麼用=====================

	1.用 artisan命令列 來產生 seeder檔
		>>>php artisan make:seeder {name}
	2.撰寫seed檔內容
		在run() 裡寫 迴圈之類的產生
	3.設定DatabaseSeeder.php內的執行緒
	4.使用 artisan命令列 執行 db相關指令 >>>artisan db:seed
	5.確認執行後的結果
	
===構造=========================

	//run()方法裡 製造測試資料 (迴圈製造之類的)
	run()
	
	//利用 truncate()方法 打掉測試資料
	DB::table('posts')->truncate();
	
=== 範例 =======================================

	public function run(){
	
		//倒10筆資料
		for($i=0;$i<10;$i++){
			Model類::create([
				'title' => str_random(10),	//10個字元
				'content' => str_random(255),	//255個字元
			]);
		}
		
		$this->call('別的Seeder');	//可以call別的seeder
	}
