===概念========================================================
	
	//return回tpl樣板輸出(html)
	$html = $smarty->fetch(string template,string cache_id,string $compile_id);
	$html = $smarty->fetch();
	
	{$tEsSt}
	//變成
	<?php echo $this->_tpl_vars['tEsSt']; ?>
	
===基本============================================

	{*註釋*}
	
	$smarty = new Smarty();
	
	//以下這些是 路徑的設置
	$smarty->template_dir = "./templates";
	$smarty->compile_dir = "./templates_c";
	$smarty->config_dir = "./configs/";
	$smarty->cache_dir = "./cache/";
	
	//顯示樣版
	$smarty->display('basic.tpl');
	
	//從 後端 把東西丟 前端
	$smarty->assign('a',array(1,2,3));

---.tpl--------------------------------------

	<input type="text" name="num1" size="3" value="{$smarty.get.num1 *相當於PHP的$_GET['1']*}"/>
	<input type="text" name="num2" size="3" value="{$smarty.get.num2 *相當於PHP的$_GET['2']*}"/>
	<div>{ $smarty.get.num1 + $smarty.get.num2 }</div>

===判斷式 (判斷ASS)==========================================
---.php----------------------------
	
	//從 後端 把東西丟 前端
	$smarty->assign('a',"床前明月光");
	$smarty->assign('married',true);
	
---.tpl--------------------------------------

	{if $a == ''}
	1
	{elseif $married == true}
	2
	{else}
	3
	{/if}


===迴圈===========================================
---.php----------------------------
	
	//從 後端 把東西丟 前端
	$smarty->assign('a',array(1,2,3))
	
---.tpl--------------------------------------

	{section name=i loop=$a}
		{$a[i]} <br/>	
	{/section}

---.php----------------------------

	//從 後端 把東西丟 前端
	$smarty->assign('a',array(1,2,3))
	
---.tpl--------------------------------------

	{foreach from=a item=$i}
		{$i} <br/>	
	{/foreach}

===迴圈.cycle========================================
---.tpl--------------------------------------

	{section from=a item=$i}
		{*讓顏色交互出現*}
		<tr style="background-color:{cycle values="silver,white"}">
			<td>{$row[0]}</td>
			<td>{$row[1]}</td>
		</tr>	
	{/section}


===函式==============================================

	{func_name attr1='value' attr1='value'}
	
===template==================================================================
---templates_utf8/index.html------------------------------------------

<!DOCTYPE html>
<html lang="en">
	<head>
		<{include file="./admin/header.html"}>
	</head> 

	<body style> 
		<{include file="./admin/top.html"}>
		<div class="main-container container-fluid">
			<{include file="./admin/menu.html"}>
			<{include file="./admin/body.html"}>
      		</div>
 	</body>
</html>
	
---templates_c/#$^#$%G%$%^$%^index.php (放已編譯過的)-----------------------------


=== 其他 =======================================================================

	escape 用於 編碼||跳脫字元 
	(a variable to for example html, url, single quotes, hex, hexentity, javascript and mail. By default its html.)
	
	
