;佇列
;使用方法如下
/*
	sDataStructureStack("Push",5)
	sDataStructureStack("PUSH",10)
	sDataStructureStack("push",15)
	sDataStructureStack("Push",20)
	sDataStructureStack("Push",25)
	sDataStructureStack("Pop")
	sDataStructureStack("pop")
	sDataStructureStack("POP")
	sDataStructureStack("Pop")
	$app := sDataStructureStack("Pop",0)
	MsgBox %$app%
*/

;Input
/*
	$act	要Push 還是pop
	$value=0	給PUSH塞值用的
*/
;Output
/*
	回傳 POP出來的值
*/

sDataStructureStack($act,$value=0)
{
	;MsgBox %$act%
	Static $pointer := -1	;水位
	Static $Data := {} ;初始化
	
	If($act = "Push" OR $act = "push" OR $act = "PUSH")	
	{
		$Data[$pointer+1] := $value
		;MsgBox 塞入的值
		;MsgBox % $Data[$pointer+1]
		$pointer++
		
		/*
		;看佇列現有的值
		MsgBox 佇列現有的值
		$i = 0
		while($i <= $pointer)
		{
			MsgBox  % $Data[$i]
			$i++
		}
		MsgBox 現在水位%$pointer%
		*/
		
		
		Return
	}
	Else If($act = "Pop" OR $act = "pop" OR $act = "POP")
	{
		if($pointer = -1)	;空堆疊,不能吐任何東西
		{
			MsgBox Stack is empty
			Return
		}
		Else if($pointer >= 0)	;只有一個,不用排隊補上
		{
			$ExitData := $Data[$pointer] 
			$pointer--
			Return $ExitData
		}
	}
	Else
	{
		MsgBox 錯了
	}
	
	
} 