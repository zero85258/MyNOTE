;佇列
;使用方法如下
/*
	sDataStructureQueue("Push",5)
	sDataStructureQueue("PUSH",10)
	sDataStructureQueue("push",15)
	sDataStructureQueue("Push",20)
	sDataStructureQueue("Push",25)
	sDataStructureQueue("Pop")
	sDataStructureQueue("pop")
	sDataStructureQueue("POP")
	sDataStructureQueue("Pop")
	$app := sDataStructureQueue("Pop",0)
	MsgBox %$app%
*/

;Input
/*
	$act	要Push 還是pop
	$value=0	給PUSH塞值用的
*/
;Output
/*
	回傳 POP出來的值
*/

sDataStructureQueue($act,$value=0)
{
	;MsgBox %$act%
	Static $pointer := -1	;水位
	Static $Data := {} ;初始化
	
	If($act = "Push" OR $act = "push" OR $act = "PUSH")	
	{
		$Data[$pointer+1] := $value
		;MsgBox 塞入的值
		;MsgBox % $Data[$pointer+1]
		$pointer++
		
		/*
		;看佇列現有的值
		MsgBox 佇列現有的值
		$i = 0
		while($i <= $pointer)
		{
			MsgBox  % $Data[$i]
			$i++
		}
		MsgBox 現在水位%$pointer%
		*/
		
		
		Return
	}
	Else If($act = "Pop" OR $act = "pop" OR $act = "POP")
	{
		if($pointer = -1)	;空佇列,不能吐任何東西
		{
			MsgBox Queue is empty
			Return
		}
		Else if($pointer = 0)	;只有一個,不用排隊補上
		{
			$ExitData := $Data[0] 
			$pointer--
			Return $ExitData
		}
		Else					;需要排隊補上
		{
			;出口
			$ExitData := $Data[0] 
			
			;排隊補上
			$i=1
			while $i <= $pointer
			{
				$Data[$i-1] := $Data[$i] 
				$i++
			}
			
			;水位降低
			$pointer--
			
			/*
			;看佇列現有的值
			MsgBox 佇列現有的值
			$i = 0
			while($i <= $pointer)
			{
				MsgBox  % $Data[$i]
				$i++
			}
			*/
			
			Return $ExitData
		}
	}
	Else
	{
		MsgBox 錯了
	}
	
	
} 