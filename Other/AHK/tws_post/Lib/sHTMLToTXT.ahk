; 說明
;	HTML 轉 TXT
; 參數
;	$sHTML 要轉換的 HTML 原始碼
; 回傳
;	$sTXT 轉換後的 TXT
; Note
;

sHTMLToTXT($sHTML)
{
	$aData := aReadCSV("Lib\sHTMLToTXT.csv")
	
	For index, element in $aData
		$sTXT := RegExReplace($sHTML, "i)" element[1], element[2])
		
	Return $sTXT
}
