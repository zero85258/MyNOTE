; 說明
;	HTML 轉 BBCode
; 參數
;	$sHTML 要轉換的 HTML 原始碼
; 回傳
;	$sBBCode 轉換後的 BBCode
; Note
;

sHTMLToBBCode($sHTML)
{
	$aData := aReadCSV("Lib\sHTMLToBBCode.csv")
	
	For index, element in $aData
		$sBBCode := RegExReplace($sHTML, "i)" element[1], element[2])
		
	Return $sBBCode
}
