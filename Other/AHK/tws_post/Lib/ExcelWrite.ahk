﻿; 說明
;	寫入 Excel 指定欄位，若未指定檔案，則詢問是否另存新檔。
; 參數
;	$sExcel 要儲存的檔案路徑與檔名
;	$sSheet 哪個分頁
;	$sRange 欄位或範圍
;	$sData 要寫入的內容
; 回傳
;	無
; Note
;	之後可搭配表單，新增區域寫入。

ExcelWrite($sExcel, $sSheet, $sRange, $sData)
{
	If ($sSheet = "" or $sRange = "")
	{
		MsgBox,
			(LTrim
				缺乏定條件。
				$sSheet: %$sSheet%
				$sRange: %$sRange%
			)
		Return 0
	}
	If ($sData = "")
	{
		MsgBox, 無需要寫入的內容。
		Return 0
	}

	IfNotExist, %$sExcel%
	{
		MsgBox, 36,, %$sExcel% 檔案不存在，是否另存新檔？
		IfMsgBox Yes
		{
			IF $sExcel := uFileOpen("*.xlsx; *.xls")
				$sExcel .= ".xlsx"
			Else
				Return 0
			
			$oExcel := ComObjCreate("Excel.Application")
			$oWorkbook := $oExcel.Workbooks.Add
			$oWorkbook.Sheets($sSheet).Range($sRange).Value := $sData
			$oWorkbook.SaveAs($sExcel)
			$oExcel.Quit
		}
		Else
			Return 0
	}
	Else
	{
		$oExcel := ComObjCreate("Excel.Application")
		$oWorkbook := $oExcel.Workbooks.Open($sExcel)
		$oWorkbook.Sheets($sSheet).Range($sRange).Value := $sData
		$oWorkbook.Save
		$oExcel.Quit
	}

	; Creat New
	; $oWorkbook := $oExcel.Workbooks.Add
	
	; Save
	; $oWorkbook.SaveAs($sExcel)
	

	Return
}
