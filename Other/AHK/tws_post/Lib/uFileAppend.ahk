﻿; 說明
;	將內容寫入用戶指定的檔案
; 參數
;	$sFileName 要儲存的檔案路徑與檔名
;	$sData 要寫入的內容
;	$iOverWrite 是否覆蓋原檔內容, 1 為覆蓋, 0 為附加在原內容後方。
; 回傳
;	無

uFileAppend($sFileName, $sData, $iOverWrite:=1)
{
	IfNotExist, %$sFileName%
	{
		MsgBox, %$sFileName% 檔案不存在。
		Return 0
	}
	If ($sData = "")
	{
		MsgBox, 無需要寫入的內容。
		Return 0
	}
	
	If ($iOverWrite = 1)
	{
		FileDelete, %$sFileName%
		FileAppend, %$sData%, %$sFileName%
	}
	Else
		FileAppend, %$sData%, %$sFileName%
	
	Return
}
