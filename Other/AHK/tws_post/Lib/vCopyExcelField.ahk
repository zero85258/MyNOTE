﻿; 說明
;	複製 Excel 欄位資料到 Clipboard，並移除換行。
; 參數
;	無
; 回傳
;	無

vCopyExcelField()
{
	Clipboard = 
	Send ^c
	Sleep, 100
	ClipWait ,1 ;等待剪貼簿中出現文本.
	StringReplace, clipboard, clipboard, `r`n, , All ;移除剪貼簿換行
}
