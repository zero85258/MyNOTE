﻿; 說明
;	將 二維陣列 以一個 Field 一行的方式 轉換為 TXT 格式,
; 	Row 之間使用 ====== 隔開
; 參數
;	$aData 陣列內容
; 回傳
;	$sData 陣列轉換後的內容

sArray2ToTXT($aData)
{
	If ($aData.MaxIndex() = "")
	{
		MsgBox, 無需要轉換的內容。
		Return 0
	}
	
	$iRow = 1
	Loop % $aData.MaxIndex()
	{
		$iCol = 1
		Loop % $aData[$iRow].MaxIndex()
		{
			$sData .= $aData[$iRow][$iCol]"`n"
			$iCol++
		}
		$sData .= "======`n"
		$iRow++
	}
	Return $sData
}
