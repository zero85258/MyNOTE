﻿; 說明
;	使用 iMacro 取得頁面 URL
; 參數
;	無
; 回傳
;	sValue 取得 Page 的 URL
; Note
;	[Done] 偵測到 iMacro = 完成
;	[Todo] 偵測到正確的 Title = 正確導頁，但有可能無法事前得知 Title。

iMacro_GetUrl()
{
	$sValue := iMacro("GetUrl", 10, 1)
	Return $sValue
}
