﻿; 說明
;	跟隨滑鼠游標的訊息提示, 預設顯示 3 秒後移除, 若不移除, 請加給第二參數 1
; 參數
;	$sMsg 要顯示的訊息
;	$iStatus ToolTip 狀態, 1為持續顯示, 0 為顯示 3 秒後移除, 預設為 0
; 回傳
;	無

vTipMouseMsg($sMsg, $iStatus:=0)
{
	Global $sMsgTipText := $sMsg
	SetTimer, WatchCursor, 100
	
	If ($iStatus = 1)
	{
		Return
	}	
	SetTimer, RemoveWatchCursor, 3000
}

WatchCursor:
	ToolTip, %$sMsgTipText%
	Return

RemoveWatchCursor:
	SetTimer, WatchCursor, Off
	SetTimer, RemoveWatchCursor, Off
	ToolTip
	Return
