﻿; 說明
;	固定在 Screen 0,0 的訊息提示, 預設顯示 3 秒後移除, 若不移除, 請加給第二參數 1
; 參數
;	$sMsg 要顯示的訊息
;	$iStatus ToolTip 狀態, 1為持續顯示, 0 為顯示 3 秒後移除, 預設為 0
; 回傳
;	無

vTipMsg($sMsg, $iStatus:=0)
{
	CoordMode, ToolTip, Screen
	ToolTip, %$sMsg%,1 ,1
	
	If ($iStatus = 1)
	{
		Return
	}	
	SetTimer, RemoveTipMsg, 3000
}

RemoveTipMsg:
	SetTimer, RemoveTipMsg, Off
	ToolTip
	Return
