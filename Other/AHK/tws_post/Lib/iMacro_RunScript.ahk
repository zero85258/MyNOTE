﻿; 說明
;	讀取 CSV 腳本執行各種命令
; 參數
;	$sFileName 包含路徑與檔名的 CSV 檔名
;	$aHash 轉換規則 Hash
; 回傳
;	無
; Note
;	參數替換之後使用欄位關聯，參考 HTML 轉 BBCode

iMacro_RunScript($sFileName, $aHash:="Null")
{
	Loop, read, %$sFileName%
	{
		$aRef := [] ;參數陣列初始化
		Loop, parse, A_LoopReadLine, CSV
		{
			If (A_Index = 1)
				$sMacroName := A_LoopField
			Else If (A_Index = 2)
				$iTimeOut := A_LoopField
			Else If (A_Index = 3)
				$iIsReturn := A_LoopField
			Else
			{
				; 判斷是否需 element mapping，
				; 若需要，替換對應變數，加入陣列
				; 若不需要，直接加入陣列
				$sString := sConvertByHash(A_LoopField, $aHash)
				$aRef.Insert($sString)
			}
		}
		/* ; 測試 是否成是否成功讀取到陣列
		Loop % $aRef.MaxIndex()
			MsgBox % $aRef[A_Index]
		*/
		iMacro($sMacroName, $iTimeOut, $iIsReturn, $aRef)
	}
}
