﻿; 說明
;	使用 iMacro 將值賦予 element - By CSV
; 參數
;	無，AHK 將 iMacor 要使用到的 Xpath 跟 Value 直接存到 XpathSendValue.csv
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_SendValueByCSV($sDatasourcesPath)
{
	iMacro("XpathSendValueByCSV", 10, 0, [$sDatasourcesPath])
}
