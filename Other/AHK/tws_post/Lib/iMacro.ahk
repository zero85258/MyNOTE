﻿; 說明
;	呼叫 Firefox 執行 iMacro 指令
; 參數
;	$sMacroName 巨集名稱
;	$iTimeOut 逾時秒數 (可省略，30秒)
;	$iIsReturn 是否使用 Clipboard 回傳值 (可省略，預設值 0 代表無回傳值)
;	$aRef 傳值陣列 (可省略，預設值 Null 代表不使用 Clipboard 傳遞參數)
; 回傳
;	$sValue 回傳的值
; Note
;	$iTimeOut $iIsReturn 給預設值是因為 $sRef1&2 為可選參數，
;	而 AHK 中，首個可選參數右邊的所有參數都必須標記為可選的。
;	執行逾時之後可用 Log or Line 通知

iMacro($sMacroName, $iTimeOut:=30, $iIsReturn:=0, $aRef:="Null")
{
	; 產生 iMacro 執行命令
	$iMacroCommand = imacros://run/?m=%$sMacroName%.iim

	; 將活動視窗設定為 Mozilla Firefox
	uActiveFirefox()
	
	; 執行 iMacro
	Send ^l
	Sleep, 1000
	;Send ^{t} 
	SendInput %$iMacroCommand%
	Sleep, 1000
	Send {Enter}
     
	; 等待 iMacro 回報 任務完成
	SetTitleMatchMode, 3
	
	; 將傳值 $aRef 暫存至 Clipboard
	If ($aRef != "Null")
	{
		Loop % $aRef.MaxIndex()
		{
			; 將參數暫存至 Clipboard
			Clipboard =
			Clipboard := $aRef[A_Index]
			
			; 等待 iMacro 回報 任務完成
			WinWait, iMacros, , %$iTimeOut%
			
			If ErrorLevel
			{
				MsgBox, iMacro 執行逾時 %A_Index%
				Pause
				Return
			}
			Else
			{
				WinActivate
				Sleep, 1000
				Send {Enter}
			}
	
		}
	}
	
	SetTitleMatchMode, 2
	
	; 從 Clipboard 中取得回傳值
	If ($iIsReturn = 1)
	{
		$sValue = %Clipboard%
		Clipboard =
		Return $sValue
	}
	Else
		Return
}
