﻿; 說明
;	使用 iMacro 取得 element 的值
; 參數
;	$sRef 使用 Clipboard 傳遞 - 取值 element 的 Xpath
; 回傳
;	$sValue 取得 element 的值
; Note
;	[Done] 偵測到 iMacro = 完成
;	[Todo] 偵測到正確的 CLIPBOARD Value = 成功

iMacro_GetValue($sXpath)
{
	$sValue := iMacro("XpathGetValue", 10, 1, [$sXpath])
	Return $sValue
}
