﻿; 說明
;	使用 iMacro 導頁
; 參數
;	$sRef 使用 Clipboard 傳遞 - 網址
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成
;	[Todo] 偵測到正確的 Title = 正確導頁，但有可能無法事前得知 Title。

iMacro_GoToUrl($sURL)
{
	iMacro("GoToUrl", 300, 0, [$sURL])
}
