﻿; 說明
;	將字串使用轉換規則 Hash 轉換，
; 	適用於 ElementMapping。
; 參數
;	$sString 要轉換的字串
;	$aHash 轉換規則 Hash
; 回傳
;	$sString 轉換後的字串
; Note

sConvertByHash($sString, $aHash)
{
	For index, element in $aHash
		StringReplace, $sString, $sString, %index%, %element%, All
	Return $sString
}
