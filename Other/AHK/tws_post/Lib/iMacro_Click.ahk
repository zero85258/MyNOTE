﻿; 說明
;	使用 iMacro 點擊 element
; 參數
;	$sRef 使用 Clipboard 傳遞 - 點擊 element 的 Xpath
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_Click($sXpath)
{
	iMacro("XpathClick", 10, 0, [$sXpath])
}
