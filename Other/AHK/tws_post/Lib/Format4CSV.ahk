﻿; 說明
;	二維陣列, 以一個 Field 一行的方式讀取到 $sData 中
; 參數
;	$aData 陣列內容
; 回傳
;	$sData 陣列轉換後的內容

Format4CSV(F4C_String)
{
	Reformat:=False ;Assume String is OK
	IfInString, F4C_String,`n ;Check for linefeeds
		Reformat:=True ;String must be bracketed by double quotes
	IfInString, F4C_String,`r ;Check for linefeeds
		Reformat:=True
	IfInString, F4C_String,`, ;Check for commas
		Reformat:=True
	IfInString, F4C_String, `" ;Check for double quotes
	{	Reformat:=True
		StringReplace, F4C_String, F4C_String, `",`"`", All ;The original double quotes need to be double double quotes
	}
	If (Reformat)
		F4C_String=`"%F4C_String%`" ;If needed, bracket the string in double quotes
	Return, F4C_String
}
