﻿; 說明
;	選擇要開啟的檔案
; 參數
;	$sFileType 要顯示的檔案類型
; 回傳
;	$sSelectedFile 選擇的檔案路徑與檔名
;	0 取消選擇檔案

uFileOpen($sFileType:="*.*")
{
	Thread, NoTimers
	FileSelectFile, $sSelectedFile, 24, , 開啟檔案, 檔案類型 (%$sFileType%)
	Thread, NoTimers, false
	If $sSelectedFile =
		Return 0 ; 取消選擇檔案
	
	Return $sSelectedFile
}
