﻿; 說明
;	將 一維陣列 轉換為標準 CSV 格式
; 參數
;	$aData 陣列內容
; 回傳
;	$sData 陣列轉換後的內容

sArray2ToCSV($aData)
{
	If ($aData.MaxIndex() = "")
	{
		MsgBox, 無需要轉換的內容。
		Return 0
	}
	
	$iRow = 1
	Loop % $aData.MaxIndex()
	{
		$iCol = 1
		Loop % $aData[$iRow].MaxIndex()
		{
			$sData .=  Format4CSV($aData[$iRow][$iCol])
			
			;最後一個 Field 不加半形逗號
			If ( $iCol < $aData[$iRow].MaxIndex() )
				$sData .= ","
			
			$iCol++
		}
		$sData .= "`n"
		$iRow++
	}

	Return $sData
}

/* 
	V1 的寫法，但無法應付 col 中有折行
	$iRow = 1
	Loop % $aData.MaxIndex()
	{
		$iCol := 1
		$MaxIndex := $aData[$iRow].MaxIndex()
		Loop % $MaxIndex
		{
			$sData .= $aData[$iRow][$iCol]
			
			;最後一個 Field 不加半形逗號
			if ( $iCol < $MaxIndex )
				$sData .= ","
			
			$iCol++
		}
		$sData .= "`n"
		$iRow++
	}

	; Format4CSV 官方範例
	TheString=
	(Join|
	例: She said, "We spent,NT$3,000."
	Smith, John
	Auto"Hot"Key
	Hogan, "Hulk"
	This "double" quote
	Plain old string
	$1,000,000
	Multi`nLine`nField
	)
	
	Loop, Parse, TheString, |
	{
		FormattedString:=Format4CSV(A_LoopField)
		Results.=A_LoopField . " -> " . FormattedString . "`n"
	}
	
	MsgBox,,Results,%Results%
*/
