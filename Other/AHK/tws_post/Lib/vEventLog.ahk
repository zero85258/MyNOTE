﻿; 說明
;	寫入 EventLog.csv（公共化 未完成）
;	記錄格式
;	EventType,DateCreated,TimeCreated,Computer,EventRecordID,Message
; 參數
;	$iEventType 事件類型
;	$sMessage 訊息內容
; 回傳
;	無
;

/* 筆記

EventLog.WriteEntry(Message, EventLogEntryType.Information, 1);
EventLog.WriteEntry(Message, EventLogEntryType.Error, 2);
EventLog.WriteEntry(Message, EventLogEntryType.Warning, 3);
EventLog.WriteEntry(Message, EventLogEntryType.SuccessAudit, 4);
EventLog.WriteEntry(Message, EventLogEntryType.FailureAudit, 5);

====

EventID
EventType
	1 : Information 成功、資訊
		描述應用程式、驅動程式或服務操作成功的事件。
		例如，當網路磁碟機載入成功，就會記錄資訊事件。
	2 : Error 錯誤
		重大的問題，如遺失資料或失去功能性。
		例如，若服務無法在啟動時載入，就會記錄錯誤。
	3 : Warning 警告
		不太重大但可能指出未來潛在的問題的事件。
		例如，當磁碟空間不足時，就會記錄警告。
	4 : SuccessAudit 成功稽核
		稽核的安全性存取嘗試成功。
		例如，使用者登入系統的成功嘗試動作將記錄為成功稽核事件。
	5 : FailureAudit 失敗稽核
		稽核的安全性存取嘗試失敗。
		例如，若使用者嘗試存取網路磁碟機失敗，則該嘗試動作將記錄為失敗稽核事件。
EventTypeName
Message

 */

vEventLog($iEventType, $sMessage)
{
	FormatTime, $sTimeCreated, Time, yyyy-MM-dd,HH:mm:ss
	$sEventLog = %$iEventType%,%$sTimeCreated%,%A_ComputerName%,%A_ScriptName%,%$sMessage%`r`n

	$sSysLogFileName = EventLog.csv
	FileAppend, %$sEventLog%, %$sSysLogFileName%
}
