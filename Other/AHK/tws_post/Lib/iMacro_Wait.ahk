﻿; 說明
;	使用 iMacro 導頁
; 參數
;	$iWaitValue 使用 Clipboard 傳遞 - 等待秒數（不超過 60 秒）
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_Wait($iWaitValue)
{
	iMacro("Wait", 70, 0, [$iWaitValue])
}
