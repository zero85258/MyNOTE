﻿; 說明
;	讀取 CSV 為二維陣列
; 參數
;	$sFilePath 要讀取的檔案路徑與檔名
; 回傳
;	$aData 從 CSV 讀取出來的二維陣列
; Note
;	日後希望能找到更好的方式處理 CSV 中兩個雙引號 "" 的處理方式
/*
雙引號
	下一個不是雙引號
		為 col 分隔起訖，通常在逗號前後，如："xxx","yyy","zzz"
	下一個是雙引號
		雙引號字元，取代為 @dq@
逗號
	起訖雙引號外
		跳下一個 col
	起訖雙引號內
		逗號字元
換行if
	起訖雙引號外
		New Row
	起訖雙引號內
		換行字元，取代為 @br@

步驟			
	將 "" 暫時取代為 @dq@
		"" => @dq@
		不這麼做的話我那不成熟的正規語法取值會有問題

	將 " 裡頭的 `n 暫時取代為 @br@
		`n => @br@

	如此，一個 row 就為一行，可使用 Loop, parse, A_LoopField, CSV 解析
	解析後
		@dq@ 還原為 "
		@br@ 還原為 `n
*/

aReadCSV($sFilePath)
{
	IfNotExist, %$sFilePath%
	{
		MsgBox, %$sFilePath% 檔案不存在。
		Return 0
	}
	
	FileRead, $sData, %$sFilePath%
	
	; 將 "" 暫時取代為 @dq@
	; 不這麼做的話，下面不成熟的正規語法取值會有問題
	StringReplace, $sData, $sData, "", @dq@, All
	
	; $2 為取出有雙引號的資料欄位
	$sHaystack = ([\s\S]*?)("[\s\S]*?")
	
	; 使用 ¢ 作為 Loop, parse 分割用符號
	$sData1 := RegExReplace($sData, $sHaystack, "$1¢$2¢")
	
	Loop, parse, $sData1, ¢
	{
		; 偶數為 $2
		If ( Mod(A_Index, 2) = 0 )
		{
			; 將 " 裡頭的 `n 暫時取代為 @br@
			StringReplace, $sTemp, A_LoopField, `r`n, @br@, All
		}
		Else
		{
			$sTemp := A_LoopField
		}
		$sData2 .= $sTemp
	}
	
	$iRow = 1
	$aData := [] ;初始化
	
	; 移除空行, 取代換行符號為 ¢ 作為 Loop, parse 分割用符號
	StringReplace, $sData3, $sData2, `r`n`r`n, `r`n, All
	StringReplace, $sData3, $sData2, `r`n, ¢, All
	
	Loop, parse, $sData3, ¢
	{
		If (A_LoopField = "")
			Continue

		$iCol = 1
		$aData[$iRow] := [] ;初始化
		
		Loop, parse, A_LoopField, CSV
		{
			StringReplace, $sTemp, A_LoopField, @dq@, ", All
			StringReplace, $sTemp, $sTemp, @br@, `n, All
			
			$aData[$iRow][$iCol] := $sTemp
			$iCol++
		}
		$iRow++
	}
	
	Return $aData
}

/*  V1 的寫法，但無法應付 col 中有折行
	$iRow = 1
	$aData := {} ;初始化
	
	Loop, read, %$sFilePath%
	{
		$iCol = 1
		$aData[$iRow] := {} ;初始化
		
		Loop, parse, A_LoopReadLine, CSV
		{
			$aData[$iRow][$iCol] := A_LoopField
			$iCol++
		}
		$iRow++
	}
	Return $aData
*/	