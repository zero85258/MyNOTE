﻿; 說明
;	使用 iMacro 將值賦予 element - By Clipboard
; 參數
;	$sXpath 使用 Clipboard 傳遞 - 賦予值 element 的 Xpath
;	$sValue 使用 Clipboard 傳遞 - 賦予值的 Value
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_SendValue($sXpath, $sValue)
{
	iMacro("XpathSendValue", 10, 0, [$sXpath, $sValue])
}
