﻿; 說明
;	讀取 Excel 指定欄位
; 參數
;	$sExcel 要讀取的檔案路徑與檔名
;	$sSheet 哪個分頁
;	$sRange 欄位或範圍
; 回傳
;	$aData 取得欄位陣列
; Note
;	Excel 讀取進來後的陣列結構跟我原本用法不太一樣，須研究，現階段使用轉換處理。

ExcelRead($sExcel, $sSheet, $sRange)
{
	; 指定的 Excel 檔是否存在
	IfNotExist, %$sExcel%
	{
		MsgBox, %$sExcel% 檔案不存在。
		Return 0
	}
	If ($sSheet = "" or $sRange = "")
	{
		MsgBox,
			(LTrim
				缺乏定條件。
				$sSheet: %$sSheet%
				$sRange: %$sRange%
			)
		Return 0
	}
	
	; 建立指定的 Excel 物件
	$oExcel := ComObjCreate("Excel.Application")
	$oWorkbook := $oExcel.Workbooks.Open($sExcel)
	
	; 指定的工作表檔是否存在 Excel 檔中
	$iSheetFind = 0
	Loop % $oWorkbook.Sheets.Count
	{
		If $oWorkbook.Sheets(A_Index).Name = $sSheet
			$iSheetFind = 1
	}
	IF ($iSheetFind = 0)
	{
		MsgBox, 找不到指定的工作表
		Return 0
	}
	
	$aRangeData := $oWorkbook.Sheets($sSheet).Range($sRange).Value
	$oExcel.Quit

	/*
	MsgBox % "$aRangeData.MaxIndex(1):" $aRangeData.MaxIndex(1)
	MsgBox % "$aRangeData.MaxIndex(2):" $aRangeData.MaxIndex(2)
	MsgBox % "$aRangeData[1][1]:" $aRangeData[1]
	MsgBox % "$aRangeData[1][2]:" $aRangeData[1]
	MsgBox % "$aRangeData[1,1]:" $aRangeData[1,1]
	MsgBox % "$aRangeData[1,2]:" $aRangeData[1,2]
	pause
	*/

	; 單一 Cell 轉換為二維陣列
	IfNotInString, $sRange, :
	{
		$aData := {} ;初始化
		$aData[1] := {} ;初始化
		$aData[1][1] := $aRangeData
		Return $aData
	}
	
	; Range 轉換為二維陣列
	$aData := {} ;初始化
	Loop % $aRangeData.MaxIndex(1)
	{
		$iRow := A_Index
		$aData[$iRow] := {} ;初始化
		Loop % $aRangeData.MaxIndex(2)
			$aData[$iRow][A_Index] := $aRangeData[$iRow, A_Index]
	}

	Return $aData
}
