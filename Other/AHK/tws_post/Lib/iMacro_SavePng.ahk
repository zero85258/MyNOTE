﻿; 說明
;	使用 iMacro 將網頁存成 PNG
; 參數
;	$sRef 使用 Clipboard 傳遞 - 存檔名稱
; 回傳
;	無
; Note
;	檔案存在 Firefox_iMacro\Data\profile\iMacros\Downloads\
;	[Done] 偵測到 iMacro = 完成

iMacro_SavePng($sBaseFileName)
{
	iMacro("SavePng", 300, 0, [$sBaseFileName])
}
