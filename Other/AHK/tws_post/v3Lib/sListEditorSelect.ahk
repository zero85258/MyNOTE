﻿
sListEditorSelect($EditorArray){
	
	$PersonalEditor := $EditorArray[4]
	$CatalogEditor	:= $EditorArray[7]
	$OfficalEditor	:= $EditorArray[8]
	
	/*
	MsgBox %$PersonalEditor%
	MsgBox %$OfficalEditor%
	MsgBox %$CatalogEditor%
	*/
	
	;===GUI layout======================================================================
	Gui, Font, S16 CDefault Bold, 微軟正黑體
	Gui, Add, Text,cBlue , 選擇編輯		;副標題
	Gui, Add, Tab,, 一般編輯 | 擴增編輯
	Gui, Tab, 1
	Gui, Add, Button, gEditorBtn1, 1.個人編輯：%$PersonalEditor%
	Gui, Add, Button, gEditorBtn2, 2.分類編輯：%$CatalogEditor%
	Gui, Add, Button, gEditorBtn3, 3.非官方：%$OfficalEditor%
	Gui, Tab, 2
	Gui, Add, Button, gExtraEditorBtnA1, 淘氣分享星
	Gui, Add, Button, gExtraEditorBtnA2, 好康老司機
	Gui, Add, Button, gExtraEditorBtnB , 好商家
	Gui, Add, Button, gExtraEditorBtnC, 好醫生牙醫
	Gui, Add, Button, gExtraEditorBtnD1 xp150 yp-170, 台北好醫生
	Gui, Add, Button, gExtraEditorBtnD2, 新北好醫生
	Gui, Add, Button, gExtraEditorBtnD3, 桃園好醫生
	Gui, Add, Button, gExtraEditorBtnD4, 新竹好醫生
	Gui, Add, Button, gExtraEditorBtnD5, 苗栗好醫生
	Gui, Add, Button, gExtraEditorBtnD6, 台中好醫生
	
	Gui +AlwaysOnTop
	Gui, Show, xCenter yCenter, 選擇身份	;視窗名稱
	Pause On
	Gui, destroy
	Return %$editorial%		; 返回主程序

	;===按扭按下的動作==========================================================
	EditorBtn1:
		$editorial = %$PersonalEditor%
		Pause Off
		Return

	EditorBtn2:
		$editorial = %$CatalogEditor%
		Pause Off
		Return

	EditorBtn3:
		$editorial = %$OfficalEditor%
		Pause Off
		Return
		
	ExtraEditorBtnA1:
		$editorial = 淘氣分享星
		Pause Off
		Return

	ExtraEditorBtnA2:
		$editorial = 好康老司機
		Pause Off
		Return
		
	ExtraEditorBtnB:
		$editorial = 台灣好商家優質企業資料分享
		Pause Off
		Return
		
	ExtraEditorBtnC:
		$editorial = 台灣好醫生牙醫院所資料分享
		Pause Off
		Return

	ExtraEditorBtnD1:
		$editorial = 台北好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD2:
		$editorial = 新北好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD3:
		$editorial = 桃園好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD4:
		$editorial = 新竹好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD5:
		$editorial = 苗栗好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD6:
		$editorial = 台中好醫生醫療健康資訊分享
		Pause Off

}



/*
SelectEditor:
	Gui, Font, S16 CDefault Bold, 微軟正黑體
	Gui, Add, Text,cBlue , 選擇編輯
	Gui, Add, Tab,, 一般編輯 | 擴增編輯
	Gui, Tab, 1
	Gui, Add, Button, gEditorBtn1, 1.個人編輯：%$PersonalEditor%
	Gui, Add, Button, gEditorBtn2, 2.分類編輯：%$CatalogEditor%
	Gui, Add, Button, gEditorBtn3, 3.非官方：%$OfficalEditor%
	Gui, Tab, 2
	Gui, Add, Button, gExtraEditorBtnA1, 淘氣分享星
	Gui, Add, Button, gExtraEditorBtnA2, 好康老司機
	Gui, Add, Button, gExtraEditorBtnB xp150 yp-53, 好商家
	Gui, Add, Button, gExtraEditorBtnC, 好醫生牙醫
	Gui, Add, Button, gExtraEditorBtnD1 xp150 yp-53, 台北好醫生
	Gui, Add, Button, gExtraEditorBtnD2, 新北好醫生
	Gui, Add, Button, gExtraEditorBtnD3, 桃園好醫生
	Gui, Add, Button, gExtraEditorBtnD4, 新竹好醫生
	Gui, Add, Button, gExtraEditorBtnD5, 苗栗好醫生
	Gui, Add, Button, gExtraEditorBtnD6, 台中好醫生
	
	Gui +AlwaysOnTop
	Gui, Show, xCenter yCenter, 選擇身份
	Pause On
	Gui, destroy
	Return ; 返回主程序

	EditorBtn1:
		$editorial = %$PersonalEditor%
		Pause Off
		Return

	EditorBtn2:
		$editorial = %$CatalogEditor%
		Pause Off
		Return

	EditorBtn3:
		$editorial = %$OfficalEditor%
		Pause Off
		Return
		
	ExtraEditorBtnA1:
		$editorial = 淘氣分享星
		Pause Off
		Return

	ExtraEditorBtnA2:
		$editorial = 好康老司機
		Pause Off
		Return
		
	ExtraEditorBtnB:
		$editorial = 台灣好商家優質企業資料分享
		Pause Off
		Return
		
	ExtraEditorBtnC:
		$editorial = 台灣好醫生牙醫院所資料分享
		Pause Off
		Return

	ExtraEditorBtnD1:
		$editorial = 台北好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD2:
		$editorial = 新北好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD3:
		$editorial = 桃園好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD4:
		$editorial = 新竹好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD5:
		$editorial = 苗栗好醫生醫療健康資訊分享
		Pause Off
		Return

	ExtraEditorBtnD6:
		$editorial = 台中好醫生醫療健康資訊分享
		Pause Off

*/