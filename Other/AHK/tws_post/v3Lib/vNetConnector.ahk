﻿
vNetConnector($Line){

	SetWorkingDir %A_ScriptDir%	;%A_ScriptDir%當前腳本所在的目錄絕對路徑	;SetWorkingDir改變腳本當前工作目錄
	$sINIFilename := RegExReplace(A_ScriptName, ".{3}$", "ini")	;A_ScriptName當前腳本文件名稱,不含路徑
	IniRead, $NICPath, %$sINIFilename%, DefaultSection, NICPath

	$LineNum := 0
	;斷線用
	/*
	Run %$NICPath% Hinet off
	Sleep, 1000
	Run %$NICPath% Seednet off
	Sleep, 1000
	Run %$NICPath% Sonet off
	Sleep, 1000
	Run	%$NICPath% TFN off
	Sleep, 1000
	Run %$NICPath% APOL off
	Sleep, 1000
	*/
	
	if $Line = 
	{
		InputBox, $LineNum, 請輸入要連線的線路, 1:Hinet `n2:Seednet `n3:Sonet `n4:TFN `n5:APOL`n如：1, , , , , , , , 1
	}
	else
	{	
		;連線
		;Run %$NICPath% %$Line% on
		Sleep, 1000
		Return
	}
	
	if($LineNum = 1){
		$Line = Hinet
	}else if($LineNum = 2){
		$Line = Seednet
	}else if($LineNum = 3){
		$Line = Sonet
	}else if($LineNum = 4){
		$Line = TFN
	}else if($LineNum = 5){
		$Line = APOL
	}
	
	
	;連線
	;Run %$NICPath% %$Line% on
	Sleep, 1000
	Return
} 
