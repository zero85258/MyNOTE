﻿;傳入 
;	使用者底下使用中的Accounts陣列
;	並請使用者輸入要執行的網站腳本
;回傳
;	[$SiteNo,$TypeNo,$Id,$Pw]
aSelectSiteRtnSiteNoTypeNo($UsingAccountsArray){
	;MsgBox 現在位置aSelectSiteRtnSiteNoTypeNo
			
	;輸入db.site.tws 表格順序
	InputBox, $sNameSearch, 請輸入執行的網站編號(表格順序), 如:1, , , , , , , ,1
		If ErrorLevel
			MsgBox, 取消輸入
			
	SetWorkingDir %A_ScriptDir%	;%A_ScriptDir%當前腳本所在的目錄絕對路徑	;SetWorkingDir改變腳本當前工作目錄
	$sINIFilename := RegExReplace(A_ScriptName, ".{3}$", "ini")	;A_ScriptName當前腳本文件名稱,不含路徑
	IniRead, $sExcel, %$sINIFilename%, AccountsDbSiteTwsXlsx, AccountPath
	IniRead, $sSheet, %$sINIFilename%, AccountsDbSiteTwsXlsx, Sheet
	IniRead, $sRange, %$sINIFilename%, AccountsDbSiteTwsXlsx, Range
	
	
	;$sSheet = db.site.tws
	;$sRange = A2:W68
	
	;參數			D:\XXX\XXX\XXX.xlsx, Accounts, A2:W68
	$dbSiteTws := ExcelRead($sExcel, $sSheet, $sRange)		
	
	
	;對應找出 $SiteNo,$TypeNo
	$j = 1
	Loop % $dbSiteTws.MaxIndex()
	{
		IF($sNameSearch = $dbSiteTws[$j][2])
		{
			;SiteName
			;MsgBox % $dbSiteTws[$j][4]
			;SiteNo
			;MsgBox % $dbSiteTws[$j][1]
			$SiteNo := $dbSiteTws[$j][1]
			;TypeNo
			;MsgBox % $dbSiteTws[$j][8]
			$TypeNo := $dbSiteTws[$j][8]
			$WebLogin := $dbSiteTws[$j][13] 
			$WebPost := $dbSiteTws[$j][14]
		}
		
		$j++	
	}

	

;	$UsingAccountsArray
;   [no1][nickname][isp][id1][pwd1][siteNo1]
;   [no2][nickname][isp][id2][pwd2][siteNo2]
;   [no3][nickname][isp][id3][pwd3][siteNo3]

	;在去$UsingAccountsArray 翻出 該SiteNo下的$Id,$Pw
	$j = 1
	Loop % $UsingAccountsArray.MaxIndex()
	{
		IF($SiteNo = $UsingAccountsArray[$j][6])
		{
			;SiteName
			;MsgBox % $UsingAccountsArray[$j][6]
			;Id
			;MsgBox % $UsingAccountsArray[$j][4]
			$Id := $UsingAccountsArray[$j][4]
			;Pw
			;MsgBox % $UsingAccountsArray[$j][5]
			$Pw := $UsingAccountsArray[$j][5]
		}
		
		$j++	
	}
	
	Return [$SiteNo,$TypeNo,$Id,$Pw,$WebLogin,$WebPost]
}