﻿

aCSVRead($sCsvPath){
	;MsgBox 現在位置ReadCSV 即將把CSV轉成AHK的2維陣列
	FileRead, $OutputVar, %$sCsvPath%	;讀取%$sCsvPath%這檔案的內容,傳到 變量$OutputVar
	StringGetPos, $Pos, $OutputVar, `n ;找到第一個換行，取得位置
	$Pos++ ;因為下一行要使用 StringTrimLeft 從字符串的左邊移除一些字符，須把換行也移除掉，故這邊位置需要使用 ++ 多取一個
	StringTrimLeft, $OutputVar, $OutputVar, $Pos
	$Array := Object() ;初始化
	Loop, parse, $OutputVar, CSV
	{
		$Array[A_Index] := A_LoopField
		;MsgBox % $Array[A_Index]
	}
	/*
		1：標題
		2：文章關鍵字
		3：大綱(社群短文)
		4：個人編輯
		5：HTML
		6：BBCode
		7：非官方
		8：分類編輯
		9：部落格分類資料夾
		10：分類編輯心得篇數
		11：短標題(UDN相片標題)
		12：服務類別
			1.好東西
			2.好醫生
		13：上稿日期
	*/
	
	Return $Array
}



