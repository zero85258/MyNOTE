﻿
sCsvSelect(){	;選擇 CSV檔 回傳 路徑
	;MsgBox 現在位於CsvSelect 選擇 CSV檔 回傳 路徑
	Thread, NoTimers
	
	SetWorkingDir %A_ScriptDir%	;%A_ScriptDir%當前腳本所在的目錄絕對路徑	;SetWorkingDir改變腳本當前工作目錄
	$sINIFilename := RegExReplace(A_ScriptName, ".{3}$", "ini")	;A_ScriptName當前腳本文件名稱,不含路徑
	IniRead, $sArticlePath, %$sINIFilename%, DefaultSection, ArticlePath
	
	FileSelectFile, $sCsvPath, 3, %$sArticlePath%, 開啟檔案, 文案 (*.csv)
	If $sCsvPath =
	{
		MsgBox,
		(
			取消，程式結束。
		)
		Exitapp
	}
	Thread, NoTimers, false
	return $sCsvPath
}