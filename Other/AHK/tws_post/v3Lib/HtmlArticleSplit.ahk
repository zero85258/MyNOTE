﻿
; 說明
;	判斷 HTML 第一段是否為圖
;	* 將 Article 以 P 為單位陣列化
;	* 忽略無內容的行
;	* 判斷是否有 img 標籤
;	* 如果有，圖片開頭的 HTML 文章
;	* 如果沒有，則為文字開頭的 HTML 文章
;	* 全文拆分為 Sum & Cnt（這是哪種文？在哪裡拆分？），詳見 TWS_上稿前製作業自動化整理_使用 CSV.xmind
; 參數
;	$Article HTML 文章
; 回傳
;	無
; 
HtmlArticleSplit($Article)
{
	; 將 Article 以 P 為單位陣列化
	$Article := RegExReplace($Article, "i)</p>", "</p>`n") ;找到 </p>，加上換行
	$HtmlArray := StrSplit($Article, "`n") ;以 `n 為分隔符號
	
	Loop % $HtmlArray.MaxIndex()
	{
		$StrContent := $HtmlArray[A_Index]
		; 判斷是否有內容，若有內容則為第一行，接著判斷是否含有 img 標籤，
		; 有回傳 1 沒有回傳0
		if (StrLen($StrContent) > 20) ; 忽略無內容的行，此處不夠嚴謹，只能抓出 <p>&nbsp</p> 這類太短的內容，但寫作有規範的情況下，出錯機率應該不高。
		{
			if (RegExMatch($StrContent, "<img")) ;判斷 是否有 img 標籤。
				$HtmlFlag := "image" ;圖片開頭的 HTML 文章
			else
				$HtmlFlag := "txt" ;文字段落開頭的 HTML 文章
			$HtmlArrayPos := A_Index
			break
		}
	}
	
		
	; 全文拆分為 Sum & Cnt，這是哪種文？在哪裡拆分？
	if ($HtmlFlag = "image") ;圖片開頭的 HTML 文章
	{
		$StrLenSum = 0
		Loop
		{
			$HtmlArrayPos++
			$StrContent := $HtmlArray[$HtmlArrayPos]
			if (RegExMatch($StrContent, "<img")) ;判斷 是否有 img 標籤。
			{
				$HtmlArrayPos--
				break
			}
			else
			{
				$StrContent := RegExReplace($StrContent, "i)<[\s\S]*?>") ; 移除 HTML Tag，留下純文字。
				StringReplace, $StrContent, $StrContent, `&nbsp`;,, All
				StringReplace, $StrContent, $StrContent, %A_Space%,, All
				$StrLenSum += StrLen($StrContent)
				if ($StrLenSum > 30)
					break
				else
					continue
			}
		}
	}
	else ;文字開頭的 HTML 文章
	{
		Loop
		{
			$HtmlArrayPos++
			$StrContent := $HtmlArray[$HtmlArrayPos]
			if (RegExMatch($StrContent, "<img")) ;判斷 是否有 img 標籤。
				break
		}
	}
	
	; 產生 Sum & Cnt
	Loop % $HtmlArray.MaxIndex()
	{
		if ($HtmlArrayPos >= A_Index)
			$Sum .= $HtmlArray[A_Index]
		else
			$Cnt .= $HtmlArray[A_Index]
	}
	
	; Return $HtmlFlag ; 回傳是否為圖片開頭的文章
	
	Return [$Sum, $Cnt, $HtmlFlag] ; 回傳摘要與內文
}