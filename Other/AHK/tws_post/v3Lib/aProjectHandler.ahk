﻿/*
傳入
	把剛從csv讀到的陣列
	拿來處理符號,關鍵字種類,把一體成形的文本做切割
回傳
	處理完的陣列
*/

;主要切割
aProjectHandler($Array){
	;傳入================================================================================================
	;MsgBox 現在位置在aProjectHandler 處理文案細節
	$OriginalTitle := $Array[1]	;using
	$KeyWord := $Array[2]		;using
	$Summary := $Array[3]
	$HTML := $Array[5]			;using
	$BBCode := $Array[6]
	$Catalog := $Array[9]
	$ShortTitle := $Array[11]
	$ServiceType := $Array[12]
	$PostDate := $Array[13]
	
	
	
	/*	傳入的
		1：標題
		2：文章關鍵字
		3：大綱(社群短文)
		4：個人編輯
		5：HTML
		6：BBCode
		7：非官方
		8：分類編輯
		9：部落格分類資料夾
		10：分類編輯心得篇數
		11：短標題(UDN相片標題)
		12：服務類別
			1.好東西
			2.好醫生
		13：上稿日期
	*/
	
	/*
	MsgBox %$OriginalTitle%
	MsgBox %$KeyWord%
	MsgBox %$Summary%
	MsgBox %$HTML%
	;MsgBox %$BBCode%
	
	MsgBox %$Catalog%			;出口
	MsgBox %$ShortTitle%
	MsgBox %$ServiceType%		;??
	MsgBox %$PostDate%			;出口
	*/
	
	/*	blog
	$RstData[0] := $Title2
	$RstData[1] := $SumHtml
	;$RstData[2] := ;摘要形式
	$RstData[3] := $CntHtml 
	$RstData[4] := $Summary ;社群短文
	;$RstData[5] := ;PIXNET URL
	$RstData[6] := $Summary	;摘要純文字 好像
	$RstData[7] := $Catalog  ;個人分類
	$RstData[8] := $PostDate ;發文時間
	$RstData[9] := $HtmlFlag ;UDN相片標題
	$RstData[10] := 	;5組關鍵字(空白)
	$RstData[11] := 	;10組關鍵字(逗號)
	$RstData[12] := 	;10組關鍵字(空白)
	$RstData[13] := 	;8組關鍵字(逗號)
	$RstData[14] := 	;不限組關鍵字(逗號)
	$RstData[15] := 	;20組關鍵字(逗號)
	;============forum========================
	$RstData[16] := $Title2
	$RstData[17] := $BBCode
	$RstData[18] := $BBCode去url
	$RstData[19] := ;全文HTML
	$RstData[20] := ;全文HTML去<p></p>
	$RstData[21] := ;Copy Pixnet文章內容
	$RstData[22] := ;PIXNET URL
	$RstData[23] := ;5組關鍵字(空白)
	$RstData[24] := $Catalog  ;個人分類
	*/
	
	;處理區===================================================================
	;MsgBox %$KeyWord%
	;關鍵字拆分，輸入到對應欄位
	;StringSplit, $KwArray, $KeyWord , `,)	;出口$KwArray 陣列
	$KwArray := StrSplit($KeyWord, ",")
	
	; 處理特殊標題(替換符號)
	StringReplace, $Title2, $OriginalTitle, [, 【
	StringReplace, $Title2, $Title2, ], 】	;出口$Title2

	
	
	; HTML 文章拆分為摘要跟內文
	$HtmlArticle := HtmlArticleSplit($Array[5])
	
	$SumHtml := $HtmlArticle[1]		;出口$Sum	前言引言Html
	$CntHtml := $HtmlArticle[2]		;出口$Cnt	內文Html
	$HtmlFlag := $HtmlArticle[3]	;出口$HtmlFlag	
	
	$RstData := {} ;初始化
	$RstData[0] := {} ;初始化
	
	$RstData[0] := $Title2
	$RstData[1] := $SumHtml
	;$RstData[2] := ;摘要形式
	$RstData[3] := $CntHtml 
	$RstData[4] := $Summary ;這群
	;$RstData[5] := ;PIXNET URL
	$RstData[6] := $Summary	;摘要純文字 好像
	$RstData[7] := $Catalog  ;個人分類
	$RstData[8] := $PostDate ;發文時間
	$RstData[9] := $HtmlFlag ;UDN相片標題
	
	
	;5組關鍵字(空白)
	$RstData[10] := sKwHandler(5,"Space",$KwArray)
	;MsgBox % $RstData[10]
	
	;10組關鍵字(逗號)
	$RstData[11] := sKwHandler(10,"Comma",$KwArray)
	;MsgBox % $RstData[11]
	
	;10組關鍵字(空白)
	$RstData[12] := sKwHandler(10,"Space",$KwArray)
	;MsgBox % $RstData[12]
	
	;8組關鍵字(逗號)
	$RstData[13] := sKwHandler(8,"Comma",$KwArray)
	;MsgBox % $RstData[13]
	
	;不限組關鍵字(逗號)
	$RstData[14] := sKwHandler($KwArray.MaxIndex(),"Comma",$KwArray)
	;MsgBox % $RstData[14]
	
	;20組關鍵字(逗號)
	$RstData[15] := sKwHandler(20,"Comma",$KwArray)
	;MsgBox % $RstData[15]
	
	/*
	;被切割的關鍵字
	$i = 1
	Loop % $KwArray.MaxIndex()
	{
		$RstData[16] := {} ;初始化
		
		;創建同時 做些事
		$RstData[16][$i] := $KwArray[$i]	
		;MsgBox % $RstData[16][$i]
	
		$i++
	}
	*/
	
	/*
	;============forum========================
	$RstData[16] := $Title2
	$RstData[17] := $BBCode
	$RstData[18] := $BBCode去url
	$RstData[19] := ;全文HTML
	$RstData[20] := ;全文HTML去<p></p>
	$RstData[21] := ;Copy Pixnet文章內容
	$RstData[22] := ;PIXNET URL
	$RstData[23] := ;5組關鍵字(空白)
	$RstData[24] := $Catalog  ;個人分類
	*/
	
	$RstData[16] := $Title2
	$RstData[17] := $BBCode
	$RstData[18] := ;$BBCode去url
	$RstData[19] := $HTML
		
	StringReplace, $HTML, $HTML, <p> , ,All 
	StringReplace, $HTML, $HTML, </p> , ,All 
	$RstData[20] := $HTML	;全文HTML去<p></p>

	$RstData[21] := ;Copy Pixnet文章內容
	$RstData[22] := ;PIXNET URL
	$RstData[23] := sKwHandler(5,"Space",$KwArray)
	$RstData[24] := $Catalog  ;個人分類
	
	Return $RstData
}

;產生關鍵字用的
;		產生個數 空白或逗號   
sKwHandler($num,$SpaceOrComma,$KwArray)
{
	;MsgBox 進來的數%$num%
	$MaxSize := $KwArray.MaxIndex()
	
	;偵測 輸入值 是否多於 關鍵字個數
	IF ($num > $MaxSize){
		
		;替換
		$num = %$MaxSize%
	}
	
	;MsgBox 產生的數%$num%
	;產生n組關鍵字(逗號)or(句號)
	if($SpaceOrComma = "Comma")
	{
		$i = 1
		Loop %$num%
		{
			$temp := $KwArray[$i]
			
			if $i < %$num%
			{
				$str = %$str%%$temp%,
			}
			Else
			{
				$str = %$str%%$temp%
			}
			
			;MsgBox %$str%
			$i++
		}
	}
	else if($SpaceOrComma = "Space")
	{
		$i = 1
		Loop %$num%
		{
			$temp := $KwArray[$i]
			
			if $i <= %$num%
			{
				$str = %$str%%A_Space%%$temp%
			}
			Else
			{
				$str = %$str%%$temp%
			}
			
			;MsgBox %$str%
			$i++
		}
	}
	
	$kw = %$str%
	;MsgBox %$kw%
	return $kw
}