﻿; 讀取指定 Excel Sheet & Range

;讀取Excel
;回傳
;	2維陣列	(讀到的結果) 

aExcelReadToAhk(){
	SetWorkingDir %A_ScriptDir%	;%A_ScriptDir%當前腳本所在的目錄絕對路徑	;SetWorkingDir改變腳本當前工作目錄
	$sINIFilename := RegExReplace(A_ScriptName, ".{3}$", "ini")	;A_ScriptName當前腳本文件名稱,不含路徑
	IniRead, $sExcel, %$sINIFilename%, AccountsAccountsXlsx, AccountPath
	IniRead, $sSheet, %$sINIFilename%, AccountsAccountsXlsx, Sheet
	IniRead, $sRange, %$sINIFilename%, AccountsAccountsXlsx, Range

	;選擇AccountsXlsx
	;$sExcel := uFileOpen("*.xlsx; *.xls")		;回傳 D:\XXX\XXX\XXX.xlsx
	;MsgBox %$sExcel%
	
	;$sSheet = Accounts
	;$sRange = A2:S5294	
	
	;參數			D:\XXX\XXX\XXX.xlsx, Accounts, A2:W68
	$aData := ExcelRead($sExcel, $sSheet, $sRange)

	;$aData 就是儲存 總表 的 2維陣列
	Return $aData
}
