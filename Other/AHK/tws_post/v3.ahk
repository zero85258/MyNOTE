﻿;導入外部資源

#Include %A_ScriptDir%\v3Lib\aCombineArray.ahk
#Include %A_ScriptDir%\v3Lib\aCSVRead.ahk
#Include %A_ScriptDir%\v3Lib\aExcelReadToAhk.ahk
#Include %A_ScriptDir%\v3Lib\aProjectHandler.ahk
#Include %A_ScriptDir%\v3Lib\aSelectSiteRtnSiteNoTypeNo.ahk
#Include %A_ScriptDir%\v3Lib\aSendNameRtnIdPw.ahk
#Include %A_ScriptDir%\v3Lib\aSendSiteNoRtnProjectMethod.ahk
#Include %A_ScriptDir%\v3Lib\HtmlArticleSplit.ahk
#Include %A_ScriptDir%\v3Lib\sCsvSelect.ahk
#Include %A_ScriptDir%\v3Lib\sListEditorSelect.ahk
#Include %A_ScriptDir%\v3Lib\vNetConnector.ahk

#Include %A_ScriptDir%\v3Lib\sSelectRunScript.ahk

$sINIFilename := RegExReplace(A_ScriptName, ".{3}$", "ini")

;前置========================================================================
	;完成完成完成完成完成完成完成完成
	
	;yee()
	;ok
	$CsvPath := sCsvSelect()		;選擇 CSV檔 回傳 CSV檔的路徑

	;ok	
	$ProjectArray := aCSVRead($CsvPath)	;解析CSV,並把資料整理,回傳 整理分割完成的 文案2維陣列

	;待調整
	$FinishedProjectArray := aProjectHandler($ProjectArray)	;文案處理,切割等等等

	;ok
	$nickname := sListEditorSelect($ProjectArray)		;列出編輯者GUI,並且選擇,回傳使用者暱稱

	;ok
	$aData := aExcelReadToAhk()		;$aData 就是儲存 總表 的 2維陣列	

	;ok
	$UsingAccountsArray := aSendNameRtnIdPw($nickname,$aData)	;給 暱稱 回傳 使用者資料2維陣列

	;ok
	$Line := $UsingAccountsArray[1][3]		;線路名
	vNetConnector($Line)	;連線函式

	MsgBox 可開始執行腳本了`n ALT+2 選擇腳本並且運行`n ALT+3 手動模式`n ALT+4 只進行網頁登入`n ALT+5 更換使用者 

	yee:

;===前置_END=====================================================================



!1::
	goto yee
	Return
!2:: 
	;自動跑腳本===================================================================================================

	;ok
	;[1][2][3][4][5][6] [$SiteNo,$TypeNo,$Id,$Pw,$WebLogin,$WebPost]
	;可能到時候改成用GUI來選
	$RunScriptArray := aSelectSiteRtnSiteNoTypeNo($UsingAccountsArray)	;選擇要運行的網站
	
	;待調整
	$TypeNo := $RunScriptArray[2]
	$SiteNo := $RunScriptArray[1]
	;[1][2][3][4][5] [$Title,$Summy,$Cnt,$Keywords,$Catalog]
	$ProjectMethod := aSendSiteNoRtnProjectMethod($TypeNo,$FinishedProjectArray)
	
	$Title = % $ProjectMethod[1]
	$Summy = % $ProjectMethod[2]
	$Cnt = % $ProjectMethod[3]
	$Keywords = % $ProjectMethod[4]
	$Catalog = % $ProjectMethod[5]
	$Id = % $RunScriptArray[3]
	$Pw = % $RunScriptArray[4]
	$WebLogin = % $RunScriptArray[5] 
	$WebPost = % $RunScriptArray[6]
	
	; 產生 Hash    全部的填入的值
	$aHash := {}		
	$aHash["$Title"] := $Title
	$aHash["$Summy"] := $Summy
	$aHash["$Cnt"] := $Cnt
	$aHash["$Keywords"] := $Keywords
	$aHash["$Catalog"] := $Catalog
	$aHash["$Id"] := $Id
	$aHash["$Pw"] := $Pw
	
	/*
	MsgBox %$SiteNo%
	;自動產生csv
	stringreplace $SiteNo, $SiteNo, .000000, , ,
	str = %$SiteNo%-1
	run 產生csv檔.bat %str%
	;run D:\Users\wolf.11\Documents\ahk\tws_post\Script\%str%.csv
	
	sleep 1000
	
	str2 = %$SiteNo%-2
	run 產生csv檔.bat %str2%
	;run D:\Users\wolf.11\Documents\ahk\tws_post\Script\%str%.csv
	*/
	
	
	
	;給$SiteNo號碼 調出 路徑與檔名的 CSV 檔名
	;運行腳本
	
	
	;登入
	;前置處理腳本
	$Path := sSelectRunScript($SiteNo,1)
	iMacro_RunScript($Path,$aHash)
	
	/*
	;導頁一坨拉庫,貼文發文
	$Path := sSelectRunScript($SiteNo,2)
	iMacro_RunScript($Path,$aHash)*/
	Return
	
!3::
	;手動()===================================================================================================
	


	;複製資料
	/*
	$RunScriptArray	;帳密
	$ProjectMethod	;文案的資料
	*/
	Return
	
!4::
	;only登入()================================================================================================
	
	;ok
	;[1][2][3][4][5][6] [$SiteNo,$TypeNo,$Id,$Pw,$WebLogin,$WebPost]
	;可能到時候改成用GUI來選
	$RunScriptArray := aSelectSiteRtnSiteNoTypeNo($UsingAccountsArray)	;選擇要運行的網站
	
	;待調整
	$TypeNo := $RunScriptArray[2]
	$SiteNo := $RunScriptArray[1]
	;[1][2][3][4][5] [$Title,$Summy,$Cnt,$Keywords,$Catalog]
	$ProjectMethod := aSendSiteNoRtnProjectMethod($TypeNo,$FinishedProjectArray)
	
	$Title = % $ProjectMethod[1]
	$Summy = % $ProjectMethod[2]
	$Cnt = % $ProjectMethod[3]
	$Keywords = % $ProjectMethod[4]
	$Catalog = % $ProjectMethod[5]
	$Id = % $RunScriptArray[3]
	$Pw = % $RunScriptArray[4]
	$WebLogin = % $RunScriptArray[5] 
	$WebPost = % $RunScriptArray[6]
	
	; 產生 Hash    全部的填入的值
	$aHash := {}		
	$aHash["$Title"] := $Title
	$aHash["$Summy"] := $Summy
	$aHash["$Cnt"] := $Cnt
	$aHash["$Keywords"] := $Keywords
	$aHash["$Catalog"] := $Catalog
	$aHash["$Id"] := $Id
	$aHash["$Pw"] := $Pw
	
	$Path := sSelectRunScript($SiteNo,2)
	iMacro_RunScript($Path,$aHash)
	;case 只登入()	//載入部分csv
	Return

!5::	
	;更換使用者()===============================================================================================
	;ok
	$nickname := sListEditorSelect($ProjectArray)		;列出編輯者GUI,並且選擇,回傳使用者暱稱

	;ok
	$aData := aExcelReadToAhk()		;$aData 就是儲存 總表 的 2維陣列	

	;ok
	$UsingAccountsArray := aSendNameRtnIdPw($nickname,$aData)	;給 暱稱 回傳 使用者資料2維陣列

	;ok
	$Line := $UsingAccountsArray[1][3]		;線路名
	vNetConnector($Line)	;連線函式

	MsgBox 可開始執行腳本了`n ALT+2 選擇腳本並且運行`n ALT+3 手動模式`n ALT+4 只進行網頁登入`n ALT+5 更換使用者 
	
	Return
	
!0::
	ExitApp ; 結束程式
	Return

;=======手動貼====================================================
;靠判斷SiteNo區間,來判斷 是blog還是forum

!A::
	Clipboard = %$Title%
	Send ^v
	return
!S::
	Clipboard = %$Summy%
	Send ^v
	return
!D::
	Clipboard = %$Cnt%
	Send ^v
	return
!F::
	;StringReplace, $Kw, $Kw, `r`n, , , All
	;Sendinput %$Kw%
	Clipboard = %$Keywords%
	Send ^v
	return
!G::
	Clipboard = %$Image%
	Send ^v
	Return
!C:: 
	Clipboard = %$Catalog%
	Send ^v
	Return
!Z::
	;帳號
	Clipboard = %$Id%
	Send ^v
	Return
!X::
	;密碼
	Clipboard = %$Pw%
	Send ^v
	Return
!Q::
	;登入頁網址
	Clipboard = %$WebLogin%
	Send ^v
	Return
!W::
	;發文網址
	Clipboard = %$WebPost%
	Send ^v
	Return
	


; 暫停/啟用熱鍵
Pause::Pause, Toggle