﻿; 說明
;	偵測 Firefox 是否存在，並設定為焦點視窗。
; 參數
;	無
; 回傳
;	無
;

uActiveFirefox()
{
	Global $sINIFilename
	IniRead, $sFirefoxPath, %$sINIFilename%, DefaultSection, FirefoxPath
	
	$iTitleMatchModeTemp := A_TitleMatchMode ; 儲存原本的 TitleMatchMode
	SetTitleMatchMode, 2 ; 改回模糊比對
	
	IfWinExist, Mozilla Firefox
		WinActivate
	Else
	{
		Run %$sFirefoxPath%
		WinWait, Mozilla Firefox, , 10
		If ErrorLevel
		{
			MsgBox, 開啟 Firefox 失敗，請排除狀況後按 Pause 繼續。
			Pause
		}
		Else
			WinActivate
	}
	SetTitleMatchMode, %$iTitleMatchModeTemp% ; 還原 TitleMatchMode
	Sleep, 2000
	Send ^l ;將游標定位在網址列
}
