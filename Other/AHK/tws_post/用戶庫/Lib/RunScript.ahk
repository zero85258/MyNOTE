﻿; 說明
;	讀取 CSV 腳本執行各種命令
; 參數
;	$sFileName 包含路徑與檔名的 CSV 檔名
;	$aHash 轉換規則 Hash
; 回傳
;	無
; Note
;	參數替換之後使用欄位關聯，參考 HTML 轉 BBCode
;	iMacro_RunScript 改為 RunScript 後，結構需調整
;		有些指令不會有 A_Index = 2 A_Index = 3，應該重新設計。
;	貼關鍵字 vSendKeywords,$aKeywords,"," 
;		指令,陣列變數(關鍵字集合),送出關鍵字後要使用的分隔按鍵

RunScript($sFileName, $aHash:="Null")
{
	Loop, read, %$sFileName%
	{
		$aRef := [] ;參數陣列初始化
		Loop, parse, A_LoopReadLine, CSV
		{
			If (A_Index = 1)
				$sCommand := A_LoopField
			Else If (A_Index = 2)
				$sRef1 := A_LoopField
			Else If (A_Index = 3)
				$iIsReturn := A_LoopField
			Else
			{
				; 判斷是否需 element mapping，
				; 若需要，替換對應變數，加入陣列
				; 若不需要，直接加入陣列
				$sString := sConvertByHash(A_LoopField, $aHash)
				$aRef.Insert($sString)
			}
		}
		/* ; 測試 是否成是否成功讀取到陣列
		Loop % $aRef.MaxIndex()
			MsgBox % $aRef[A_Index]
		*/
		
		If (RegExMatch($sCommand, "^#"))
			Continue
		Else If (RegExMatch($sCommand, "^iMacro_"))
			iMacro($sCommand, $sRef1, $iIsReturn, $aRef) ; sRef1 在此為 逾時秒數
		Else If (RegExMatch($sCommand, "Pause"))
			Pause
		Else If (RegExMatch($sCommand, "MsgBox")) ; sRef1 在此為 訊息內容
			MsgBox %$sRef1%
		Else ; 無特定指令者，皆視為按鍵動作
		{
			If ($sRef1) ; sRef1 在此為延遲毫秒數
			{
				$iKeyDelayTemp := A_KeyDelay ; 儲存原本的 KeyDelay
				SetKeyDelay, %$sRef1%
				Send %$sCommand%
				SetKeyDelay, %$iKeyDelayTemp% ; 還原 KeyDelay
				$sRef1 = ; 清空 $sRef1
			}
			Else
				Send %$sCommand%
		}
	}
}
