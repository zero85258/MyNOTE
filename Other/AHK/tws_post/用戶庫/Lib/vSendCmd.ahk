﻿; 說明
;	送出常用指令
;	腳本格式說明
;	滑鼠：c, X 軸座標, Y 軸座標
;	字串：s,字串
;	按鍵：
;	u 上
;	d 下
;	l 左
;	r 右
;	h Home
;	e Enter
;	su 上
;	sd 下
;	sl 左
;	sr 右
;	cp 複製
;	cv 貼上
; 參數
;	$sCmd 指令簡碼
;	$iRef1 參數1（滑鼠：X 軸座標/字串：字串/按鍵：次數）
;	$iRef2 參數2（滑鼠：Y 軸座標）
; 回傳
;	無
;

vSendCmd($sCmd, $iRef1, $iRef2)
{
	If ($sCmd = "c")
	{
		Click %$iRef1%, %$iRef2%
		Return
	}
	If ($sCmd = "s")
	{
		SendInput %$iRef1%
		Return
	}
		
	If ($sCmd = "u")
		$sCmd = {Up}
	If ($sCmd = "d")
		$sCmd = {Down}
	If ($sCmd = "l")
		$sCmd = {Left}
	If ($sCmd = "r")
		$sCmd = {Right}
		
	If ($sCmd = "h")
		$sCmd = {Home}
	If ($sCmd = "e")
		$sCmd = {Enter}
	If ($sCmd = "=")
		$sCmd = `=
	
	If ($sCmd = "su")
		$sCmd = +{Up}
	If ($sCmd = "sd")
		$sCmd = +{Down}
	If ($sCmd = "sl")
		$sCmd = +{Left}
	If ($sCmd = "sr")
		$sCmd = +{Right}
	
	If ($sCmd = "cp")
		$sCmd = ^c
	If ($sCmd = "cv")
		$sCmd = ^v
	
	IF $iRef1 is not integer
		$iRef1 = 1
	
	Loop % $iRef1
	{
		Send %$sCmd%
	}
}
