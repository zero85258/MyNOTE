﻿; 說明
;	將 一維陣列 以一個 Field 一行的方式 轉換為 TXT 格式
; 參數
;	$aData 陣列內容
; 回傳
;	$sData 陣列轉換後的內容

sArray1ToTXT($aData)
{
	If ($aData.MaxIndex() = "")
	{
		MsgBox, 無需要轉換的內容。
		Return 0
	}
	
	$iRow = 1
	Loop % $aData.MaxIndex()
	{
		$sData .= $aData[$iRow]"`n"
		$iRow++
	}
	Return $sData
}
