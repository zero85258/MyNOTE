﻿; 說明
;	將【2x2 索引陣列】轉換為【Hash（關聯陣列）】，
;	適用於將 CSV 讀取為【2x2 索引陣列】後轉換用。
; 參數
;	$aData 要轉換的 2x2 索引陣列
; 回傳
;	$aHash 轉換後的Hash（關聯陣列）
; Note
/*
	轉換前
	$aData := [[
	(Join],[
		"$sID"      , "Bill"
		"$sPWD"     , "0800"
		"$sTitle"   , "檢查是否有漏掉的元素"
	)]]
	
	轉換後
	$aData := {
	(LTrim Join,
		"$sID": "Allen"
		"$sPWD": "wolf7079"
		"$sTitle": "最簡單的標題下法"
	)}
*/

aArray2Hash($aData)
{
	$aHash := {}
	Loop % $aData.MaxIndex()
		$aHash[$aData[A_Index, 1]] := $aData[A_Index, 2]
	Return $aHash
}
