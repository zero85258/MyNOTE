; 說明
;	將剪貼簿內的 RichText 圖文轉換為 HTML
; 參數
;	None
; 回傳
;	$sHTML 轉換後的 HTML
; Note
;
sClipboardToHTML()
{
    if !DllCall("OpenClipboard", "ptr", 0)
        return
    if hdata := DllCall("GetClipboardData", "uint", DllCall("RegisterClipboardFormat", "str", "HTML Format"), "ptr")
    {
        size := DllCall("GlobalSize", "ptr", hdata)
        if pdata := DllCall("GlobalLock", "ptr", hdata, "ptr")
        {
            data := StrGet(pdata, size, "utf-8")
            if RegExMatch(data, "Os)<!--StartFragment-->\K.*(?=<!--EndFragment-->)", m)
            {
                $sHTML := m.Value
            }
            DllCall("GlobalUnlock", "ptr", hdata)
        }
    }
    DllCall("CloseClipboard")
    return $sHTML
}
