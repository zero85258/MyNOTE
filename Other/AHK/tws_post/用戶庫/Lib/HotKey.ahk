﻿; 暫停/啟用熱鍵
Pause::Pause, Toggle

; 終止程式
!0::ExitApp  

; 輔助說明
!F1::
	ToolTip,
	(LTrim
		Alt+0 終止程式
		Pause 暫停
		Alt+F1 顯示輔助說明
	)
	Sleep, 5000
	ToolTip
	Return
