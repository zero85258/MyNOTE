﻿; 說明
;	讀取 TXT 為陣列, 忽略註解符號 #
; 參數
;	$sFilePath 要讀取的檔案路徑與檔名
; 回傳
;	$aData 從 TXT 讀取出來的陣列

aReadTXT($sFilePath)
{
	IfNotExist, %$sFilePath%
	{
		MsgBox, %$sFilePath% 檔案不存在。
		Return 0
	}
	
	FileRead, $sFileData, %$sFilePath%
	
	$iRow = 1
	$aData := [] ;初始化
	
	Loop, parse, $sFileData, `n, `r
	{
		; 不是空行 AND # 開頭的, 讀取到陣列 $aData
		if ( A_LoopField <> "" AND !RegExMatch(A_LoopField, "^#") )
		{
			$aData[$iRow] := A_LoopField
			$iRow++
		}
	}
	
	Return $aData
}
