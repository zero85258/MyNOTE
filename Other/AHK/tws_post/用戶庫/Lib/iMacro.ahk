﻿; iMarco 函式集
;	iMacro 主函式
;	iMacro_Click			使用 iMacro 點擊 element
;	iMacro_GetUrl			使用 iMacro 取得頁面 URL
;	iMacro_GetValue			使用 iMacro 取得 element 的值
;	iMacro_GoToUrl			使用 iMacro 導頁
;	iMacro_SavePng			使用 iMacro 將網頁存成 PNG
;	iMacro_Wait				iMacro 等待
;	iMacro_SendValue		使用 iMacro 將值賦予 element - By Clipboard
;	iMacro_SendValueByCSV	使用 iMacro 將值賦予 element - By CSV

; 說明
;	呼叫 Firefox 執行 iMacro 指令
; 參數
;	$sMacroName 巨集名稱
;	$iTimeOut 逾時秒數 (可省略，30秒)
;	$iIsReturn 是否使用 Clipboard 回傳值 (可省略，預設值 0 代表無回傳值)
;	$aRef 傳值陣列 (可省略，預設值 Null 代表不使用 Clipboard 傳遞參數)
; 回傳
;	$sValue 回傳的值
; Note
;	$iTimeOut $iIsReturn 給預設值是因為 $sRef1&2 為可選參數，
;	而 AHK 中，首個可選參數右邊的所有參數都必須標記為可選的。
;	執行逾時之後可用 Log or Line 通知

iMacro($sMacroName, $iTimeOut:=30, $iIsReturn:=0, $aRef:="Null")
{
	; 產生 iMacro 執行命令
	$iMacroCommand = imacros://run/?m=%$sMacroName%.iim

	; 將活動視窗設定為 Mozilla Firefox
	uActiveFirefox()
	
	; 執行 iMacro
	Send ^l
	Sleep, 1000
	;Send ^{t} 
	SendInput %$iMacroCommand%
	Sleep, 1000
	Send {Enter}
     
	; 等待 iMacro 回報 任務完成
	SetTitleMatchMode, 3 ; 改用精準判斷，避免分頁包含 iMacro
	
	; 將傳值 $aRef 暫存至 Clipboard
	If ($aRef != "Null")
	{
		Loop % $aRef.MaxIndex()
		{
			; 將參數暫存至 Clipboard
			Clipboard =
			Clipboard := $aRef[A_Index]
			
			; 等待 iMacro 回報 任務完成
			WinWait, iMacros, , %$iTimeOut%
			
			If ErrorLevel
			{
				MsgBox, iMacro 執行逾時 %A_Index%
				Pause
				SetTitleMatchMode, 2 ; 改回模糊比對
				Return
			}
			Else
			{
				WinActivate
				Sleep, 1000
				Send {Enter}
			}
	
		}
	}
	SetTitleMatchMode, 2 ; 改回模糊比對
	
	; 從 Clipboard 中取得回傳值
	If ($iIsReturn = 1)
	{
		$sValue = %Clipboard%
		Clipboard =
		Return $sValue
	}
	Else
		Return
}


; 說明
;	使用 iMacro 點擊 element
; 參數
;	$sRef 使用 Clipboard 傳遞 - 點擊 element 的 Xpath
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_Click($sXpath)
{
	iMacro("XpathClick", 10, 0, [$sXpath])
}


; 說明
;	使用 iMacro 取得頁面 URL
; 參數
;	無
; 回傳
;	sValue 取得 Page 的 URL
; Note
;	[Done] 偵測到 iMacro = 完成
;	[Todo] 偵測到正確的 Title = 正確導頁，但有可能無法事前得知 Title。

iMacro_GetUrl()
{
	$sValue := iMacro("GetUrl", 10, 1)
	Return $sValue
}


; 說明
;	使用 iMacro 取得 element 的值
; 參數
;	$sRef 使用 Clipboard 傳遞 - 取值 element 的 Xpath
; 回傳
;	$sValue 取得 element 的值
; Note
;	[Done] 偵測到 iMacro = 完成
;	[Todo] 偵測到正確的 CLIPBOARD Value = 成功

iMacro_GetValue($sXpath)
{
	$sValue := iMacro("XpathGetValue", 10, 1, [$sXpath])
	Return $sValue
}


; 說明
;	使用 iMacro 導頁
; 參數
;	$sRef 使用 Clipboard 傳遞 - 網址
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成
;	[Todo] 偵測到正確的 Title = 正確導頁，但有可能無法事前得知 Title。

iMacro_GoToUrl($sURL)
{
	iMacro("GoToUrl", 300, 0, [$sURL])
}


; 說明
;	使用 iMacro 將網頁存成 PNG
; 參數
;	$sRef 使用 Clipboard 傳遞 - 存檔名稱
; 回傳
;	無
; Note
;	檔案存在 Firefox_iMacro\Data\profile\iMacros\Downloads\
;	[Done] 偵測到 iMacro = 完成

iMacro_SavePng($sBaseFileName)
{
	iMacro("SavePng", 300, 0, [$sBaseFileName])
}


; 說明
;	iMacro 等待
; 參數
;	$iWaitValue 使用 Clipboard 傳遞 - 等待秒數（不超過 60 秒）
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_Wait($iWaitValue)
{
	iMacro("Wait", 70, 0, [$iWaitValue])
}


; 說明
;	使用 iMacro 將值賦予 element - By Clipboard
; 參數
;	$sXpath 使用 Clipboard 傳遞 - 賦予值 element 的 Xpath
;	$sValue 使用 Clipboard 傳遞 - 賦予值的 Value
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_SendValue($sXpath, $sValue)
{
	iMacro("XpathSendValue", 10, 0, [$sXpath, $sValue])
}


; 說明
;	使用 iMacro 將值賦予 element - By CSV
; 參數
;	無，AHK 將 iMacor 要使用到的 Xpath 跟 Value 直接存到 XpathSendValue.csv
; 回傳
;	無
; Note
;	[Done] 偵測到 iMacro = 完成

iMacro_SendValueByCSV($sDatasourcesPath)
{
	iMacro("XpathSendValueByCSV", 10, 0, [$sDatasourcesPath])
}
