	
	
	;接取傳進來的參數
	;MsgBox %0%,%1%,%2%	
	$SkillNum := %1%
	$Time  = %2%
	
	;產生上界與下界
	$TimeFront := ($Time - 2) * 1000
	$TimeBack := ($Time + 2) * 1000
	;MsgBox %$TimeFront% , %$TimeBack%
	
	;開始輪詢
	while(1){
	
		;按下按鍵
		send %1%
		;MsgBox 按下%1%鍵
		
		;秒數隨機化,防偵測
		Random, $rand, $TimeFront, $TimeBack
		Random, $little, 1, 999
		$RunRand := $rand - $little
		;MsgBox %$RunRand% = %$rand% - %$little%
		
		;等待
		sleep %$RunRand%
	}
	
	ExitApp ; 結束程式
	
;===按鍵區==============================================
!0::
	ExitApp ; 結束程式
	Return
	
; 暫停/啟用熱鍵
Pause::Pause, Toggle